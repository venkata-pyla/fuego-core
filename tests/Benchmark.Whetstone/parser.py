#!/usr/bin/python

import os, sys
import common as plib

regex_string = '^(C Converted Double Precision Whetstones:)(\ )([\d]{1,4}.?[\d]{1,2})(.*)$'

measurements = {}
matches = plib.parse_log(regex_string)

if matches:
    measurements['default.Whetstone'] = [{'name': 'Score', 'measure' : float(matches[0][2])}]

sys.exit(plib.process(measurements))
