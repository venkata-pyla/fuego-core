#!/bin/sh

#  In target, run command tar.
#  option: czf(gzip)

test="create02"

mkdir test_dir

if tar czf mytest.tar.gz test_dir
then
    echo "creation of tar succeeded."
else
    echo "creation of tar failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if test -f mytest.tar.gz
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test_dir
