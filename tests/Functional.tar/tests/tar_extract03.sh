#!/bin/sh

#  In target, run command tar.
#  option: xjf(bzip2)

test="extract03"

if tar xjf data/test.tar.bz2
then
    echo " -> tar xjf succeeded."
else
    echo " -> tar xjf failed."
fi

if test -f test/data/test_c.txt
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test
