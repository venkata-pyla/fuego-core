#!/bin/sh

#  In target, run command tar.
#  option: cf

test="create01"

mkdir test_dir

if tar cf mytest.tar test_dir
then
    echo "creation of tar succeeded."
else
    echo "creation of tar failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if test -f mytest.tar
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr test_dir
