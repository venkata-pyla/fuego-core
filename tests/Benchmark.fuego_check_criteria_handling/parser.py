#!/usr/bin/python

import os, re, sys, random
import common as plib

# this is a bit tricky
# when os.environ["TESTSPEC"]=="gen_results", there is a different list
# of reported measures
regex_string = "^(.*)=(.*)$"

measurements = {}
matches = plib.parse_log(regex_string)
if matches:
    for match in matches:
        tguid = "default." + match[0]
        measurements[tguid] = [{"name": "number", "measure": float(match[1])}]

sys.exit(plib.process(measurements))
