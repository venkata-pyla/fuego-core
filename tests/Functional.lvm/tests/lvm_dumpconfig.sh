#!/bin/sh

#  In target, display the configuration information.
#  option: none

test="dumpconfig"

if lvm dumpconfig | grep "devices"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
