#!/bin/sh

#  In target, display the version information.
#  option: none

test="version"

if lvm version | grep "LVM version"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
