function test_pre_check {
    is_on_target_path pam_timestamp_check PROGRAM_PAM
    assert_define PROGRAM_PAM "Missing 'pam_timestamp_check' program on target board"
}

function test_deploy {
    put $TEST_HOME/pam_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    sh -v pam_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
