===========
Description
===========
Obtained from inotify06.c DESCRIPTION:

Test for inotify mark destruction race.

Kernels prior to 4.2 have a race when inode is being deleted while
inotify group watching that inode is being torn down.  When the race is
hit, the kernel crashes or loops.

The problem has been fixed by commit:
 8f2f3eb59dff "fsnotify: fix oops in fsnotify_clear_marks_by_group_flags()".

The parent test for testcase is Functional.LTP, and this testcase is
included in test set 'syscalls'.

A rough outline of this testcase is:
 - make a bunch of files (5), and then remove them, over and over again
 - in a separate thread (fork):
   - in a loop (400 times):
     - call inotify_init1
     - for each file, add do syscall inotify_add_watch for the filename
     - close inotify_fd

====
Tags
====

* kernel, syscall, inotify

=========
Resources
=========

* https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8f2f3eb59dff4ec538de55f2e0592fec85966aab

=======
Results
=======
This test causes a kernel Oops on a BeagleBone Black running kernel version
3.8.13.

Here is what the Oops for this looked like on that board:::

   kernel panic - not syncing: softlockup: hung tasks
   [<c00111f1>] (unwind_backtrace+0x1/0x9c) from [<c04c8955>] (panic+0x59/0x158)
   [<c04c8955>] (panic+0x59/0x158) from [<c00726d9>] (watchdog_timer_fn+0xe5/0xfc)
   [<c00726d9>] (watchdog_timer_fn+0xe5/0xfc) from [<c0047b4b>] (__run_hrtimer+0x4b/0x154)
   [<c0047b4b>] (__run_hrtimer+0x4b/0x154) from [<c00483f7>] (hrtimer_interrupt+0xcf/0x1fc)
   [<c00483f7>] (hrtimer_interrupt+0xcf/0x1fc) from [<c001e98b>] (omap2_gp_timer_interrupt+0x1f/0x24)
   [<c001e98b>] (omap2_gp_timer_interrupt+0x1f/0x24) from [<c0072dd3>] (handle_irq_event_percpu+0x3b/0x188)
   [<c0072dd3>] (handle_irq_event_percpu+0x3b/0x188) from [<c0072f49>] (handle_irq_event+0x29/0x3c)
   [<c0072f49>] (handle_irq_event+0x29/0x3c) from [<c007489b>] (handle_level_irq+0x53/0x8c)
   [<c007489b>] (handle_level_irq+0x53/0x8c) from [<c00729ff>] (generic_handle_irq+0x13/0x1c)
   [<c00729ff>] (generic_handle_irq+0x13/0x1c) from [<c000d0df>] (handle_irq+0x23/0x60)
   [<c000d0df>] (handle_irq+0x23/0x60) from [<c00085a9>] (omap3_intc_handle_irq+0x51/0x5c)
   [<c00085a9>] (omap3_intc_handle_irq+0x51/0x5c) from [<c04cea9b>] (__irq_svc+0x3b/0x5c)
   exception stack(0xcb7e5e98 to 0xcb7e5ee0)
   5e80:                                                       de03ef54 df34f180
   5ea0: 68836882 00000000 de03ef54 cb7e4000 de03ef54 ffffffff df34f180 df34f1dc
   5ec0: 00000010 df016450 cb7e5ee8 cb7e5ee0 c00e2e33 c025eb88 60000033 ffffffff
   [<c04cea9b>] (__irq_svc+0x3b/0x5c) from [<c025eb88>] (do_raw_spin_lock+0xa4/0x114)
   [<c025eb88>] (do_raw_spin_lock+0xa4/0x114) from [<c00e2e33>] (fsnotify_destroy_mark_locked+0x17/0xec)
   [<c00e2e33>] (fsnotify_destroy_mark_locked+0x17/0xec) from [<c00e316b>] (fsnotify_clear_marks_by_group_flags+0x57/0x74
   [<c00e316b>] (fsnotify_clear_marks_by_group_flags+0x57/0x74) from [<c00e2869>] (fsnotify_destroy_group+0x9/0x24)
   [<c00e2869>] (fsnotify_destroy_group+0x9/0x24) from [<c00e3c91>] (inotify_release+0x1d/0x20)
   [<c00e3c91>] (inotify_release+0x1d/0x20) from [<c00bbb69>] (__fput+0x65/0x16c)
   [<c00bbb69>] (__fput+0x65/0x16c) from [<c004323d>] (task_work_run+0x6d/0xa4)
   [<c004323d>] (task_work_run+0x6d/0xa4) from [<c000f3eb>] (do_work_pending+0x6f/0x70)
   [<c000f3eb>] (do_work_pending+0x6f/0x70) from [<c000c893>] (work_pending+0x9/0x1a)


.. fuego_result_list::

======
Status
======

.. fuego_status::

=====
Notes
=====
