#!/usr/bin/python
# FIXTHIS: support parsing streams

import os, sys
import json
import matplotlib
matplotlib.use('Agg')
import pylab as plt
import common as plib

measurements = {}
onembps=1000000.0
with open(plib.TEST_LOG) as f:
    data = json.load(f)

    # Measurements to apply criteria
    if 'sum_sent' in data['end']:
        measurements["sum.sent"] = [
            {"name": "bits_per_second", "measure" : float(data['end']['sum_sent']['bits_per_second'])/onembps},
            {"name": "retransmits", "measure" : float(data['end']['sum_sent']['retransmits'])}
        ]
    if 'sum_received' in data['end']:
        measurements["sum.received"] = [
            {"name": "bits_per_second", "measure" : float(data['end']['sum_received']['bits_per_second'])/onembps}
        ]
    if 'sum' in data['end']:
        measurements["sum.udp"] = [
            {"name": "bits_per_second", "measure" : float(data['end']['sum']['bits_per_second'])/onembps},
            {"name": "jitter_ms", "measure" : float(data['end']['sum']['jitter_ms'])}
        ]
    measurements["cpu_utilization.host"] = [
        {"name": "total", "measure" : float(data['end']['cpu_utilization_percent']['host_total'])},
        {"name": "system", "measure" : float(data['end']['cpu_utilization_percent']['host_system'])},
        {"name": "user", "measure" : float(data['end']['cpu_utilization_percent']['host_user'])}
    ]
    measurements["cpu_utilization.remote"] = [
        {"name": "total", "measure" : float(data['end']['cpu_utilization_percent']['remote_total'])},
        {"name": "system", "measure" : float(data['end']['cpu_utilization_percent']['remote_system'])},
        {"name": "user", "measure" : float(data['end']['cpu_utilization_percent']['remote_user'])}
    ]
    # Create graph with interval data
    time = []
    bits_per_second = []
    for interval in data['intervals']:
        if interval['sum']['omitted']:
            continue
        time.append(interval['sum']['start'])
        bits_per_second.append(float(interval['sum']['bits_per_second'])/onembps)
    fig = plt.figure()
    plt.plot(time, bits_per_second)
    plt.title('iperf3 client results')
    plt.xlabel('time (s)')
    plt.ylabel('Megabits per second (Mbps)')
    fig.savefig(os.environ['LOGDIR'] + '/iperf3.png')

sys.exit(plib.process(measurements))
