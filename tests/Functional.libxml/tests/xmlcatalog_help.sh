#!/bin/sh

#  In the target to execute command xmlcatalog and confirm the result.
#  option : --help

test="xmlcatalog1"

if xmlcatalog --help | grep "Usage"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
