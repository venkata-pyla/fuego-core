function test_pre_check {
    assert_has_program ospf6d
    assert_has_program zebra
}

function test_deploy {
    put $TEST_HOME/ospf6d_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    cmd "sed -i 's/!interface xxx/interface $IFETH/' $BOARD_TESTDIR/fuego.$TESTDIR/data/zebra.conf"
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        sh ospf6d_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
