#!/bin/sh

#  In the target start ospf6d and zebra, then confirm the log file.
#  check the /var/log/quagga/ospf6d.log file.

test="logfile"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target ospf6d stop
exec_service_on_target zebra stop

if [ ! -d /var/log/quagga ]
then
   mkdir /var/log/quagga
   chown -R quagga:quagga /var/log/quagga
fi

if [ -f /var/log/quagga/ospf6d.log ]
then
   mv /var/log/quagga/ospf6d.log /var/log/quagga/ospf6d.log.bck
fi

#Backup the config file
mv /etc/quagga/ospf6d.conf /etc/quagga/ospf6d.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck

cp data/ospf6d.conf /etc/quagga/ospf6d.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target ospf6d start
then
    echo " -> start of ospf6d succeeded."
else
    echo " -> start of ospf6d failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ls /var/log/quagga/ospf6d.log
then
    echo " -> get log file of ospf6d."
    echo " -> $test: TEST-PASS"
else
    echo " -> can't get log file of ospf6d."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target ospf6d stop
exec_service_on_target zebra stop

#Restore the config file
mv /etc/quagga/ospf6d.conf.bck /etc/quagga/ospf6d.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf

if [ -f /var/log/quagga/ospf6d.log.bck ]
then
    mv /var/log/quagga/ospf6d.log.bck /var/log/quagga/ospf6d.log
fi
