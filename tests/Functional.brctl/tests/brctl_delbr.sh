#!/bin/sh

#  In the target to execute command brct; and confirm the result.
#  option : delbr

test="brctl->delbr"

brctl addbr br0
if brctl delbr br0
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
