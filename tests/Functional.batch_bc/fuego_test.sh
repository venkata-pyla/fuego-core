# test to test bc with different specs
#
# Notes:
# - each sub-test is a testcase
# - we use TAP output to summarize the results for this test
#

BATCH_TESTPLAN=$(cat <<END_TESTPLAN
{
    "testPlanName": "default",
    "default_timeout": "5m",
    "default_spec": "default",
    "default_reboot" : "false",
    "default_rebuild" : "false",
    "default_precleanup" : "true",
    "default_postcleanup" : "true",
    "tests": [
        { "testName": "Functional.bc", "spec": "default" },
        { "testName": "Functional.bc", "spec": "bc-add" },
        { "testName": "Functional.bc", "spec": "bc-mult" },
        { "testName": "Functional.bc", "spec": "bc-by2" }
    ]
}
END_TESTPLAN
)

function test_run {
    export TC_NUM=1
    DEFAULT_TIMEOUT=3m
    export FUEGO_BATCH_ID="bc-$(allocate_next_batch_id)"

    # don't stop on test errors
    set +e
    log_this "echo \"batch_id=$FUEGO_BATCH_ID\""
    run_test Functional.bc -s default
    run_test Functional.bc -s bc-add
    run_test Functional.bc -s bc-mult
    run_test Functional.bc -s bc-by2
    set -e
}
