# test to check if needed modules and check_module routines work
#
# NOTE: you have to manually uncomment the calls to assert_has_module
# to do the test_pre_check tests

# msdos should work (at least on min1)
NEED_MODULE="msdos"
# test a missing module
#NEED_MODULE="foo"
# test a builtin
#NEED_MODULE="bitrev"

# see if we can do direct calls to is_on_target_module
function test_pre_check {
    echo "in test_pre_check"
    #assert_has_module "msdos"
    #assert_has_module "foo"
    assert_has_module "bitrev"
    echo "done with assert_has_modules in test_pre_check"
}

function test_run {
    report "echo Module check should have happened already"
    report_append "echo \"ok 1 - check_needs allowed script to proceed: NEED_MODULE=$NEED_MODULE\""

    tmpfile=$(mktemp)

    # some tests will fail, so disable errexit mode
    set +e

    # now do some host-driven direct tests of check_needs
    NEED_MODULE=msdos
    desc="module present: NEED_MODULE=$NEED_MODULE"
    check_needs
    rcode="$?"
    expected="0"
    if [ $rcode == "$expected" ] ; then
      report_append "echo ok 2 - $desc : rcode=$rcode"
    else
      report_append "not ok 2 - $desc : rcode=$rcode"
    fi

    NEED_MODULE=foo
    desc="module missing: NEED_MODULE=$NEED_MODULE"
    check_needs
    rcode="$?"
    expected="1"
    if [ $rcode == "$expected" ] ; then
      report_append "echo ok 3 - $desc : rcode=$rcode"
    else
      report_append "echo not ok 3 - $desc : rcode=$rcode"
    fi

    NEED_MODULE=bitrev
    desc="builtin module: NEED_MODULE=$NEED_MODULE"
    check_needs
    rcode="$?"
    expected="0"
    if [ $rcode == "$expected" ] ; then
      report_append "echo ok 4 - $desc : rcode=$rcode"
    else
      report_append "echo not ok 4 - $desc : rcode=$rcode"
    fi
    set -e
}

function test_processing {
    log_compare "$TESTDIR" "4" "^ok" "p"
}
