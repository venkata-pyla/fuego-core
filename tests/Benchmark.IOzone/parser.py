#!/usr/bin/python

import os, sys
import common as plib

measurements = {}
cur_file = open(plib.TEST_LOG,'r')
print "Reading current values from " + plib.TEST_LOG +"\n"

lines = cur_file.readlines()
cur_file.close()

tests = ["Writer report", "Re-writer report", "Reader report", "Re-Reader report", "Random read report", "Random write report", "Fwrite report","Re-Fwrite report", "Fread report", "Re-Fread report"]

for line in lines:
    string = line.lstrip('\"').rstrip('\" \n')
    split = line.split(" ")

    if string in tests:
        if string == "Writer report":
                test = "2048_Kb_Record_Write.Write"
        elif string == "Re-writer report":
                test = "2048_Kb_Record_Write.ReWrite"
        elif string == "Reader report":
                test = "2048_Kb_Record_Read.Read"
        elif string == "Re-Reader report":
                test = "2048_Kb_Record_Read.ReRead"
        elif string == "Random read report":
                test = "2048_Kb_Record_Read.Random_read"
        elif string == "Random write report":
                test = "2048_Kb_Record_Write.Random_write"
        elif string == "Fwrite report":
                test = "2048_Kb_Record_Write.Fwrite"
        elif string == "Re-Fwrite report":
                test = "2048_Kb_Record_Write.ReFwrite"
        elif string == "Fread report":
                test = "2048_Kb_Record_Read.Fread"
        elif string == "Re-Fread report":
                test = "2048_Kb_Record_Read.ReFread"

    if split[0].lstrip('\"').rstrip('\"') == "2048":
        measurements[test] = [{"name": "Score", "measure" : float(split[5])}]

sys.exit(plib.process(measurements))
