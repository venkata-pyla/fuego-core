#!/usr/bin/python
# See common.py for description of command-line arguments
#
# clitest output parser
#
# clitest output has the format:
#
# #1   command args ...
# #2   next-command args ...
# [FAILED #3, line 20] command args ...
# @@ -1 +1 @@
# -saw
# +expected
# #4   next-command args
#
# on success, the test numbers is first, followed by the command
# on failure, the [FAILED...] line is shown
#

import os
import sys
import collections
import common as plib

results = {}
results = collections.OrderedDict()

regex_string = r"^(\[FAILED #|#)(\d+)(\s|, line \d+\])(.*)$"

matches = plib.parse_log(regex_string)

for m in matches:
    print("DEBUG: in parser.py: m=%s" % str(m))
    if "FAILED" in m[0]:
        status = "FAIL"
    else:
        status = "PASS"
    test_id = "test-"+m[1]
    results[test_id] = status

# split the output for each testcase
# information follows result line for each test (info_follows_regex=1)
plib.split_output_per_testcase(regex_string, results, 1)

sys.exit(plib.process(results))
