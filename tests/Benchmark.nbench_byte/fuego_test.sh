tarball=nbench-byte-2.2.3.tar.gz

function test_pre_check {
    is_on_sdk libm.a LIBM /lib:/usr/lib:/usr/local/lib
}

function test_build {
    patch -N -s -p0 < $TEST_HOME/nbench.Makefile.patch
    rm -f pointer.h && touch pointer.h

    if [ -z $LIBM ]; then
       CFLAGS+=" -s -Wall -O3"
    else
       CFLAGS+=" -s -static -Wall -O3"
    fi

    make CFLAGS="${CFLAGS}" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP"
}

function test_deploy {
	put hardware nbench sysinfo.sh *.DAT  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report  "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./nbench"  
}


