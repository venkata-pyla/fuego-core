#!/usr/bin/python

import os, re, sys
import common as plib

ref_section_pat = "^\[[\w\d\s_.-]+.[gle]{2}\]"
cur_search_pat = re.compile("^([A-Z- ]{4,21})(.+)([:\s]+)([\d]{1,2}.[\d]{2,4})(\n)")


cur_dict = {}
cur_file = open(plib.TEST_LOG,'r')
print "Reading current values from " + plib.TEST_LOG +"\n"
logfile = cur_file.readlines()

if logfile:
	for line in logfile:
		result = cur_search_pat.match(line)
		if result:
			if "INDEX" in result.group(1):
				cur_dict["INDEX."+result.group(1).split(' ')[0]] = result.group(4)
			else:
				if result.group(1).split(' ')[1] != '':
					cur_dict["TEST_GROUP1."+result.group(1).split(' ')[0]+"_"+result.group(1).split(' ')[1]] = result.group(4)
				else:
					cur_dict["TEST_GROUP2."+result.group(1).split(' ')[0]] = result.group(4)
cur_file.close()

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'Index'))
