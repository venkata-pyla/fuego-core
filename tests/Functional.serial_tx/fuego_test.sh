#FUEGO_DEBUG=1

# Make sure that:
#
# -"getty" is not running on the DUT's port under test.
# - the latest 'lc' is in the docker container
# - the labcontrol server is running

function test_deploy {
    put $TEST_HOME/serial-transmit-test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    # get baud rate list from spec file
    assert_define FUNCTIONAL_SERIAL_TX_BAUDRATES

    # serial ports for test should be defined in the board file
    assert_define SERIAL_ID
    assert_define SERIAL_DEV

    if [ -z "$BAUD_RATES" ] ; then
        BAUD_RATES="$FUNCTIONAL_SERIAL_TX_BAUDRATES"
    fi

    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./serial-transmit-test.sh -v $NODE_NAME $SERIAL_ID $SERIAL_DEV $BAUD_RATES"
}

#function test_processing {
#    log_compare "$TESTDIR" "$(echo "$BAUD_RATES" | wc -w)" "^ok" "p"
#}
