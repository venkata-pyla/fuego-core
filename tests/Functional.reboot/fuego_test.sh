function test_run {
    # MAX_REBOOT_RETRIES can be defined in the board file.
    # Otherwise, the default is 20 retries
    retries=${MAX_REBOOT_RETRIES:-20}

    # REBOOT_WAIT_TIME can be defined in the board file, or by a user
    if [ -n "$REBOOT_WAIT_TIME" ] ; then
        wait_time="$REBOOT_WAIT_TIME"
    else
        # the spec can define a wait time
        wait_time=${FUNCTIONAL_REBOOT_WAIT_TIME:-40}
    fi

    log_this "echo TAP version 13"
    log_this "echo 1..4"

    # drop a file into the tmp directory, for a later test
    # calculate the tmp dir the same way Fuego does:
    local fuego_test_tmp="${FUEGO_TARGET_TMP:-/tmp}/fuego.$TESTDIR"
    log_this "echo \"# fuego_test_tmp=$fuego_test_tmp\" "

    # NOTE: pre-test operations should have already created the fuego_test_tmp dir
    cmd "echo \"Does this file survive a reboot?\" >$fuego_test_tmp/reboot-tmp-testfile.txt"

    # try a software reboot first
    testcase_num=1
    desc="software reboot with target_reboot"
    start_time=$(date +"%s")

    echo "Calling target_reboot for board $NODE_NAME..."
    target_reboot $retries

    # sleep a bit more, just because we're paranoid
    # board may have networking up, but need more time to mount filesystems
    sleep 10

    # get uptime (first word, up to decimal point)
    uptime_str=$(cmd cat "/proc/uptime")
    uptime="${uptime_str%%.*}"
    log_this "echo \"# uptime of board=$uptime (seconds)\""

    end_time=$(date +"%s")
    duration=$(( $end_time - $start_time ))
    log_this "echo \"# duration of reboot=$duration (seconds)\""
    if [ ${uptime} -lt ${duration} ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi

    # check if board has BOARD_CONTROL setting
    testcase_num=$(( testcase_num + 1 ))
    desc="does board have BOARD_CONTROL setting"

    log_this "echo \"# BOARD_CONTROL=$BOARD_CONTROL\" "
    if [ -n "$BOARD_CONTROL" ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi

    # try a hardware reboot
    testcase_num=$(( testcase_num + 1 ))
    desc="hardware reboot with ov_board_control_reboot"
    start_time=$(date +"%s")

    echo "Calling ov_board_control_reboot for board $NODE_NAME..."
    ov_board_control_reboot
    OBCR_RESULT="$?"

    log_this "echo \"# return code of $OBCR_RESULT from ov_board_control_reboot\""
    echo "waiting for hardware reboot"
    sleep $wait_time

    uptime_str=$(cmd cat "/proc/uptime")
    uptime="${uptime_str%%.*}"
    log_this "echo \"# uptime of board=$uptime (seconds)\""

    end_time=$(date +"%s")
    duration=$(( $end_time - $start_time ))
    log_this "echo \"# duration of reboot=$duration (seconds)\""
    if [ ${uptime} -lt ${duration} ] ; then
        log_this "echo ok $testcase_num - $desc"
    else
        log_this "echo not ok $testcase_num - $desc"
    fi

    # check /tmp directory persistence
    testcase_num=$(( testcase_num + 1 ))
    desc="is Fuego tmp directory persistent"
    if cmd "test -f $fuego_test_tmp/reboot-tmp-testfile.txt" ; then
        log_this "echo ok $testcase_num - $desc"
        cmd "rm $fuego_test_tmp/reboot-tmp-testfile.txt"
    else
        log_this "echo \"# Could not find $fuego_test_tmp/reboot-tmp-testfile.txt on the board\" "
        log_this "echo \"# Fuego tmp directory may not be persistent across board reboots\" "
        log_this "echo \"# Please consider using FUEGO_TARGET_TMP in the board file to avoid\" "
        log_this "echo \"# problems.\" "
        log_this "echo not ok $testcase_num - $desc"
    fi
}
