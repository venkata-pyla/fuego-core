#!/bin/sh

#  The testscript checks the following options of the command cryptsetup
#  option: --help

test="help"

if cryptsetup --help | grep Usage
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
