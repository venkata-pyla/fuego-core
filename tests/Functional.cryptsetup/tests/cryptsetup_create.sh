#!/bin/sh

#  The testscript checks the following options of the command cryptsetup
#  option: create

test="create"

modprobe loop

dd if=/dev/zero of=/tmp/cryptloop bs=1k count=1000
losetup -f
losetup /dev/loop0 /tmp/cryptloop

expect <<-EOF
spawn cryptsetup create loop_test /dev/loop0
expect {
 -re "Enter passphrase:" {
           send "test\n"
           send_user " -> $test: create loop_test succeeded.\n"
          }
 default { send_user " -> $test: TEST-FAIL\n" }  }
expect eof
EOF

if ls /dev/mapper | grep loop_test
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
cryptsetup remove loop_test
losetup -d /dev/loop0
