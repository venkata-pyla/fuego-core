#!/bin/sh

#  In target, run command mcelog.
#  If the text that is not an error log is input to mcelog, it is confirmed that the decoding process is not performed.

test="mcelog_01"

mkdir mcelog
echo "this file is not hardware error log" > mcelog/mcelog_plain_text.log

if mcelog --ascii < mcelog/mcelog_plain_text.log | grep "this file is not hardware error log"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr mcelog
