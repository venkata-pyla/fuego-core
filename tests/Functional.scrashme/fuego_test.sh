tarball=scrashme.tar.bz2

function test_pre_check {
    assert_define FUNCTIONAL_SCRASHME_NUM
    assert_define FUNCTIONAL_SCRASHME_MODE
}

function test_build {
    if [ $ARCHITECTURE == "arm64" ] ; then
       patch -p2 -N -s < $TEST_HOME/scrashme-testfix-arm64.patch
    fi
    if [ $ARCHITECTURE == "arm" ] ; then
       patch -p1 -N -s < $TEST_HOME/scrashme-testfix.patch
    fi
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD"
}

function test_deploy {
    put scrashme  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./scrashme --mode=$FUNCTIONAL_SCRASHME_MODE -N$FUNCTIONAL_SCRASHME_NUM"
}
