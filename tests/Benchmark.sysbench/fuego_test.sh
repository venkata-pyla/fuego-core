tarball=sysbench-0.4.8.tar.bz2

# FIXTHIS: should check for libaio in test_pre_check

function test_build {
    # not needed due to patch removing MALLOC check
    #echo "ac_cv_func_malloc_o_nonnull=no" > config.cache
    patch -p0 < $TEST_HOME/malloc_cross_compile_fix.patch
    patch -p0 < $TEST_HOME/disable_libtool.patch
    ./autogen.sh

    # get updated config.sub and config.guess files, so configure
    # doesn't reject new toolchains
    cp /usr/share/misc/config.{sub,guess} config

    # use this line if config.cache is required
    #./configure --build=`./config.guess` --HOST=$HOST CC="$CC" AR="$AR" RANLIB="$RANLIB" --config-cache
    # FIXTHIS: would be nice to support mysql and other tests as well
    ./configure --host=$PREFIX --without-mysql
    make
}

function test_deploy {
    put sysbench/sysbench $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    # tests available are: fileio, cpu, memory, threads, mutex, otlp
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./sysbench --num-threads=4 --test=cpu --cpu-max-prime=20000 --validate run"
}

function test_processing {
    echo "Need something in test_processing"
}

