#!/usr/bin/python

import os, re, sys
import common as plib

measurements = {}
regex_string = "^.*total time:\s+([\d]{1,8}.?[\d]{1,43})s$"
matches = plib.parse_log(regex_string)

if matches:
    measurements['cpu.prime_calculation'] = [{"name": "time", "measure" : float(matches[0])}]

regex_string = "^.*events \(avg/stddev\):\s+([\d]{1,8}.?[\d]{1,43})/.*$"
matches = plib.parse_log(regex_string)

if matches:
    measurements['cpu.thread_fairness'] = [{"name": "events", "measure" : float(matches[0])}]

sys.exit(plib.process(measurements))
