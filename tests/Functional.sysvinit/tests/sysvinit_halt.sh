#!/bin/sh

#  In target, run command halt.

test="halt"

if halt -w
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
