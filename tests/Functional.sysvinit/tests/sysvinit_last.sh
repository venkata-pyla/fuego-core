#!/bin/sh

#  In target, run command last.

test="last"

USER_NAME=$(id -u -n)
if last | grep "$USER_NAME.*still logged in"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
