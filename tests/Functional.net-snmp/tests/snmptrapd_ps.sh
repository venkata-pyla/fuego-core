#!/bin/sh

#  In the target start snmptrapd, and confirm the process condition by command ps.
#  check the keyword "snmptrapd".

test="snmptrapd_ps"

snmptrapd_status=$(get_service_status snmptrapd)
exec_service_on_target snmptrapd stop

if exec_service_on_target snmptrapd start
then
    echo " -> start of snmptrapd succeeded."
else
    echo " -> start of snmptrapd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps aux | grep "[/]usr/sbin/snmptrapd"
then
    echo " -> get the pid of snmptrapd."
else
    echo " -> can't get the pid of snmptrapd."
    echo " -> $test: TEST-FAIL"

    if [ "$snmptrapd_status" = "inactive" ]
    then
        exec_service_on_target snmptrapd stop
    fi
    exit
fi

if exec_service_on_target snmptrapd stop
then
    echo " -> stop of snmptrapd succeeded."
else
    echo " -> stop of snmptrapd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ps aux | grep "[/]usr/sbin/snmptrapd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

if [ "$snmptrapd_status" = "active" -o "$snmptrapd_status" = "unknown" ]
then
    exec_service_on_target snmptrapd start
fi
