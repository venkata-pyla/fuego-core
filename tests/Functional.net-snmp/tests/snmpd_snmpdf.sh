#!/bin/sh

#  In the target start service snmpd.
#  Run command snmpdf and check the keyword "Memory Buffers".

test="snmpdf"

snmpd_status=$(get_service_status snmpd)

exec_service_on_target snmpd stop

mv /etc/hosts /etc/hosts_bak
cp data/bind9/hosts /etc/hosts
mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf_bak
cp data/net-snmp/snmpd.conf /etc/snmp/snmpd.conf

restore_target() {
    mv /etc/hosts_bak /etc/hosts
    mv /etc/snmp/snmpd.conf_bak /etc/snmp/snmpd.conf
}

if exec_service_on_target snmpd start
then
    echo " -> start of snmpd succeeded."
else
    echo " -> start of snmpd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

if snmpdf -v1 -c RWIINCOM localhost | grep "Physical memory"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$snmpd_status" = "inactive" ]
then
    exec_service_on_target snmpd stop
fi
restore_target
