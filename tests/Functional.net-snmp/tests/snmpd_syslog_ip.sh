#!/bin/sh

#  In the target start snmpd.
# Check the log of IP address.

test="snmpd_syslog_ip"

logger_service=$(detect_logger_service)

snmpd_status=$(get_service_status snmpd)
snmpd_logfile=$(get_service_logfile)
exec_service_on_target snmpd stop
exec_service_on_target $logger_service stop

if [ -f $snmpd_logfile ]
then
    mv $snmpd_logfile $snmpd_logfile"_bak"
fi

mv /etc/hosts /etc/hosts_bak
cp data/bind9/hosts /etc/hosts
mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf_bak
cp data/net-snmp/snmpd.conf /etc/snmp/snmpd.conf

restore_target() {
    mv /etc/hosts_bak /etc/hosts
    mv /etc/snmp/snmpd.conf_bak /etc/snmp/snmpd.conf
    if [ -f $snmpd_logfile"_bak" ]
    then
        mv $snmpd_logfile"_bak" $snmpd_logfile
    fi
}

exec_service_on_target $logger_service restart

sleep 2

if exec_service_on_target snmpd start
then
    echo " -> start of snmpd succeeded."
else
    echo " -> start of snmpd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

snmpget -v1 -c RWIINCOM -M /usr/share/snmp/mibs localhost system.sysDescr.0

sleep 3

if cat $snmpd_logfile | grep "127.0.0.1"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ "$snmpd_status" = "inactive" ]
then
    exec_service_on_target snmpd stop
fi
restore_target
