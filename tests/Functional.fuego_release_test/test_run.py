#!/usr/bin/env python3
import argparse
import atexit
import http
import http.client
import logging
import os
import re
import subprocess
import sys
import time
from getpass import getpass
from io import BytesIO

import docker
import pexpect
import requests
import selenium.common.exceptions as selenium_exceptions
from selenium import webdriver
from selenium.webdriver.common.by import By
from PIL import Image


class FuegoReleaseTestLogger(logging.Logger):
    REPORT_LOG_LEVEL = 51

    def report(self, msg, *args, **kwargs):
        self.log(FuegoReleaseTestLogger.REPORT_LOG_LEVEL,
                 msg, *args, **kwargs)


logging.setLoggerClass(FuegoReleaseTestLogger)
logging.addLevelName(FuegoReleaseTestLogger.REPORT_LOG_LEVEL, "REPORT")

LOGGER = logging.getLogger('test_run')
STREAM_HANDLER = logging.StreamHandler()
STREAM_HANDLER.setFormatter(
    logging.Formatter("%(name)s:%(levelname)s: %(message)s"))
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(STREAM_HANDLER)


def loop_until_timeout(func, timeout=10, num_tries=5):
    LOGGER.debug("Running %s()", func.__name__)

    for _ in range(num_tries):
        LOGGER.debug("  Try number %s...", _ + 1)
        if func():
            LOGGER.debug("  Success")
            return True
        time.sleep(timeout/num_tries)
    LOGGER.debug("  Failure")

    return False


def silent_remove(filename):
    try:
        os.remove(filename)
    except (FileNotFoundError, IsADirectoryError):
        pass


class SeleniumCommand:
    def exec(self, selenium_ctx):
        self.driver = selenium_ctx.driver
        self.driver.refresh()
        LOGGER.info("Executing Selenium Command '%s'",
                    self.__class__.__name__)

    @staticmethod
    def check_element_text(element, text):
        try:
            if text in element.text:
                LOGGER.\
                    debug("  Text '%s' matches element.text '%s'",
                          text, element.text)
                LOGGER.info("  Matching text found")
                return True
            else:
                LOGGER.\
                    debug("  Text '%s' does not match element.text '%s'",
                          text, element.text)
                LOGGER.error("  Matching text not found.")
                return False
        except (selenium_exceptions.ElementNotVisibleException,
                selenium_exceptions.NoSuchAttributeException,):
            LOGGER.error("  Element has no visible Text")
            return False

    @staticmethod
    def click_element(element):
        try:
            element.click()
            LOGGER.info("  Element clicked")
            return True
        except (selenium_exceptions.ElementClickInterceptedException,
                selenium_exceptions.ElementNotVisibleException,):
            LOGGER.error("  Element is not clickable")
            return False

    @staticmethod
    def find_element(context, locator, pattern):
        try:
            element = context.driver.find_element(locator, pattern)
        except selenium_exceptions.NoSuchElementException:
            LOGGER.error("  Element not found")
            return None

        LOGGER.debug("  Element found")
        return element

    @staticmethod
    def get_element_screenshot(context, element=None):

        if not element:
            return Image.open(
                BytesIO(context.driver.get_screenshot_as_png()))

        location = element.location_once_scrolled_into_view
        size = element.size

        viewport_screenshot = Image.open(
            BytesIO(context.driver.get_screenshot_as_png()))
        element_screenshot = viewport_screenshot.\
            crop((location['x'], location['y'],
                  location['x'] + size['width'],
                  location['y'] + size['height'],))

        return element_screenshot


class Visit(SeleniumCommand):
    def __init__(self, url, timeout=10, expected_result=200,
                 description="visit"):
        self.url = url
        self.timeout = timeout
        self.expected_result = expected_result
        self.description = description

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        LOGGER.info("  Visiting '%s'", self.url)
        self.driver.get(self.url)

        r = requests.get(self.url)
        if r.status_code != self.expected_result:
            LOGGER.error("  HTTP Status Code '%s' is different " +
                         "from the expected '%s'", r.status_cod, self.url)
            return False

        LOGGER.info("  HTTP Status Code is same as expected '%s'",
                    r.status_code)
        return True


class CheckText(SeleniumCommand):
    def __init__(self, locator, pattern, text='', expected_result=True,
                 description="check text"):
        self.pattern = pattern
        self.locator = locator
        self.text = text
        self.expected_result = expected_result
        self.description = description

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        element = SeleniumCommand.\
            find_element(selenium_ctx, self.locator, self.pattern)
        if not element:
            return False

        result = SeleniumCommand.check_element_text(element, self.text)
        return result == self.expected_result


class CheckScreenshot(SeleniumCommand):
    def __init__(self, ref_img, output_dir='/tmp', locator=None,
                 pattern=None, mask_img=None, expected_result=True,
                 threshold=0.0, rm_images_on_success=True,
                 description="check screenshot"):
        def basename_with_suffix(filename, suffix):
            basename = os.path.basename(filename)
            head, sep, tail = basename.rpartition('.')
            if sep:
                return head + sep + suffix + '.' + tail

            return tail + '.' + suffix

        self.pattern = pattern
        self.locator = locator
        self.reference_img_path = ref_img
        self.mask_img_path = mask_img
        self.expected_result = expected_result
        self.threshold = threshold
        self.rm_images_on_success = rm_images_on_success
        self.description = description

        # Output files
        self.diff_img_path = os.path.join(
            output_dir, basename_with_suffix(ref_img, 'diff'))
        self.test_img_path = os.path.join(
            output_dir, basename_with_suffix(ref_img, 'test'))

    def compare_cmd_magick_v7(test_img, ref_img, mask_img, diff_img):
        """
        This compare command uses the --read-mask option that works only with
        ImagMagick V7.0.3.9+. That option allows a mask to be used for limiting
        the regions to be compared.
        """
        cmd = ('compare',
               '-verbose',
               '-metric',
               'RMSE',
               '-highlight-color',
               'Red',
               '-compose',
               'Src',
               '-read-mask' if mask_img else None,
               mask_img if mask_img else None,
               test_img,
               ref_img,
               diff_img,
               )
        return ' '.join(list(filter(None, cmd)))

    def compare_cmd_magick_v6(test_img, ref_img, mask_img, diff_img):
        """
        This compare command does not use the option --read-mask that is
        available only on ImageMagic V7.0.3.9+. It has been tested with
        ImageMagick V6.0 and works fine. The strategy being used here is to
        multiply test and ref by the same B/W mask, in which black regions will
        be ignored during the comparison.
        """
        def apply_mask_cmd(img, mask=None):
            if not mask:
                return (img,)

            return (
                '<(composite -compose Multiply %s %s png:)' % (img, mask),)

        cmd = ('compare',
               '-verbose',
               '-metric',
               'RMSE',
               '-highlight-color',
               'Red',
               '-compose',
               'Src',
               )
        cmd += apply_mask_cmd(test_img, mask_img)
        cmd += apply_mask_cmd(ref_img, mask_img)
        cmd += (diff_img,)

        return ' '.join(list(cmd))

    def compare_images(self, current_img_path, reference_img_path,
                       mask_img_path, diff_img_path, threshold):
        def exec_cmd(cmd):
            LOGGER.debug('    cmd: $ %s', cmd)
            process = subprocess.Popen(cmd, universal_newlines=True,
                                       executable='/bin/bash', shell=True,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            process_stdout, process_stderr = process.communicate()
            LOGGER.debug('  Results:')
            LOGGER.debug('    return code: %s', process.returncode)
            LOGGER.debug('    stdout: %s', process_stdout.strip())
            LOGGER.debug('    stderr: %s', process_stderr.strip())

            return (process.returncode, process_stdout, process_stderr,)

        def cmd_failed(ret):
            """
            ImageMagick Compare command returns 2 if the command failed, 1 if
            the images are dissimilar and 0 if the images are similar. This
            function checks if the return code represents an error, returning
            True if so.
            """
            MAGICK_CMP_ERROR = 2
            return ret == MAGICK_CMP_ERROR

        LOGGER.debug('  Comparing images with ImageMagick v7 API...')
        ret, stdout, stderr = exec_cmd(
            CheckScreenshot.compare_cmd_magick_v7(current_img_path,
                                                  reference_img_path,
                                                  mask_img_path,
                                                  diff_img_path))

        if cmd_failed(ret):
            LOGGER.debug('  Comparing images with ImageMagick v6 API...')
            ret, stdout, stderr = exec_cmd(
                CheckScreenshot.compare_cmd_magick_v6(current_img_path,
                                                      reference_img_path,
                                                      mask_img_path,
                                                      diff_img_path))

        RESULTS_REGEX = re.compile(".*all:.*\(([0-9\-\.e]*)\).*")
        result = RESULTS_REGEX.search(stderr)

        if not result:
            LOGGER.error("   Error processing the output")
            return False

        difference = float(result.group(1))
        if difference > threshold:
            LOGGER.error("  Resulting difference (%s) above threshold (%s)",
                         difference, threshold)
            LOGGER.error("  Element's screenshot does not match the reference."
                         "  See %s for a visual representation of the "
                         "  differences.", diff_img_path)
            return False

        LOGGER.info("  Resulting difference (%s) below threshold (%s)",
                    difference, threshold)
        LOGGER.info("  Element's screenshot matches the reference")
        return True

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        element = None
        if self.locator and self.pattern:
            element = SeleniumCommand.\
                find_element(selenium_ctx, self.locator, self.pattern)
            if not element:
                return False

        screenshot = SeleniumCommand.\
            get_element_screenshot(selenium_ctx, element)

        try:
            screenshot.save(self.test_img_path, format='PNG')
        except (FileNotFoundError, PermissionError) as e:
            LOGGER.error(
                "  Unable to save the screenshot to '%s'. Does the " +
                " directory exist and can you write to it? Error: %s",
                self.test_img_path, e)
            return False

        result = self.compare_images(self.test_img_path,
                                     self.reference_img_path,
                                     self.mask_img_path, self.diff_img_path,
                                     self.threshold)

        if result != self.expected_result:
            return False

        # Remove diff and test images if there's a match
        if self.rm_images_on_success:
            silent_remove(self.diff_img_path)
            silent_remove(self.test_img_path)

        return True


class Click(SeleniumCommand):
    def __init__(self, locator, pattern, description="click"):
        self.pattern = pattern
        self.locator = locator
        self.description = description

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        element = SeleniumCommand.\
            find_element(selenium_ctx, self.locator, self.pattern)
        if element:
            return SeleniumCommand.click_element(element)

        return False


class Back(SeleniumCommand):
    def __init__(self, description="back"):
        self.description = description

    def exec(self, selenium_ctx):
        super().exec(selenium_ctx)

        self.driver.back()
        LOGGER.info("  Went back")

        return True


class ShExpect():
    BASH_PATTERN = 'test_run_pr1:#'
    COMMAND_RESULT_PATTERN = re.compile('^([0-9]+)', re.M)
    OUTPUT_VARIABLE = 'cmd_output'
    COMMAND_OUTPUT_DELIM = ':test_run_cmd_out:'
    COMMAND_OUTPUT_PATTERN = re.compile(
        r'^%s(.*)%s\s+%s' %
        (COMMAND_OUTPUT_DELIM, COMMAND_OUTPUT_DELIM, BASH_PATTERN),
        re.M | re.S)

    def __init__(self, cmd, expected_output=None, expected_result=0,
                 description=None):
        self.cmd = cmd
        self.expected_result = expected_result
        self.expected_output = expected_output
        self.description = description or "sh expect %s" % cmd

    def exec(self, pexpect_ctx):
        self.client = pexpect_ctx.client

        LOGGER.info("Executing command '%s'", self.cmd)
        try:
            self.client.sendline('%s=$(%s 2>&1)' %
                                 (self.OUTPUT_VARIABLE, self.cmd))
            self.client.expect(self.BASH_PATTERN)

            self.client.sendline('echo $?')
            self.client.expect(self.COMMAND_RESULT_PATTERN)
            result = int(self.client.match.group(1))

            self.client.sendline(
                'echo "%s${%s}%s"' % (self.COMMAND_OUTPUT_DELIM,
                                      self.OUTPUT_VARIABLE,
                                      self.COMMAND_OUTPUT_DELIM))
            self.client.expect(self.COMMAND_OUTPUT_PATTERN)
            out = self.client.match.group(1)

            if result != self.expected_result:
                LOGGER.error("The command '%s' returned the code '%d', " +
                             "but the expected code is '%d'" +
                             "\nCommand output: '%s'",
                             self.cmd, result, self.expected_result, out)
                return False
            if self.expected_output is not None and \
                    re.search(self.expected_output, out) is None:
                LOGGER.error("Wrong output for command '%s'. " +
                             "Expected '%s'\nReceived '%s'",
                             self.cmd, self.expected_output, out)
                return False
        except pexpect.exceptions.TIMEOUT:
            LOGGER.error("Timeout for command '%s'", self.cmd)
            return False
        except pexpect.exceptions.EOF:
            LOGGER.error("Lost connection with docker. Aborting")
            return False

        LOGGER.info("  Command result and command output matches " +
                    "the expected result. (Return code: %d)", result)
        return True


class FuegoContainer:
    def __init__(self, install_script, image_name, container_name,
                 jenkins_port, rm_after_test=True):
        self.install_script = install_script
        self.image_name = image_name
        self.container_name = container_name
        self.jenkins_port = jenkins_port
        self.rm_after_test = rm_after_test

    def delete(self):
        if self.container:
            if self.rm_after_test:
                LOGGER.info("Removing Container")
                self.container.remove(force=True)
            else:
                LOGGER.info("Not Removing the test container")

    def install(self):
        self.docker_client = docker.APIClient()
        self.container = self.setup_docker()

        return self.container

    def stop(self):
        self.container.remove(force=True)
        self.container = None

    def setup_docker(self):
        def this_container_id():
            with open('/proc/self/cgroup', 'rt') as f:
                f_text = f.read()

                if 'docker' not in f_text:
                    return None

                for c in self.docker_client.containers(quiet=True):
                    if c['Id'] in f_text:
                        return c['Id']

                return None

        def map_to_host(mounts, container_id):
            host_mounts = self.docker_client.\
                inspect_container(container_id)['Mounts']

            for mount in mounts:
                LOGGER.debug("  Trying to find '%s' mountpoint in the host",
                             mount['source'])
                for host_mount in host_mounts:
                    if mount['source'].startswith(host_mount['Destination']):
                        mount['source'] = mount['source'].\
                            replace(host_mount['Destination'],
                                    host_mount['Source'], 1)
                        LOGGER.debug("    Found: '%s'", mount['source'])
                        break
                else:
                    LOGGER.debug("    Not Found")
                    mount['source'] = None

        cmd = './%s %s' % (self.install_script, self.image_name)
        LOGGER.info("Running '%s' to install the docker image. " +
                    "This may take a while....", cmd)
        status = subprocess.call(cmd, shell=True)

        LOGGER.debug("Install output code: %s", status)

        if status != 0:
            LOGGER.error("Installation Failed")
            return None

        LOGGER.info("Installation Complete")
        LOGGER.info("Creating the container '%s'..." % self.container_name)

        docker_client = docker.from_env()
        containers = docker_client.containers.list(
            all=True, filters={'name': self.container_name})
        if containers:
            LOGGER.debug("Container already exists. Removing it, so a new " +
                         "one can be created")
            containers[0].remove(force=True)

        mounts = [
            {'source': os.path.abspath('./fuego-rw'),
             'destination':  '/fuego-rw',
             'readonly': False,
             },
            {'source': os.path.abspath('./fuego-ro'),
             'destination':  '/fuego-ro',
             'readonly': True,
             },
            {'source': os.path.abspath('../fuego-core'),
             'destination':  '/fuego-core',
             'readonly': True,
             },
        ]

        our_id = this_container_id()

        if our_id:
            LOGGER.debug("Running inside the Docker container %s", our_id)
            map_to_host(mounts, our_id)
        else:
            LOGGER.debug("Not running inside a Docker container")

        LOGGER.debug("Creating container with the following mountpoints:")
        LOGGER.debug("  %s", mounts)

        container = docker_client.containers.create(
            self.image_name,
            stdin_open=True, tty=True, network_mode='bridge',
            mounts=[docker.types.Mount(m['destination'],
                                       m['source'],
                                       type='bind',
                                       read_only=m['readonly'])
                    for m in mounts],
            name=self.container_name, command='/bin/bash')

        LOGGER.info("Container '%s' successfully created", self.container_name)
        return container

    def is_running(self):
        try:
            container_status = self.docker_client.\
                inspect_container(self.container_name)['State']['Running']
        except KeyError:
            return False

        return container_status

    def get_ip(self):
        container_addr = None

        if not self.is_running():
            return None

        def fetch_ip():
            nonlocal container_addr
            try:
                container_addr = self.docker_client.\
                    inspect_container(
                        self.container_name)['NetworkSettings']['IPAddress']
            except KeyError:
                return False

            if container_addr is None:
                return False
            else:
                return True

        if not loop_until_timeout(fetch_ip, timeout=10):
            LOGGER.error("Could not fetch the container IP address")
            return None

        return container_addr

    def get_url(self):
        container_addr = self.get_ip()

        if container_addr:
            return 'http://%s:%s/fuego/' % (container_addr, self.jenkins_port)
        else:
            return None


class PexpectContainerSession():
    def __init__(self, container, start_script, timeout):
        self.container = container
        self.start_script = start_script
        self.timeout = timeout

    def start(self):
        def prompt_password_if_needed():
            try:
                PASSWORD_PROMPT_REGEX = re.compile("password for.+$")
                self.client.expect(PASSWORD_PROMPT_REGEX, timeout=1)
            except pexpect.exceptions.TIMEOUT:
                pass
            else:
                prompt = self.client.before + self.client.match.group(0)
                LOGGER.info("  Start script requires a password...")
                password = getpass(prompt=prompt)
                self.client.sendline(password)

        LOGGER.info(
            "Starting container '%s'...", self.container.container_name)
        self.client = pexpect.spawnu(
            '%s %s' % (self.start_script, self.container.container_name),
            echo=False, timeout=self.timeout)

        prompt_password_if_needed()

        PexpectContainerSession.set_ps1(self.client)

        LOGGER.info("Container started with the ip '%s'",
                    self.container.get_ip())
        if not self.wait_for_jenkins():
            return False

        return True

    def __del__(self):
        self.client.terminate(force=True)

    @staticmethod
    def set_ps1(client):
        client.sendline('export PS1="%s"' % ShExpect.BASH_PATTERN)
        client.expect(ShExpect.BASH_PATTERN)

    def wait_for_jenkins(self):
        def ping_jenkins():
            try:
                conn = http.client.HTTPConnection(container_addr,
                                                  self.container.jenkins_port,
                                                  timeout=30)
                conn.request('HEAD', '/fuego/')
                resp = conn.getresponse()
                version = resp.getheader('X-Jenkins')
                status = resp.status
                conn.close()
                LOGGER.debug(
                    "  HTTP Response code: '%d' - Jenkins Version: '%s'",
                    status, version)
                if status == http.client.OK and version is not None:
                    return True
            except (ConnectionRefusedError, OSError):
                return False

            return False

        container_addr = self.container.get_ip()
        if container_addr is None:
            return False
        LOGGER.info("Waiting for jenkins on '%s:%d'...",
                    container_addr, self.container.jenkins_port)
        if not loop_until_timeout(ping_jenkins, timeout=60):
            LOGGER.error("Could not connect to jenkins")
            return False

        LOGGER.info("Jenkins is running")
        return True


class SeleniumContainerSession():
    def __init__(self, container, viewport_width=1920, viewport_height=1080):
        self.container = container
        self.driver = None
        self.root_url = container.get_url()
        self.viewport_width = viewport_width
        self.viewport_height = viewport_height

    def start(self):
        LOGGER.info("Starting a Selenium Session on %s...", self.root_url)
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('no-sandbox')
        options.add_argument(
            'window-size= %sx%s' % (self.viewport_width, self.viewport_height))
        options.add_experimental_option(
            'prefs', {'intl.accept_languages': 'en,en_US'})

        self.driver = webdriver.Chrome(chrome_options=options)
        self.driver.implicitly_wait(3)

        self.driver.get(self.root_url)

        LOGGER.info("Selenium Session started successfully")
        return True

    def __del__(self):
        if self.driver:
            self.driver.quit()


def main():
    DEFAULT_TIMEOUT = 120
    DEFAULT_IMAGE_NAME = 'fuego-release'
    DEFAULT_CONTAINER_NAME = 'fuego-release-container'
    DEFAULT_INSTALL_SCRIPT = 'install.sh'
    DEFAULT_START_SCRIPT = 'fuego-host-scripts/docker-start-container.sh'
    DEFAULT_JENKINS_PORT = 8090
    DEFAULT_OUTPUT_DIR = '/tmp'
    DEFAULT_LOCALE = 'en_US.UTF-8'

    @atexit.register
    def cleanup():
        try:
            container.delete()
        except NameError:
            pass

    def get_abs_working_dirs():
        abs_install_dir = os.path.abspath(args.install_dir)

        if args.resources_dir:
            abs_resources_dir = os.path.abspath(args.resources_dir)
        else:
            abs_resources_dir = abs_install_dir

        if args.output_dir:
            abs_output_dir = os.path.abspath(args.output_dir)
        else:
            abs_output_dir = abs_output_dir

        return abs_install_dir, abs_resources_dir, abs_output_dir

    def execute_tests(timeout):
        LOGGER.info("Starting tests...")

        ctx_mapper = {
            ShExpect: pexpect_session,
            SeleniumCommand: selenium_session
        }

        tests_ok = True
        for cmd_id, cmd in enumerate(COMMANDS_TO_TEST, 1):
            for base_class, ctx in ctx_mapper.items():
                if isinstance(cmd, base_class):
                    LOGGER.report("run %s %s", cmd_id, cmd.description)
                    if not cmd.exec(ctx):
                        tests_ok = False
                        LOGGER.report("not ok %s", cmd_id)
                        break
                    LOGGER.report("ok %s", cmd_id)

        if tests_ok:
            LOGGER.info("All tests finished with SUCCESS")
        else:
            LOGGER.info("Tests finished with ERRORS")

        return tests_ok

    parser = argparse.ArgumentParser()
    parser.add_argument('install_dir', help="The directory where the install "
                        "script resides.", type=str)
    parser.add_argument('-d', '--resources_dir', help="Root directory for " +
                        " test assets, as reference screenshots and other "
                        " resources. Defaults to install_dir.",
                        default=None, type=str)
    parser.add_argument('-o', '--output_dir', help="Location in which " +
                        "temporary files generated by test cases will be " +
                        "stored. Defaults to '%s'." % DEFAULT_OUTPUT_DIR,
                        default=DEFAULT_OUTPUT_DIR, type=str)
    parser.add_argument('-s', '--install-script',
                        help="The script that will be used to install the " +
                        "docker image. Defaults to '%s'" %
                        DEFAULT_INSTALL_SCRIPT, default=DEFAULT_INSTALL_SCRIPT,
                        type=str)
    parser.add_argument('-a', '--start-script',
                        help="The script used to start the container. " +
                        "Defaults to '%s'" % DEFAULT_START_SCRIPT,
                        default=DEFAULT_START_SCRIPT,
                        type=str)
    parser.add_argument('-i', '--image-name', default=DEFAULT_IMAGE_NAME,
                        help="The image name that should be used. " +
                        "Defaults to '%s'" % DEFAULT_IMAGE_NAME, type=str)
    parser.add_argument('-c', '--container-name',
                        default=DEFAULT_CONTAINER_NAME,
                        help="The container name that should be used for " +
                        "the test. Defaults to '%s'" % DEFAULT_CONTAINER_NAME,
                        type=str)
    parser.add_argument('-t', '--timeout', help="The timeout value for " +
                        "commands. Defaults to '%s'" % DEFAULT_TIMEOUT,
                        default=DEFAULT_TIMEOUT, type=int)
    parser.add_argument('-j', '--jenkins-port',
                        help="The port where the jenkins is running on the " +
                        "test container. Defaults to '%s'" %
                        DEFAULT_JENKINS_PORT, default=DEFAULT_JENKINS_PORT,
                        type=int)
    parser.add_argument('--no-rm-container',
                        help="Do not remove the container after tests are " +
                        "complete.",
                        dest='rm_test_container',
                        default=True, action='store_false')
    args = parser.parse_args()

    args.install_dir, args.resources_dir, args.output_dir = \
        get_abs_working_dirs()

    LOGGER.debug("Changing working dir to '%s'", args.install_dir)
    os.chdir(args.install_dir)

    LOGGER.debug("Setting locale to '%s' to standardize Pexpect output",
                 DEFAULT_LOCALE)
    os.environ['LANG'] = DEFAULT_LOCALE

    container = FuegoContainer(args.install_script, args.image_name,
                               args.container_name, args.jenkins_port,
                               rm_after_test=args.rm_test_container)
    if not container.install():
        return 1

    pexpect_session = PexpectContainerSession(container, args.start_script,
                                              args.timeout)
    if not pexpect_session.start():
        LOGGER.error("Pexpect session failed to start")
        return 1

    selenium_session = SeleniumContainerSession(container, viewport_width=1920,
                                                viewport_height=1080)

    if not selenium_session.start():
        LOGGER.error("Selenium session failed to start")
        return 1

    if os.getcwd != args.resources_dir:
        LOGGER.debug("Changing working dir to '%s'", args.resources_dir)
        os.chdir(args.resources_dir)

    COMMANDS_TO_TEST = [
        # Set Selenium Browser root
        Visit(url=container.get_url(),
              description="visit " + container.get_url()),

        # Compare screenshot of the full viewport ignoring an area
        CheckScreenshot(ref_img='screenshots/full_screenshot.png',
                        rm_images_on_success=False,
                        mask_img='screenshots/full_screenshot_mask.png',
                        threshold=0.05,
                        description="check full screenshot"),

        # Add Nodes
        ShExpect('ftc add-nodes docker'),
        ShExpect('ftc list-nodes -q', r'.*docker.*'),
        CheckText(By.ID, 'executors', text='master',
                  description="check text executors master"),
        CheckText(By.ID, 'executors', text='docker',
                  description="check text executors docker"),

        # Add Fuego TestPlan
        ShExpect('ftc add-jobs -b docker -p testplan_fuego_tests'),
        ShExpect('ftc list-jobs', r'.*docker\.testplan_fuego_tests\.batch.*'),

        Click(By.PARTIAL_LINK_TEXT, 'docker',
              description="click docker"),
        CheckText(By.ID, 'projectstatus',
                  text='docker.testplan_fuego_tests.batch',
                  description="check text projectstatus fuego batch"),
        Back(),

        # Install Views
        ShExpect('ftc add-view batch .*.batch'),
        CheckText(By.ID, 'projectstatus-tabBar',
                  text='batch',
                  description="check text projectstatus tab bar batch"),

        # Start a Test through Command Line
        ShExpect('ftc build-jobs *.*.Functional.fuego_board_check'),
        CheckText(By.ID, 'buildQueue',
                  text='Functional.fuego_board_check',
                  description="check text build queue fuego board check"),

        # Start a Test through Jenkins UI (Xpath)
        Click(By.XPATH,
              '//img[@title="Schedule a build for ' +
              'docker.default.Functional.hello_world"]',
              description="click img build hello world"),
        CheckText(By.ID, 'executors',
                  text='docker.default.Functional.hello_world',
                  description="check text executors hello world"),

        # Compare screenshot of an element of Jenkins UI
        CheckScreenshot(ref_img='screenshots/side-panel-tasks.png',
                        output_dir=args.output_dir,
                        locator=By.ID, pattern='tasks',
                        rm_images_on_success=True,
                        threshold=0.1,
                        description="check screenshot side-panel-tasks"),

        # Compare screenshot of an element of Jenkins UI ignoring an area
        CheckScreenshot(ref_img='screenshots/footer.png',
                        output_dir=args.output_dir,
                        locator=By.CLASS_NAME, pattern='col-md-18',
                        rm_images_on_success=False,
                        mask_img='screenshots/footer_mask.png',
                        threshold=0.1,
                        description="check screenshot footer"),
    ]

    if not execute_tests(args.timeout):
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
