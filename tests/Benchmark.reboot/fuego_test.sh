NEED_KCONFIG="CONFIG_PRINTK_TIME=y"

# most tmp directories on the target are ephemeral
# for this test, however, the tmp/log directory on the target
# needs to persist over a reboot.
FUEGO_TARGET_TMP=${BOARD_TESTDIR}/tmp

function test_deploy {
    put $TEST_HOME/get_reboot_time.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    # MAX_REBOOT_RETRIES can be defined in the board's file.
    # Otherwise, the default is 20 retries
    retries=${MAX_REBOOT_RETRIES:-20}
    reboot_wait_time=${BENCHMARK_REBOOT_WAIT_TIME:-40}

    if [ "$BENCHMARK_REBOOT_TYPE" = "hard" ] ; then
        ov_board_control_reboot
        sleep $reboot_wait_time
    else
        target_reboot $retries
    fi
    # sleep a bit more, just because we're paranoid
    # remote system may have networking up, but need more
    # time to mount filesystems
    sleep 10
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./get_reboot_time.sh"
}
