tarball=glib-$FUNCTIONAL_GLIB_VERSION.tar.bz2

function test_build {
    if [[ $FUNCTIONAL_GLIB_VERSION =~ "2.22" ]] ; then
        patch -p0 -N -s < $TEST_HOME/glib-strfuncs.patch
    fi

    # Don't run tests on the build host
    find . -name 'Makefile*' | xargs sed -i '/@test -z "${TEST_PROGS}" || ${GTESTER} --verbose ${TEST_PROGS}/d'
    echo -e "glib_cv_stack_grows=no \nglib_cv_uscore=no \nac_cv_func_posix_getpwuid_r=no \nac_cv_func_nonposix_getpwuid_r=no \nac_cv_func_posix_getgrgid_r=no" > config.cross
    # get updated config.sub and config.guess files, so configure
    # doesn't reject new toolchains
    cp /usr/share/misc/config.{sub,guess} .

    if [[ $FUNCTIONAL_GLIB_VERSION =~ "2.22" ]] ; then
        ./configure --prefix=`pwd`/build --cache-file=config.cross --host=$HOST --target=$HOST --build=`uname -m`-linux-gnu CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CXXFLAGS="$CXXFLAGS"
        make 2>&1
        cd tests && make test; cd -
        cd glib/tests && make test; cd -
    fi

    if [[ $FUNCTIONAL_GLIB_VERSION =~ "2.46" ]] || [[ $FUNCTIONAL_GLIB_VERSION =~ "2.48" ]] ; then
        ./configure --prefix=`pwd`/build  --enable-always-build-tests --cache-file=config.cross --host=$HOST --target=$HOST --build=`uname -m`-linux-gnu CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CXXFLAGS="$CXXFLAGS"
        make 2>&1
        cd tests && make; cd -
        cd glib/tests && make; cd -
    fi
}

function test_deploy {
    put `find . -name .libs | xargs -IDIRS find DIRS -not -type d -not -name '*.so*' -not -name '*.la*' -perm /u+x`  $BOARD_TESTDIR/fuego.$TESTDIR/

    if [[ $FUNCTIONAL_GLIB_VERSION =~ "2.22" ]] ; then
        #glib2.22 contexts need gio/tests/Makefile.am
        put gio/tests/Makefile.am $BOARD_TESTDIR/fuego.$TESTDIR/
    fi

    if [[ $FUNCTIONAL_GLIB_VERSION =~ "2.46" ]] || [[ $FUNCTIONAL_GLIB_VERSION =~ "2.48" ]] ; then
        #glib(2.46 2.48) contexts need gio/tests/contexts.c
        put gio/tests/contexts.c $BOARD_TESTDIR/fuego.$TESTDIR/

        #glib(2.46 2.48) keyfile need glib/tests/keyfiletest.ini,glib/tests/keyfile.c,glib/tests/pages.ini
        put glib/tests/keyfiletest.ini glib/tests/keyfile.c glib/tests/pages.ini $BOARD_TESTDIR/fuego.$TESTDIR/

        #glib(2.46 2.48) g-icon need gio/tests/g-icon.c
        put gio/tests/g-icon.c $BOARD_TESTDIR/fuego.$TESTDIR/
    fi
}

# Excluded tests
# --------------
# gtester - test runner
# echo-server - requires wrapper script
# errorcheck-mutex-test - requires wrapper script
# timeloop - hangs
# unicode-normalize - requires external data
# glib-genmarshal - requires external ???
# socket-server, socket-client - require wrapper script
# send-data - requires wrapper script
# testglib - ???
# testing - crashes
# gobject-query - requires external data
# desktop-app-info - requires supported desktop ???
# resolver - requires wrapper script
# unicode-collate - requires some locale data
# markup-subparser
# live-g-file
# filter-streams

function test_run {
    if [[ $FUNCTIONAL_GLIB_VERSION =~ "2.22" ]] ; then
        report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./gtester  ./objects ./readwrite ./g-file ./fileutils ./data-output-stream ./signal3 ./properties ./rand ./memory-input-stream ./sleepy-stream  ./g-icon ./array-test ./strfuncs ./testgdateparser ./srvtarget ./threadtests ./keyfile ./string ./hostutils ./memory-output-stream ./objects2 ./closures ./scannerapi ./testgdate ./data-input-stream ./signal2 ./signal1 ./testgobject ./g-file-info ./simple-async-result ./buffered-input-stream ./contexts ./printf ./properties2 ./unix-streams "
    fi

    if [[ $tarball =~ "2.46" ]] || [[ $FUNCTIONAL_GLIB_VERSION =~ "2.48" ]] ; then
        report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./gtester ./readwrite  ./g-file  ./data-output-stream  ./properties ./rand ./memory-input-stream ./sleepy-stream  ./array-test ./strfuncs  ./srvtarget  ./string ./hostutils ./memory-output-stream   ./scannerapi ./data-input-stream  ./g-file-info ./simple-async-result ./buffered-input-stream   ./test-printf  ./unix-streams  ./contexts ./keyfile ./g-icon "
    fi
}

function test_processing {
	#log_compare "$TESTDIR" "196" "OK" "p"
	#log_compare "$TESTDIR" "0" "ERROR" "n"
	true
}


