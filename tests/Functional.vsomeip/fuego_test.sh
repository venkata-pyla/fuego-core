tarball=vSomeIP.tar.bz2

function test_build {
	cd ./run_test
	sed -i "s/192.168.160.212/${IPADDR}/g" `grep -rl "192.168.160.212" .`
#	sed -i "s/192.168.160.99/${IPADDR}/g" `grep -rl "192.168.160.99" .`

	cd ../examples
	$CXX -std=gnu++11 notify-sample.cpp  -rdynamic -lpthread -lvsomeip -o notify-sample
	$CXX -std=gnu++11 request-sample.cpp  -rdynamic -lpthread -lvsomeip -o request-sample
	$CXX -std=gnu++11 response-sample.cpp  -rdynamic -lpthread -lvsomeip -o response-sample
	$CXX -std=gnu++11 subscribe-sample.cpp  -rdynamic -lpthread -lvsomeip -o subscribe-sample

	cd ../test/application_tests
	$CXX -std=gnu++11 application_test.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o application_test
	cp application_test ../../run_test
	cd ../configuration_tests
	$CXX -std=gnu++11 configuration-test.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o configuration-test
	cp configuration-test ../../run_test
	cd ../magic_cookies_tests
	$CXX -std=gnu++11 magic_cookies_test_client.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o magic_cookies_test_client
	$CXX -std=gnu++11 magic_cookies_test_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o magic_cookies_test_service
	cp magic_cookies_test_client ../../run_test
	cp magic_cookies_test_service ../../run_test
	cd ../header_factory_tests
	$CXX -std=gnu++11 header_factory_test.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o header_factory_test
	$CXX -std=gnu++11 header_factory_test_client.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o header_factory_test_client
	$CXX -std=gnu++11 header_factory_test_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o header_factory_test_service
	cp header_factory_test ../../run_test
	cp header_factory_test_client ../../run_test
	cp header_factory_test_service ../../run_test
	cd ../routing_tests
	$CXX -std=gnu++11 external_local_routing_test_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o external_local_routing_test_service
	$CXX -std=gnu++11 local_routing_test_client.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o local_routing_test_client
	$CXX -std=gnu++11 local_routing_test_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o local_routing_test_service
	cp external_local_routing_test_service ../../run_test
	cp local_routing_test_client ../../run_test
	cp local_routing_test_service ../../run_test
	cd ../payload_tests
	$CXX -std=gnu++11 payload_test_client.cpp stopwatch.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o payload_test_client
	$CXX -std=gnu++11 payload_test_service.cpp stopwatch.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o payload_test_service
	cp payload_test_client ../../run_test
	cp payload_test_service ../../run_test
	cd ../big_payload_tests
	$CXX -std=gnu++11 big_payload_test_client.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o big_payload_test_client
	$CXX -std=gnu++11 big_payload_test_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o big_payload_test_service
	cp big_payload_test_client ../../run_test
	cp big_payload_test_service ../../run_test
	cd ../client_id_tests
	$CXX -std=gnu++11 client_id_test_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o client_id_test_service
	cp client_id_test_service ../../run_test

	cd ../../examples/hello_world
	$CXX -std=gnu++11 hello_world_client.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o hello_world_client
	$CXX -std=gnu++11 hello_world_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../ -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o hello_world_service

#	g++ -std=gnu++11 hello_world_client.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../../interface -L../../build -L../../build/gtest -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o hello_world_client
#	g++ -std=gnu++11 hello_world_service.cpp -rdynamic -DBOOST_LOG_DYN_LINK=1 -I../../interface -L../../build -L../../build/gtest -lpthread -lvsomeip -lgtest -lboost_log_setup -lboost_log -lboost_system -lboost_thread -o hello_world_service

}

function test_deploy {
	pwd
	put -r ./examples  $BOARD_TESTDIR/fuego.$TESTDIR/
	put -r ./run_test  $BOARD_TESTDIR/fuego.$TESTDIR/
	put -r ./config  $BOARD_TESTDIR/fuego.$TESTDIR/

#	put -r ./examples  /home/bi/vsomeip/
#	put -r ./run_test  /home/bi/vsomeip/
#	put -r ./config  /home/bi/vsomeip/

#	mkdir build
#	cd build
#	export GTEST_ROOT=./gtest-1.7.0/
#	cmake -DTEST_IP_MASTER=192.168.160.212 -DTEST_IP_SLAVE=192.168.7.2 ..
#	make check
#	cmake -DTEST_IP_MASTER=192.168.160.212 -DTEST_IP_SLAVE=192.168.160.237 ..

#	export LD_LIBRARY_PATH=../../../build:$LD_LIBRARY_PATH
#	export LD_LIBRARY_PATH=../../build:$LD_LIBRARY_PATH
	
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod -R 777 *; \
	cd run_test; \

	if ./configuration-test -someip ../config/vsomeip-test.json; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi; \
	if ./application_test_starter.sh; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi; \
	(./magic_cookies_test_service_start.sh &); \
	if ./magic_cookies_test_client_start.sh; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi; \
        killall magic_cookies_test_service;\

	if ./header_factory_test; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi; \
	if ./header_factory_test_send_receive_starter.sh; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi; \
	if ./local_routing_test_starter.sh; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi; \

	(./external_local_routing_test_starter.sh &); \
	for (( ; ; ))
	do
		if [ -f case7.txt ] ; then
			if cat case7.txt | grep 'Please now run:external_local_routing_test_client_external_start.sh'
			then echo 'log is found'
			break
			fi
		fi
	done
	if ./external_local_routing_test_client_external_start.sh; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi; \
killall external_local_routing_test_service;\	
killall local_routing_test_client:\

	if ./local_payload_test_starter.sh; then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi; \
	(./external_local_payload_test_service_start.sh &); \
        sleep 2; \
	if ./external_local_payload_test_client_local_start.sh; then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi; \
killall payload_test_service;\

	(./external_local_payload_test_client_external_starter.sh &); \
	for (( ; ; ))
	do
		if [ -f case10.txt ] ; then
			if cat case10.txt | grep 'Please now run:external_local_payload_test_client_external_start.sh'
			then echo 'log is found'
			break
			fi
		fi
	done
	if ./external_local_payload_test_client_external_start.sh; then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi; \
killall payload_test_service;\

	(./external_local_payload_test_client_local_and_external_starter.sh &); \
	for (( ; ; ))
	do
		if [ -f case11.txt ] ; then
			if cat case11.txt | grep 'Please now run:external_local_payload_test_client_external_start.sh'
			then echo 'log is found'
			break
			fi
		fi
	done
	if ./external_local_payload_test_client_external_start.sh; then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi; \
killall payload_test_service;\
killall payload_test_client;\
	
	(./big_payload_test_service_local_start.sh &); \
	if ./big_payload_test_client_local_start.sh; then echo 'TEST-12 OK'; else echo 'TEST-12 FAIL'; fi; \
killall big_payload_test_service;\

	cd ../examples/
	(env VSOMEIP_CONFIGURATION=../config/vsomeip-local.json VSOMEIP_APPLICATION_NAME=client-sample ./request-sample &); \
	(env VSOMEIP_CONFIGURATION=../config/vsomeip-local.json VSOMEIP_APPLICATION_NAME=service-sample ./response-sample &); \
	for (( ; ; ))
	do
		if [ -f log_request_sample.txt ] ; then
			if cat log_request_sample.txt | grep 'Received a response from Service'
			then echo 'log is found'
			break
			fi
		fi
	done
	pkill request-sample; \
	pkill response-sample; \
	if cat log_request_sample.txt | grep 'Received a response from Service'; then echo 'TEST-13 OK'; else echo 'TEST-13 FAIL'; fi; \
	if cat log_response_sample.txt | grep 'Received a message with Client/Session'; then echo 'TEST-14 OK'; else echo 'TEST-14 FAIL'; fi; \
	(env VSOMEIP_CONFIGURATION=../config/vsomeip-local.json VSOMEIP_APPLICATION_NAME=client-sample ./subscribe-sample &); \
	(env VSOMEIP_CONFIGURATION=../config/vsomeip-local.json VSOMEIP_APPLICATION_NAME=service-sample ./notify-sample &); \
	for (( ; ; ))
	do
		if [ -f log_subscribe_sample.txt ] ; then
			if cat log_subscribe_sample.txt | grep 'Received a notification for Event'
			then echo 'log is found'
			break
			fi
		fi
	done
	pkill subscribe-sampl; \
	pkill notify-sample; \
	if cat log_subscribe_sample.txt | grep 'Received a notification for Event'; then echo 'TEST-15 OK'; else echo 'TEST-15 FAIL'; fi; \
	if cat log_notify_sample.txt | grep 'Setting event (Length'; then echo 'TEST-16 OK'; else echo 'TEST-16 FAIL'; fi; \

	cd ../examples/hello_world
	(VSOMEIP_CONFIGURATION=./helloworld-local.json VSOMEIP_APPLICATION_NAME=hello_world_service ./hello_world_service &); \
	if VSOMEIP_CONFIGURATION=./helloworld-local.json VSOMEIP_APPLICATION_NAME=hello_world_client ./hello_world_client; then echo 'TEST-17 OK'; else echo 'TEST-17 FAIL'; fi
	killall hello_world_service"
}

function test_processing {
	P_CRIT="TEST.*OK"
	N_CRIT="TEST.*FAIL"

	log_compare "$TESTDIR" "17" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}




