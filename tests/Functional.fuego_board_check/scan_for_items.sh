#!/bin/sh
# scan_for_items.sh - look for specific items on the target
# support for the following is assumed (and mandatory)
# echo, shell functions, command line redirection

BOARD_TESTDIR=$1
if test -z "$BOARD_TESTDIR" ; then
    echo "Usage scan_for_items.sh \<board_testdir\>"
    exit 1
fi

# $1 = command to check for
# $2 = command name in caps (since we might not have tr)
# show a message about presence of indicated command
check_for_program_msg () {
    cmd_name_caps=$2
    if [ -z "${cmd_name_caps}" ] ; then
        cmd_name_caps="$cmd"
    fi
    if command -v "$1" >/dev/null 2>&1 ; then
        echo "HAS_PROGRAM_${cmd_name_caps}=1"
    else
        echo "HAS_PROGRAM_${cmd_name_caps}=0"
    fi
}

# $1 = command to check for
# $2 = command name in caps
# show a message about presence of indicated command, and set
# RESULT string if the command is missing
check_for_program_msg_fail () {
    if ! check_for_program_msg $1 $2 ; then
        RESULT="FAILURE - Missing '$1'"
    fi
}

set +e

RESULT="SUCCESS"
echo "== Fuego target feature scan results =="
echo "Information:"
echo "  account information: $(id)"
echo "  working directory: $(pwd)"
echo "  PATH: $PATH"
echo "-------------------------------------"
echo "Scan results:"

# you would think that the following could be done in a loop
# but without a sure-fire way to convert to upper-case, no.

check_for_program_msg_fail cat CAT

# used by ov_rootfs_state
check_for_program_msg_fail df DF

check_for_program_msg_fail find FIND

# used by ov_rootfs_state
check_for_program_msg_fail free FREE
check_for_program_msg_fail grep GREP
check_for_program_msg_fail head HEAD

if check_for_program_msg logger LOGGER ; then
    HAS_PROGRAM_LOGGER=1
else
    HAS_PROGRAM_LOGGER=0
fi

if check_for_program_msg logread LOGREAD ; then
    HAS_PROGRAM_LOGREAD=1
else
    HAS_PROGRAM_LOGREAD=0
fi

check_for_program_msg_fail mkdir MKDIR
check_for_program_msg_fail mount MOUNTS

# used by ov_rootfs_state
check_for_program_msg_fail ps PS

check_for_program_msg_fail rm RM

# used by ov_firmware - we should get rid of this usage
check_for_program_msg_fail rmdir RMDIR

# used by ov_rootfs_sync
check_for_program_msg_fail sync SYNC

check_for_program_msg_fail tail TAIL
check_for_program_msg_fail tee TEE
check_for_program_msg_fail touch TOUCH
check_for_program_msg_fail true TRUE
check_for_program_msg_fail mount UMOUNT

# used by ov_firmware
check_for_program_msg_fail uname UNAME

# used by ov_rootfs_state
check_for_program_msg_fail uptime UPTIME

check_for_program_msg_fail xargs XARGS
check_for_program_msg_fail "[" BRACKET

# test for specific arguments:
# tee -a - assumed to be an intrinsic feature of 'tee'
# true vs. /bin/true?
# head -n - assumed to be an intrinsic feature of 'head'
# find -name - assumed to be an intrinsic feature of 'find'
# pkill -9 - assumed to be an intrinsic feature of 'pkill'

# mkdir -p
mkdir -p foo/bar
if test -d foo/bar ; then
   echo HAS_MKDIR_P=1
else
   echo HAS_MKDIR_P=0
   RESULT="FAILURE - mkdir missing -p"
fi
rmdir foo/bar
rmdir foo

# rm -rf
mkdir foo-local-fuego-test
touch foo-local-fuego-test/bar
rm -rf foo-local-fuego-test
if test ! -e foo-local-fuego-test/bar ; then
   echo HAS_RM_RF=1
else
   echo HAS_RM_RF=0
   RESULT="FAILURE - rm missing -rf"
fi
rm foo-local-fuego-test/bar 2>/dev/null
rmdir foo-local-fuego-test 2>/dev/null

# grep -Fv - used by ov_rootfs_state - we should get rid of -F
cat <<eof1 >local-fuego-test-grep
1  foo
2  [foo]
eof1

LINE_COUNT=$(cat local-fuego-test-grep | grep -Fv "  [" | wc -l)
if test $LINE_COUNT -eq "1" ; then
   echo HAS_GREP_F=1
else
   echo HAS_GREP_F=0
   RESULT="FAILURE - grep missing -F"
fi
rm local-fuego-test-grep

# now check for specific binaries and files
# /sbin/reboot, /sbin/true, /sbin/route
# /var/log/messages, /var/log/syslog
# /proc/interrupts, /proc/sys/vm/drop_caches, /proc/$$/oom_score_adj
# and /proc/config.gz

if test -x /sbin/reboot ; then
   echo HAS_SBIN_REBOOT=1
else
   echo HAS_SBIN_REBOOT=0
   RESULT="FAILURE - missing /sbin/reboot"
fi

if test -x /bin/true ; then
   echo HAS_BIN_TRUE=1
else
   echo HAS_BIN_TRUE=0
   RESULT="FAILURE - missing /bin/true"
fi

if test -x /sbin/route ; then
   echo HAS_SBIN_ROUTE=1
else
   echo HAS_SBIN_ROUTE=0
   RESULT="FAILURE - missing /sbin/route"
fi

if test -e /var/log/message ; then
   echo HAS_VAR_LOG_MESSAGES=1
   HAS_VAR_LOG_MESSAGES=1
else
   echo HAS_VAR_LOG_MESSAGES=0
   HAS_VAR_LOG_MESSAGES=0
fi

if test -e /var/log/syslog ; then
   echo HAS_VAR_LOG_SYSLOG=1
   HAS_VAR_LOG_SYSLOG=1
else
   echo HAS_VAR_LOG_SYSLOG=0
   HAS_VAR_LOG_SYSLOG=0
fi

if test -e /proc/interrupts ; then
   echo HAS_PROC_INTERRUPTS=1
else
   echo HAS_PROC_INTERRUPTS=0
   RESULT="FAILURE - missing /proc/interrupts"
fi

if test -e /proc/sys/vm/drop_caches ; then
   echo HAS_PROC_DROP_CACHES=1
else
   echo HAS_PROC_DROP_CACHES=0
fi

if test -e /proc/\$\$/oom_score_adj ; then
   echo HAS_PROC_OOM_SCORE_ADJ=1
else
   echo HAS_PROC_OOM_SCORE_ADJ=0
fi

if test -e /proc/config.gz ; then
   echo HAS_PROC_CONFIG_GZ=1
else
   echo HAS_PROC_CONFIG_GZ=0
fi

##########################################
# now get some purely information stuff
#HAS_MEMORY
MEMORY_LINE=$(free | grep Mem:)
set -- junk $MEMORY_LINE
HAS_MEMORY=$3
echo "HAS_MEMORY=$HAS_MEMORY K"

STORAGE_LINE=$(df $BOARD_TESTDIR | tail -n 1)
set -- junk $STORAGE_LINE
HAS_STORAGE=$3
echo "HAS_STORAGE=$HAS_STORAGE K"

#########################################
# detect process 1 identity
echo "Process 1 is program: $(tr -d '\0' </proc/1/cmdline)"

#########################################
# make a distribution recommendation
if test "$HAS_PROGRAM_LOGGER" = "1" ; then
    if test "$HAS_PROGRAM_LOGREAD" = "1" ; then
        RECOMMENDED_DISTRIB="base.dist"
    else
        RECOMMENDED_DISTRIB="nologread.dist"
    fi
else
    RECOMMENDED_DISTRIB="nosyslogd.dist"
fi

if test "$HAS_VAR_LOG_MESSAGES" = "0" ; then
    RECOMMENDED_DISTRIB="nosyslogd.dist"
fi

echo "-------------------------------------"
echo "Based on this scan,"
echo "the recommended DISTRIB value should be:"
echo "   $RECOMMENDED_DISTRIB"
echo "-------------------------------------"

echo "RESULT=$RESULT"
if test "$RESULT" = "SUCCESS" ; then
    echo "All required programs, files, features, and permissions are present"
else
    echo "One or more required items are missing"
fi
echo "-------------------------------------"

