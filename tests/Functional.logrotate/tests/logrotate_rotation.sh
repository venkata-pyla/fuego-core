#!/bin/sh

#  Verify that the rotate file is created in the specified directory.
#  option : -v

test="log rotation"

if [ -f /var/log/testlog* ]
then
    rm -f /var/log/testlog*
fi

if [ -f /var/lib/logrotate.status ]
then
    mv /var/lib/logrotate.status /var/lib/logrotate.status_bak
fi

cp data/test.conf /etc/logrotate.d/test.conf

cp data/testlog100k /var/log/testlog

cp data/testlog.sh data/testlog_test.sh
chmod +x data/testlog_test.sh
data/testlog_test.sh
chown root /etc/logrotate.d/test.conf
logrotate -v /etc/logrotate.d/test.conf
if ls /var/log/testlog.1
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -f /etc/logrotate.d/test.conf
rm -f /var/log/testlog*
rm -f data/testlog_test.sh
if [ -f /var/lib/logrotate.status_bak ]
then
    mv /var/lib/logrotate.status_bak /var/lib/logrotate.status
fi
