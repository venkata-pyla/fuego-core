#!/bin/sh

# In target, run command ifdown/ifup.
# option: none

test="ifdown_ifup"

# pattern for detecting loopback network device
LO_PAT="^lo[: ]"

#if [ ! -f /etc/sysconfig/network-scripts/ifcfg-lo ]
#then
#    echo " -> $test: ifcfg file does not exist."
#    echo " -> $test: TEST-SKIP"
#    exit 0
#fi

if ! ifconfig | grep -q "$LO_PAT" ;
then
    echo " -> $test: 'lo' not found in ifconfig output on test start"
    echo " -> $test: TEST-SKIP"
fi

if ifdown lo
then
    echo " -> $test: ifdown lo succeeded."
else
    echo " -> $test: ifdown lo  failed."
    echo " -> $test: TEST-FAIL"
    exit 0
fi

if ifconfig | grep -q "$LO_PAT" ;
then
    echo " -> $test: 'lo' was found in ifconfig output after 'ifdown lo'"
    echo " -> $test: TEST-FAIL"
fi

if ! ifup lo ;
then
    echo " -> $test: error $? running 'ifup lo'"
    echo " -> $test: TEST-FAIL"
fi

if ifconfig | grep -q "$LO_PAT" ; 
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: 'lo' was not found in ifconfig output after 'ifup lo'"
    echo " -> $test: TEST-FAIL"
fi
