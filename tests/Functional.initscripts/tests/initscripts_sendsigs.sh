#!/bin/sh

# In target, verify that the file is executable.
# option: none

test="sendsigs"

if [ -x /etc/init.d/sendsigs ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
