#!/bin/sh

#  In the target start ipmi, and confirm the process condition by command ps.

test="ps"

. ./fuego_board_function_lib.sh

set_init_manager

service_status=$(get_service_status ipmi)

exec_service_on_target ipmi stop

if exec_service_on_target ipmi start
then
    echo " -> $test: TEST-PASS"
else
    echo " -> start of ipmi failed."
    echo " -> $test: TEST-FAIL"
fi

if [ "$service_status" = "inactive" ]
then
    exec_service_on_target ipmi stop
fi
