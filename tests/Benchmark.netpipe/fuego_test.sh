tarball=NetPIPE-3.7.1.tar.gz

function test_build {
    patch -p1 -N -s < $TEST_HOME/netpipe-makefile.patch
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD"
}

function test_deploy {
    put NPtcp  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    cmd "killall -SIGKILL NPtcp 2>/dev/null; exit 0"

    # Start netpipe server on Jenkins host
    netpipe_exec=$(which NPtcp)

    if [ -z $netpipe_exec ]; then
        echo "ERROR: Cannot find netpipe server on host (NPtcp)"
        false
    else
        $netpipe_exec -p 2 &
    fi

    assert_define BENCHMARK_NETPIPE_PERT

    if [ "$BENCHMARK_NETPIPE_SRV" = "default" ]; then
        srv=$SRV_IP
    else
        srv=$BENCHMARK_NETPIPE_SRV
    fi

    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./NPtcp -h $srv -p $BENCHMARK_NETPIPE_PERT" $BOARD_TESTDIR/fuego.$TESTDIR/${TESTDIR}.log
}
