#!/bin/sh

#  In target, run comannd ltrace to count time and calls.
#  option: -c

test="count"

rm log_for_fuego || true
ltrace -c dd if=/dev/urandom of=/dev/null count=1000 > log_for_fuego 2>&1
if grep "usecs/call" log_for_fuego
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm log_for_fuego
