#!/bin/sh

#  Write the trace output to the file filename rather than to stderr.
#  option: -o/--output

test="output"

rm log_for_fuego || true
ltrace -o log_for_fuego ls
if grep "+++ exited (status 0) +++" log_for_fuego
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm log_for_fuego
