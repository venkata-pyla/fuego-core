# test to test bc with different specs
#
# Notes:
# - each sub-test is a testcase
# - we use TAP output to summarize the results for this test
#

BATCH_TESTPLAN=$(cat <<END_TESTPLAN
{
    "testPlanName": "smoketest",
    "default_timeout": "6m",
    "default_spec": "default",
    "default_reboot": "false",
    "default_rebuild": "false",
    "default_precleanup": "true",
    "default_postcleanup": "true",
    "tests": [
        { "testName": "Benchmark.Dhrystone" },
        { "testName": "Benchmark.dbench4" },
        { "testName": "Benchmark.hackbench" },
        { "testName": "Benchmark.himeno" },
        { "testName": "Benchmark.netperf" },
        { "testName": "Benchmark.Whetstone" },
        { "testName": "Benchmark.signaltest" },
        { "testName": "Benchmark.linpack" },
        { "testName": "Benchmark.cyclictest" },
        { "testName": "Functional.bc" },
        { "testName": "Functional.crashme" },
        { "testName": "Functional.ipv6connect" },
        { "testName": "Functional.jpeg" },
        { "testName": "Functional.netperf", "timeout": "12m" },
        { "testName": "Functional.scrashme" },
        { "testName": "Functional.synctest" },
        { "testName": "Functional.zlib" },
        { "testName": "Functional.hello_world" }
    ]
}
END_TESTPLAN
)

function test_run {
    export TC_NUM=1
    # should generally use the testplan default timeout, but this
    # is available if needed.  However, 3m was found to be too short
    # for some tests (dbench4 and zlib)
    #DEFAULT_TIMEOUT=3m
    export FUEGO_BATCH_ID="st-$(allocate_next_batch_id)"

    # don't stop on test errors
    set +e
    log_this "echo \"batch_id=$FUEGO_BATCH_ID\""
    run_test Benchmark.Dhrystone
    run_test Benchmark.dbench4
    run_test Benchmark.hackbench
    run_test Benchmark.himeno
    run_test Benchmark.netperf
    run_test Benchmark.Whetstone
    run_test Benchmark.signaltest
    run_test Benchmark.linpack
    run_test Benchmark.cyclictest
    run_test Functional.bc
    run_test Functional.crashme
    run_test Functional.ipv6connect
    run_test Functional.jpeg
    run_test Functional.netperf --timeout 12m
    run_test Functional.scrashme
    run_test Functional.synctest
    run_test Functional.zlib
    run_test Functional.hello_world
    set -e
}
