#!/bin/sh

#  In the target start multipathd, and check if the /var/run/multipathd.pid is exist
#  check the keyword "multipathd".

test="pidfile"

service_status=$(get_service_status multipathd)

exec_service_on_target multipathd stop

if [ -f /etc/multipath.conf ]
then
    cp /etc/multipath.conf /etc/multipath.conf_bak
fi

if [ -f /etc/multipath.conf.example ]
then
    cp /etc/multipath.conf.example /etc/multipath.conf
fi

restore_target() {
    if [ -f /etc/multipath.conf_bak ]
    then
        mv /etc/multipath.conf_bak /etc/multipath.conf
    else
        rm -f /etc/multipath.conf
    fi
}

if exec_service_on_target multipathd start
then
    echo " -> start of multipathd succeeded."
else
    echo " -> start of multipathd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 10

if test -f /var/run/multipathd.pid
then
    echo " -> get the pidfile of multipathd."
else
    echo " -> can't get the pidfile of multipathd."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target multipathd stop
    restore_target
    if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
    then
        exec_service_on_target multipathd start
    fi
    exit
fi

exec_service_on_target multipathd stop

if test ! -f /var/run/multipathd.pid
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

restore_target
if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target multipathd start
fi
