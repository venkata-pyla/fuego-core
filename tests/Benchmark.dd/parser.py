#!/usr/bin/python

import os, re, sys
import common as plib

regex_string = "^real[\t ]*(.*)m(.*)s"

results = {}
matches = plib.parse_log(regex_string)
print("DEBUG: matches=%s" % str(matches))

if matches:
    minutes = (float)(matches[0][0])
    seconds = (float)(matches[0][1])
    time = minutes*60 + seconds
    results['default.dd_1G'] = \
            [{"name": "time", "measure" : time}]

sys.exit(plib.process(results))
