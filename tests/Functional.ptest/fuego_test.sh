# Usage: ptest-runner [-d directory] [-l list] [-t timeout] [-x xml-filename] [-h] [ptest1 ptest2 ...]

function test_pre_check {
    assert_has_program ptest-runner
    AVAILABLE_TESTS=$(cmd "ptest-runner -l | tail -n +2 | awk '{print \$1;}'")
    TESTS=${FUNCTIONAL_PTEST_TESTS:-$AVAILABLE_TESTS}
    echo "The following tests will be run: $TESTS"
}

function test_run {
    report "ptest-runner $TESTS"
}

