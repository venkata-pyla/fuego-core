#!/bin/sh

#  In the target start nfs-mountd, and confirm the process condition
#  using the ps command.

test="nfs-mountd_ps"

. ./fuego_board_function_lib.sh

set_init_manager

service_status=$(get_service_status nfs-mountd)
exec_service_on_target nfs-mountd stop

if [ -f /etc/exports ]
then
    mv /etc/exports /etc/exports_bak
fi

touch /etc/exports

restore_target() {
    if [ -f /etc/exports_bak ]
    then
        mv /etc/exports_bak /etc/exports
    else
        rm -f /etc/exports
    fi
}

if exec_service_on_target nfs-mountd start
then
    echo " -> start of nfs-mountd succeeded."
else
    echo " -> start of nfs-mountd failed."
    echo " -> $test: TEST-FAIL"
    restore_target
    exit
fi

sleep 5

if ps aux | grep "[/]usr/sbin/rpc.mountd"
then
    echo " -> rpc.mountd process is running."
else
    echo " -> rpc.mountd process is not running."
    echo " -> $test: TEST-FAIL"
    if [ "$service_status" = "inactive" ]
    then
        exec_service_on_target nfs-mountd stop
    fi
    restore_target
    exit
fi

exec_service_on_target nfs-mountd stop

if ps aux | grep "[/]usr/sbin/rpc.mountd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi

if [ "$service_status" = "active" -o "$service_status" = "unknown"]
then
    exec_service_on_target nfs-mountd start
fi
restore_target
