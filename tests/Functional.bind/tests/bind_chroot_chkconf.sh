#!/bin/sh

#  After running the/etc/named.conf to the target, run the named checkconf command in the chroot environment to verify the normal termination.

test="chroot_chkconf"

if [ -d /var/named/ ]
then
    mv /var/named/ /var/named_bak
fi

mkdir -p /var/named/chroot/etc/bind
cp -f /etc/bind/* /var/named/chroot/etc/bind/
mkdir -p /var/named/chroot/var/named
mkdir -p /var/named/chroot/var/cache/bind
mkdir -p /var/named/chroot/var/run/named
cp /etc/sysconfig/named /etc/sysconfig/named_bak
cp data/bind9/sysconfig/named /etc/sysconfig/named

touch /var/named/chroot/etc/bind/named.conf

cp data/bind9/named.conf /var/named/chroot/etc/bind/named.conf

if named-checkconf -t /var/named/chroot
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -rf /var/named
if [ -d /var/named_bak ]
then
    mv /var/named_bak /var/named
fi
mv /etc/sysconfig/named_bak /etc/sysconfig/named
