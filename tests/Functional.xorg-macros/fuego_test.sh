function test_build {
    echo "#!/bin/bash
    if [ -f /usr/share/aclocal/xorg-macros.m4 ]; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi;
    if [ -f /usr/share/pkgconfig/xorg-macros.pc ]; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi;
    " > run-tests.sh

}

function test_deploy {
    put run-tests.sh $BOARD_TESTDIR/fuego.$TESTDIR/;
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v run-tests.sh"  
}

function test_processing {
    log_compare "$TESTDIR" "2" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}


