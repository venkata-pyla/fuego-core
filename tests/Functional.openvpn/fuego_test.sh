NEED_ROOT=1

function test_pre_check {
    assert_has_program openvpn
    assert_has_program netstat
    assert_has_program ifconfig
}

function test_deploy {
    put $TEST_HOME/openvpn_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    sed -i 's/remote_host/'"$SRV_IP"'/g' data/testcli.conf;\
    ./openvpn_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
