#!/bin/sh

# Verify the creation of the tun device (tap0) interface by openvpn.

test="tun_$1"

test_type=$1

setup_routine $test_type

exec_service_on_target $service_name start

if ifconfig tap0
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target $service_name stop

restore_routine

if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target $service_name start
fi
