#!/bin/sh

#  In the target, run openvpn and check the status of process.

test="ps_$1"

test_type=$1

setup_routine $test_type

exec_service_on_target $service_name start

if ps aux | grep "[/]usr/sbin/openvpn"
then
    echo " -> $test: get the process of openvpn succeeded."
else
    echo " -> $test: get the process of openvpn failed."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target $service_name stop
    restore_routine
    exit
fi

exec_service_on_target $service_name stop

if ! ps aux | grep "[/]usr/sbin/openvpn"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
restore_routine
if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target $service_name start
fi
