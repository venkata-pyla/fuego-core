#!/bin/sh

#  In the target, run openvpn and check the message of syslog-ng.

test="syslog-ng_$1"

test_type=$1

setup_routine $test_type
logger_service=$(detect_logger_service)
openvpn_logfile=$(get_service_logfile)

exec_service_on_target $logger_service stop

if [ -f $openvpn_logfile ]
then
    mv $openvpn_logfile $openvpn_logfile"_bak"
fi

exec_service_on_target $logger_service restart

sleep 2

exec_service_on_target $service_name start

sleep 5

if cat $openvpn_logfile | grep OpenVPN
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target $service_name stop

restore_routine

if [ "$service_status" = "active" -o "$service_status" = "unknown" ]
then
    exec_service_on_target $service_name start
fi
