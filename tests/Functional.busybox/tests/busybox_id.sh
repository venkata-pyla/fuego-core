#!/bin/sh

#  The testscript checks the following options of the command id
#  1) Option -u -g

test="id"

if grep "^$(whoami):" /etc/passwd | grep "$(busybox id -u):$(busybox id -g)"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
