#!/bin/sh

#  The testscript checks the following options of the command tar
#  1) Option: none

test="time"

busybox time pwd > log 2>&1
if [ "$(head -n 1 log | grep ".*")" ] && [ "$(head -n 2 log | tail -n 1 | grep "real.*m.*s")" ] \
&& [ "$(tail -n 2 log | head -n 1 | grep "user.*m.*s")" ] && [ "$(tail -n 1 log | grep "sys.*m.*s")" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log;
