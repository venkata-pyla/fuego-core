#!/bin/sh

#  The testscript checks the following options of the command sed
#  1) Option -e

test="sed"

if [ "$(echo "test case" | busybox sed -e 's/case/result/g')" = "test result" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
