#!/bin/sh

#  The testscript checks the following options of the command mknod
#  1) Option: -m

test="mknod"

mkdir test_dir
busybox mknod -m 666 test_dir/test_pipe p
if ls -l test_dir | grep "prw\-rw\-rw\-.*test_pipe"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir;
