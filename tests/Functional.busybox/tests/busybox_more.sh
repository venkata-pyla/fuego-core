#!/bin/sh

#  The testscript checks the following options of the command more
#  1) Option none

test="more"

mkdir test_dir
echo "This is a file" > test_dir/test1
if [ "$(busybox more ./test_dir/test1)" = "This is a file" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf ./test_dir;
