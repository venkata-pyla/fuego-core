#!/bin/sh

#  The testscript checks the following options of the command du
#  1) Option: -k

test="du"

mkdir test_dir
touch ./test_dir/test1
touch ./test_dir/test2
if busybox du -k test_dir | grep "4.*test_dir"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir;
