#!/bin/sh

#  The testscript checks the following options of the command deallocvt
#  1) Option none

test="deallocvt"

openvt -f -c 4 ls
sleep 1
if busybox deallocvt 4
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
