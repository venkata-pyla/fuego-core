#!/bin/sh

#  The testscript checks the following options of the command dmesg
#  1) Option none

test="dmesg"

if busybox dmesg | grep "Linux version"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
