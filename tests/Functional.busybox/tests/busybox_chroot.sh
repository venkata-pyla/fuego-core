#!/bin/sh

#  The testscript checks the following options of the command chroot
#  1) Option none

test="chroot"

test_dir=test_dir

mkdir -p $test_dir
mkdir -p $test_dir/bin
mkdir -p $test_dir/lib64
mkdir -p $test_dir/lib
# put ls and the libs it needs into the chroot area
cp -v /bin/ls $test_dir/bin/
libs="$(ldd /bin/ls | egrep -o '/lib.*\.[0-9]')"
for lib in $libs; do
   lib_dirname=$(dirname $lib)
   mkdir -p ${test_dir}${lib_dirname}
   cp -v $lib ${test_dir}${lib_dirname}
done
if busybox chroot $test_dir /bin/ls
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -fr $test_dir
