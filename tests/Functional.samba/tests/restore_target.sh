#!/bin/sh

# restore the status of target when test finished.

. ./fuego_board_function_lib.sh
set_init_manager

if [ -f /etc/samba/smb.conf_bak ]
then
    mv /etc/samba/smb.conf_bak /etc/samba/smb.conf
fi
mv data/smb06.conf_bak data/smb06.conf

if [ "$(tail -n 1 $1)" = "active" -o "$(tail -n 1 $1)" = "unknown" ]
then
    exec_service_on_target iptables start
fi
if [ "$(head -n 1 $1)" = "inactive" ]
then
    exec_service_on_target smb stop
fi

rm -fr $1
