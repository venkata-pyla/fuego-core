#!/bin/sh

#  In the target to start acpid acpid, to confirm the acquisition of the log.
#  check the keyword "acpid".

test="logfile"

. ./fuego_board_function_lib.sh

set_init_manager
logger_service=$(detect_logger_service)

exec_service_on_target $logger_service stop
if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

sleep 5

exec_service_on_target acpid stop
if [ -f /var/log/acpid ]
then
    rm -f /var/log/acpid
fi

exec_service_on_target $logger_service restart
if exec_service_on_target acpid start
then
    echo " -> start of acpid succeeded."
else
    echo " -> start of acpid failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if tail /var/log/syslog | grep "starting up"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target acpid stop
if [ -e /var/log/syslog_bak ]
then
    mv /var/log/syslog_bak /var/log/syslog
fi
