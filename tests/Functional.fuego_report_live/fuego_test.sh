# This test uses report_live to gather information from
# commands on the board, without using the regular log
# file on the board.
function test_run {
    log_this "echo testing report_live"
    # start with some simple stuff
    report_live "cd $BOARD_TESTDIR/fuego.$TESTDIR; pwd"

    report_live "echo running on board before reboot"

    # check exit code from operation run on board
    set +e
    report_live "echo 'echo' running on board before reboot"
    RESULT=$?
    log_this "echo Return code from report_live echo was $RESULT"

    report_live "echo 'false' running on board before reboot ; false"
    RESULT=$?
    log_this "echo Return code from report_live false was $RESULT"
    set -e

    # delay, to allow to check files
    iprint "Waiting 10 seconds to give time to check files"
    sleep 10
    set +e
    report_live "reboot ; sleep 60"
    set -e

    sleep 60
    report_live "echo running on board after reboot: SUCCESS"
}

function test_processing {
    log_compare "$TESTDIR" "1" "SUCCESS" "p"
}
