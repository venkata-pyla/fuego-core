#!/bin/sh

#  In target, run command vconfig.
#  option: rem

test="remove"

vconfig add $vcon_ifeth 4

if vconfig rem $vcon_ifeth.4 | grep "Removed VLAN"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
