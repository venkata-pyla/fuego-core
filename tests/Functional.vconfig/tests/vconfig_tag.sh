#!/bin/sh

#  In target, run command vconfig.
#  option: add/rem

test="tag"

mkdir test_dir

ifconfig $vcon_ifeth up
vconfig add $vcon_ifeth 100

# specify the address to vlanif by ifconfig. 
ifconfig $vcon_ifeth.100 inet $vcon_ip1 netmask 0xffffff00

# to verify the IP address exists and can accept requests.
ping -q -c 50 -I $vcon_ifeth.100 $vcon_ip2 &

# check the VLAN tag by tcpdump.
tcpdump -c 10 -ex -i $vcon_ifeth ether broadcast > test_dir/dumpdata

if cat test_dir/dumpdata | grep "vlan 100"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm -fr test_dir
vconfig rem $vcon_ifeth.100
