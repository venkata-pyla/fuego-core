#!/bin/sh

#  In target, run command vconfig.
#  option: add

test="add"

vconfig add $vcon_ifeth 4

if ifconfig $vcon_ifeth.4 | grep "$vcon_ifeth.4"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
vconfig rem $vcon_ifeth.4
