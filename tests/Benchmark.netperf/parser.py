#!/usr/bin/python

import os, re, sys
import common as plib

ref_section_pat = "^\[[\w\d_ ./]+.[gle]{2}\]"
cur_search_pat = re.compile("^([\w /]+) TEST from .*\n.*\n.*\n.*\n.*\n.*\n^.* ([\d.]+) +([\d.]+) +[\d.]+ +[\d.]+ +[\d.]+ *\n", re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	for item in pat_result:
		cur_dict[re.sub(' ', '_', item[0]) + ".net"] = item[1]
		cur_dict[re.sub(' ', '_', item[0]) + ".cpu"] = item[2]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'Rate, Mb/s\nCPU %'))
