tarball=rmaptest.tar.gz

function test_build {
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD"
}

function test_deploy {
    put rmap-test  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    assert_define FUNCTIONAL_RMAPTEST_ITERATIONS
    assert_define FUNCTIONAL_RMAPTEST_VMA
    assert_define FUNCTIONAL_RMAPTEST_VMA_SIZE
    assert_define FUNCTIONAL_RMAPTEST_TASKS
    assert_define FUNCTIONAL_RMAPTEST_VMAS_FOR_PROCESS

    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./rmap-test -h -i$FUNCTIONAL_RMAPTEST_ITERATIONS -n$FUNCTIONAL_RMAPTEST_VMA -s$FUNCTIONAL_RMAPTEST_VMA_SIZE -t$FUNCTIONAL_RMAPTEST_TASKS -V$FUNCTIONAL_RMAPTEST_VMAS_FOR_PROCESS -v file1.dat"
}
