#!/bin/sh

#  In the target start atd, and confirm the process condition by command ps.
#  check the keyword "atd".

test="ps"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target atd stop

if exec_service_on_target atd start
then
    echo " -> start atd succeeded."
else
    echo " -> start atd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if ps -N a | grep "[/]usr/sbin/atd"
then
    echo " -> get the pid of atd."
else
    echo " -> can't get the pid of atd."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target atd stop
    exit
fi

if exec_service_on_target atd stop
then
    echo " -> stop atd succeeded."
else
    echo " -> stop atd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if ps -N a | grep "[/]usr/sbin/atd"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
