#!/bin/sh

#  In the target start atd, and check if the /var/run/atd.pid is exist.

test="pidfile"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target atd stop

if [ -f /var/run/atd.pid ]
then
    rm -f /var/run/atd.pid
fi

if exec_service_on_target atd start
then
    echo " -> start atd succeeded."
else
    echo " -> start atd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if ls /var/run/atd.pid
then
    echo " -> /var/run/atd.pid exists."
else
    echo " -> /var/run/atd.pid dose not exist."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target atd stop
    exit
fi

if exec_service_on_target atd stop
then
    echo " -> stop atd succeeded."
else
    echo " -> stop atd failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

sleep 5

if test -f /var/run/atd.pid
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
