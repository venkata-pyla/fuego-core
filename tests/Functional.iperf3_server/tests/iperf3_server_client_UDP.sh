#!/bin/sh

#  Test command iperf3 on target.
#  Option : -c (UDP)

test="client_UDP"

killall -KILL iperf3
iperf3 -s -D&

sleep 3

if iperf3 -c 127.0.0.1 -u | grep "connected"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;

killall -KILL iperf3
