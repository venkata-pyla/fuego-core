tarball=commonAPI_Dbus.tar.gz

function test_build {
    $CXX -std=gnu++11 HelloWorldService.cpp HelloWorldStubImpl.cpp HelloWorldDBusStubAdapter.cpp HelloWorldStubDefault.cpp HelloWorldDBusDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-DBus -ldbus-1 -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -I $SDKTARGETSYSROOT/usr/include/dbus-1.0/ -I $SDKTARGETSYSROOT/usr/lib/dbus-1.0/include/ -o HelloWorldService
    $CXX -std=gnu++11 HelloWorldClient.cpp HelloWorldDBusProxy.cpp HelloWorldDBusDeployment.cpp -rdynamic -lCommonAPI -lCommonAPI-DBus -ldbus-1 -I $SDKTARGETSYSROOT/usr/include/CommonAPI-3.1/ -I $SDKTARGETSYSROOT/usr/include/dbus-1.0/ -I $SDKTARGETSYSROOT/usr/lib/dbus-1.0/include/ -o HelloWorldClient   
}

function test_deploy {
	put HelloWorldService $BOARD_TESTDIR/fuego.$TESTDIR/
	put HelloWorldClient $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export PATH=$BOARD_TESTDIR/fuego.$TESTDIR:$PATH; chmod 777 *;
	if (./HelloWorldService &); then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
	if ./HelloWorldClient; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;
	pkill HelloWorldServi"
}

function test_processing {
	log_compare "$TESTDIR" "2" "^TEST.*OK" "p"
    	log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}




