function test_pre_check {
    is_on_target_path file PROGRAM_FILE
    assert_define PROGRAM_FILE "Missing program 'file' on board"
}

function test_deploy {
    put $TEST_HOME/file_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; \
        sh -v file_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
