OF.NAME="base-board"
OF.DESCRIPTION="Basic board file"
SSH_PORT="22"
SRV_IP="`/sbin/ip a s |awk -F ' +|/' '/inet / && $3 != "127.0.0.1" { print $3; exit; }'`"

# Make a stub function for setting up a board for a test.
# This can be any set of operations desired by the user,
# and should be overriden in the board file.
function ov_board_setup () {
  return
}

function ov_board_teardown () {
  return
}

# establish or validate connection to target
# log in if needed
# takes an option $1 which is number of retries to attempt
function ov_transport_connect () {
  local max_retries=1
  if [ -n "$1" ] ; then
    max_retries=$1
  fi

  case "$TRANSPORT" in
    local)
      return 0
      ;;
    ssh|ttc)
      export SSHPASS=$PASSWORD
      retries=1
      while [ $retries -le $max_retries ] ; do
        ov_transport_cmd "true"
        if [ $? -eq 0 ] ; then
          return 0
        fi
        sleep 1
        retries=$(( $retries + 1 ))
      done
      return 1
      ;;
    serial)
      assert_define SERIAL
      retries=1
      while [ $retries -le $max_retries ] ; do
        # use serlogin for logging in to the target board
        if [ -n ${PASSWORD} ] ; then
          $SERLOGIN -P "${PASSWORD}" $LOGIN
        else
          $SERLOGIN $LOGIN
        fi
        if [ $? -eq 0 ] ; then
          return 0
        fi
        sleep 1
        retries=$(( $retries + 1 ))
      done
      return 1
      ;;
    lava)
      # put LAVA connect here
      ;;
    ssh2serial)
      export SSHPASS=$S2S_PASSWORD

      # make sure required variables are present
      assert_define IPADDR
      assert_define SERIAL
      assert_define S2S_LOGIN

      retries=1
      while [ $retries -le $max_retries ] ; do
        if [ -n ${PASSWORD} ] ; then
          sshpass -e ssh ${S2S_SSH_ARGS}${IPADDR} "$S2S_SERLOGIN -P \"${PASSWORD}\" $LOGIN"
        else
          sshpass -e ssh ${S2S_SSH_ARGS}${IPADDR} "$S2S_SERLOGIN $LOGIN"
        fi
        if [ $? -eq 0 ] ; then
          return 0
        fi
        sleep 1
        retries=$(( $retries + 1 ))
      done
      return 1
      ;;
    *)
     abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT} in ov_transport_connect"
     ;;
  esac
}

function ov_transport_disconnect () {
  case "$TRANSPORT" in
  ssh|ttc|local|serial|ssh2serial)
    return 0
    ;;
  lava)
    # put LAVA disconnect here
    ;;
  *)
    abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT} in ov_transport_disconnect"
    ;;
  esac
}

# $1 = remote file (source); $2 = local file (destination)
# I'm not sure what will happen with arguments past 2, but they're passed
# to scp unmodified.  I suspect this will confuse scp, and that all but
# the last will be treated as sources.
# FIXTHIS - ov_transport_get:  'ttc cp' doesn't support recursion
function ov_transport_get () {
  case "$TRANSPORT" in
  "ssh")
    if [ -n "${SSH_KEY}" ] ; then
      scp -i ${SSH_KEY} ${SCP_ARGS} -r $LOGIN@${DEVICE}:"$1" "${*:2}"
    else
      sshpass -e scp ${SCP_ARGS} -r $LOGIN@${DEVICE}:"$1" "${*:2}"
    fi
    ;;
  "ttc")
    $TTC $TTC_TARGET cp target:"$1" "${*:2}"
    ;;
  "local")
    if [ "$1" != "${*:2}" ]; then
        cp -r "$1" "${*:2}"
    fi
    ;;
  "serial")
    if startswith $SERIAL "/" ; then
        $SERCP -d $SERIAL serial:"$1" "${*:2}"
    else
        $SERCP $SERIAL:"$1" "${*:2}"
    fi
    wait
    ;;
  lava)
    # put LAVA get here
    # could be a recursive call with TRANSPORT=ssh
    ;;
  ssh2serial)
    # make a staging area on the proxy host
    TRANSFER_DIR="/tmp/transfer-$IPADDR"
    ov_s2s_proxy_cmd "mkdir -p $TRANSFER_DIR"

    # copy materials from board to proxy host
    if startswith $SERIAL "/" ; then
        cmds="$S2S_SERCP -d $SERIAL serial:\"$1\" ${TRANSFER_DIR}"
    else
        cmds="$S2S_SERCP $SERIAL:\"$1\" ${TRANSFER_DIR}"
    fi
    if [ -n "${SSH_KEY}" ] ; then
      ssh -i ${SSH_KEY} ${S2S_SSH_ARGS}${IPADDR} "$cmds"
    else
      sshpass -e ssh ${S2S_SSH_ARGS}${IPADDR} "$cmds"
    fi

    # copy materials from proxy host to here
    filename=$(basename $1)
    if [ -n "${SSH_KEY}" ] ; then
      scp -i ${SSH_KEY} ${S2S_SCP_ARGS} -r "${S2S_LOGIN}@${IPADDR}:${TRANSFER_DIR}/$filename" "${*:2}"
    else
      sshpass -e scp ${S2S_SCP_ARGS} -r "${S2S_LOGIN}@${IPADDR}:${TRANSFER_DIR}/$filename" "${*:2}"
    fi

    # remove materials from proxy host
    ov_s2s_proxy_cmd "rm -rf $TRANSFER_DIR"
    ;;
  *)
   abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT} in ov_transport_get"
   ;;
  esac
}

# execute a command on the ssh2serial proxy host
function ov_s2s_proxy_cmd () {
    if [ -n "${SSH_KEY}" ] ; then
      ssh -i ${SSH_KEY} ${S2S_SSH_ARGS}${IPADDR} "$@"
    else
      sshpass -e ssh ${S2S_SSH_ARGS}${IPADDR} "$@"
    fi
}

# $1 = local file (source); $2 = remote file (destination)
# FIXTHIS - ov_transport_put:  'ttc cp' doesn't support multiple sources
# FIXTHIS - ov_transport_put:  'ttc cp' doesn't support recursion (-r argument)
function ov_transport_put () {
  case "$TRANSPORT" in
  "ssh")
    if [ -n "${SSH_KEY}" ] ; then
      scp -i ${SSH_KEY} ${SCP_ARGS} -r "${@:1:$(($#-1))}" $LOGIN@$DEVICE:"${@: -1}"
    else
      sshpass -e scp ${SCP_ARGS} -r "${@:1:$(($#-1))}" $LOGIN@$DEVICE:"${@: -1}"
    fi
    ;;
  "ttc")
    $TTC $TTC_TARGET cp "${@:1:$(($#-1))}" target:"${@: -1}"
    ;;
  "local")
    for par in "${@:1:$(($#-1))}"; do
        if [ "$par" != "-r" -a "$par" != "${@: -1}" ]; then
            cp -r "$par" "${@: -1}"
        fi
    done
    ;;
  "serial")
    if startswith $SERIAL "/" ; then
        $SERCP -d $SERIAL "${@:1:$(($#-1))}" "serial:${@: -1}"
    else
        $SERCP "${@:1:$(($#-1))}" "${SERIAL}:${@: -1}"
    fi
    wait
    ;;
  lava)
    # put LAVA put here
    # could be a recursive call with TRANSPORT=ssh
    ;;
  ssh2serial)
    # make a staging area on the proxy host
    TRANSFER_DIR="/tmp/transfer-$IPADDR"

    # pre-remove dir, just in case a previous test left stuff around
    ov_s2s_proxy_cmd "rm -rf $TRANSFER_DIR"
    ov_s2s_proxy_cmd "mkdir -p $TRANSFER_DIR"

    # copy materials to the proxy host
    if [ -n "${SSH_KEY}" ] ; then
      scp -i ${SSH_KEY} ${S2S_SCP_ARGS} -r "${@:1:$(($#-1))}" $S2S_LOGIN@$IPADDR:"${TRANSFER_DIR}"
    else
      sshpass -e scp ${S2S_SCP_ARGS} -r "${@:1:$(($#-1))}" $S2S_LOGIN@$IPADDR:"${TRANSFER_DIR}"
    fi

    # copy from proxy host to board
    filename=$(basename ${@: 1})
    # FIXTHIS - ssh2serial put doesn't work with multiple source files
    if startswith $SERIAL "/" ; then
        cmds="cd $TRANSFER_DIR ; $S2S_SERCP -d $SERIAL \"${filename}\" \"serial:${@: -1}\" ; wait"
    else
        cmds="cd $TRANSFER_DIR ; $S2S_SERCP \"${filename}\" \"${SERIAL}:${@: -1}\" ; wait"
    fi
    if [ -n "${SSH_KEY}" ] ; then
      ssh -i ${SSH_KEY} ${S2S_SSH_ARGS}${IPADDR} "$cmds"
    else
      sshpass -e ssh ${S2S_SSH_ARGS}${IPADDR} "$cmds"
    fi

    # remove materials from proxy host
    ov_s2s_proxy_cmd "rm -rf $TRANSFER_DIR"
    ;;
  *)
   abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT} in ov_transport_put"
   ;;
  esac
}

# $@ = command and args for remote execution
function ov_transport_cmd() {
  case "$TRANSPORT" in
  ssh)
    if [ -n "${SSH_KEY}" ] ; then
      ssh -i ${SSH_KEY} ${SSH_ARGS}${DEVICE} "$@"
    else
      sshpass -e ssh ${SSH_ARGS}${DEVICE} "$@"
    fi
    ;;
  ttc)
    $TTC $TTC_TARGET run "$@"
    ;;
  local)
    /bin/sh -c "$@"
    ;;
  serial)
    ${SERSH} "$@"
    wait
    ;;
  lava)
    # put LAVA cmd here
    # could be a recursive call with TRANSPORT=ssh
    ;;
  ssh2serial)
    if [ -n "${SSH_KEY}" ] ; then
      ssh -i ${SSH_KEY} ${S2S_SSH_ARGS}${IPADDR} "${S2S_SERSH} '$@'"
    else
      sshpass -e ssh ${S2S_SSH_ARGS}${IPADDR} "${S2S_SERSH} '$@'"
    fi
    ;;
  *)
   abort_job "Error reason: unsupported TRANSPORT ${TRANSPORT} in ov_transport_cmd"
   ;;
  esac
}

# function to reboot the board
function ov_board_control_reboot() {
  ftc power-cycle -b $NODE_NAME
  if [ $? -ne 0 ]; then
    abort_job "Error: could not power-cycle the board"
  fi
}
