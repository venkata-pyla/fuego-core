#!/bin/sh
# jta-convert - convert tests from JTA to Fuego

# convert board test directory from
# $JTA_HOME/jta.$TESTDIR to $BOARD_TESTDIR/fuego.$TESTDIR
find . -name "*.sh" | xargs grep -l JTA_HOME/jta | xargs -n 1 perl -p -i -e  "s/JTA_HOME\/jta/BOARD_TESTDIR\/fuego/g"

# convert JTA_SCRIPTS_PATH to FUEGO_CORE/scripts
find . -name "*.sh" | xargs grep -l JTA_SCRIPTS_PATH | xargs -n 1 perl -p -i -e  "s/JTA_SCRIPTS_PATH/FUEGO_CORE\/scripts/g"


