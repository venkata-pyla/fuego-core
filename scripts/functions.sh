# Copyright (c) 2014 Cogent Embedded, Inc.
# Copyright (c) 2017-2020 Sony Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains core functions of Fuego that needed for running tests

# These are supporting functions for test suites building process.
. $FUEGO_CORE/scripts/need_check.sh

function signal_handler {
    # if we got here, something went wrong.  Let's clean up and leave
    echo "In Fuego signal_handler (due to error in $FUEGO_CUR_PHASE phase)"
    unlock_build_dir
    export FUEGO_RECEIVED_SIGNAL="true"
    if [[ "$FUEGO_TEST_PHASES" == *post_test* ]] ; then
        FUEGO_CUR_PHASE=post_test
        echo "##### doing fuego phase: post_test (from signal handler) #####"
        post_test
    fi
    if [[ "$FUEGO_TEST_PHASES" == *processing* ]] ; then
        FUEGO_CUR_PHASE=processing
        echo "##### doing fuego phase: processing (from signal handler) #####"
        processing
    fi
    exit 130
}

trap signal_handler SIGTERM SIGHUP SIGALRM SIGINT EXIT
# cause ERR trap setting to be visible inside functions
set -o errtrace

# Clones a git repository into the current folder
# $1 (gitrepo): git repository URL
# $2 (gitref): branch, tag or commit id (optional, default: master)
# FIXTHIS: add commit id information to the json output
function git_clone {
    local gitrepo=${1}
    local gitref=${2}

    is_empty "$gitrepo"

    if [ -z "$gitref" ]; then
        gitref="master"
    fi

    local is_namedref=$(git ls-remote $gitrepo $gitref)

    if [ "$is_namedref" = "" ]; then
        echo "Clone repository $gitrepo."
        git clone -n $gitrepo fuego_git_repo
    else
        echo "Clone repository $gitrepo."
        git clone -n --depth=1 --branch=$gitref $gitrepo fuego_git_repo
    fi

    # equivalent to tarball's --strip-components=1
    mv fuego_git_repo/.git .git
    rm -rf fuego_git_repo

    echo "Checkout branch/tag/commit id $gitref."
    git checkout $gitref
}

# FIXTHIS: add support for other protocol, e.g. ftp
# Download a tarball from a upstream location
# $1 (tarball): the upstream location of tarball
# $2 (tarball): the store location for $1
function download_tarball {
    local tarball_url=${1}
    local tarball_des=${2}

    is_empty "$tarball_url"
    is_empty "$tarball_des"

    echo "Downloading $tarball to $tarball_des"
    wget $tarball_url -O $tarball_des &> /dev/null
    if [ $? -ne 0 ]; then
        abort_job "Downloading $tarball failed."
        return 1
    fi
}

# Untars a tarball in the current folder
# $1 (tarball): file to untar
function untar {
    local tarball=${1}

    is_empty "$tarball"

    # Check if it is a upstream tarball.
    if [[ $tarball == http* ]]; then
        upName="${TESTDIR^^}"
        md5sum_value=$(echo ${tarball} | md5sum)
        md5sum_value=${md5sum_value:0:7}
        tarball_dest=${FUEGO_RW}/buildzone/${upName//[-,.]/_}-${md5sum_value}-$(basename ${tarball})
        [[ -f ${tarball_dest} ]] && echo "Already downloaded, skip download..." \
                                 || download_tarball ${tarball} ${tarball_dest}
        tarball=${tarball_dest}
    fi

    echo "Unpacking $tarball"
    case ${tarball/*./} in
        gz|tgz) key=z ;;
        bz2) key=j ;;
        tar) key= ;;
        *) echo "Unknown $tarball file format. Not unpacking."; return 1;;
    esac
    if ! is_abs_path $tarball; then
        tarball=$TEST_HOME/$tarball
    fi

    tar ${key}xf $tarball --strip-components=1

    # record md5sum for possible source code updates
    md5sum $tarball > fuego_tarball_src_md5sum
}

# Unpacks/clones the test source code into the current directory.
#
# The following tarball and git variables can be specified in the test's
# spec.json or fuego_test.sh.
#   - local_source=1 in fuego_test.sh
#     - source resides in test directory (parallel to fuego_test.sh)
#   - Tarball variables
#     - tarball: tarball file name (e.g. bc-script.tar.gz)
#   - Git variables
#     - gitrepo: git repository (e.g. https://github.com/torvalds/linux.git)
#     - gitref: git branch, tag or commit id (optional, default: master)
#
# tarball and git variables follow the next preference rule:
#   fuego_test local_source > spec gitrepo > spec tarball >
#     fuego_test gitrepo > fuego_test tarball
function unpack {
    # prepare variables
    upName="${TESTDIR^^}"
    upNameUnder="${upName//[-,.]/_}"
    spec_gitrepo="${upNameUnder}_GITREPO"
    spec_gitref="${upNameUnder}_GITREF"
    spec_tarball="${upNameUnder}_TARBALL"

    # 0) fuego_test local source
    if [ -n "$local_source" ]; then
        cp -r $TEST_HOME/* .
        return
    fi

    # 1) spec gitrepo
    if [ -n "${!spec_gitrepo}" ]; then
        gitrepo=${!spec_gitrepo}
        if [ -n "${!spec_gitref}" ]; then
            gitref=${!spec_gitref}
        else
            # gitref could have been defined by fuego_test.sh
            gitref="master"
        fi
        git_clone "$gitrepo" "$gitref"
        return
    fi
    # 2) spec tarball
    if [ -n "${!spec_tarball}" ]; then
        tarball=${!spec_tarball}
        untar "$tarball"
        return
    fi
    # 3) fuego_test gitrepo
    if [ -n "$gitrepo" ]; then
        git_clone "$gitrepo" "$gitref"
        return
    fi
    # 4) fuego_test tarball
    if [ -n "$tarball" ]; then
        untar "$tarball"
        return
    fi

    echo "No tarball or gitrepo definition."
}

function is_empty {
# $1 - parameter

 if [ -z $1 ]; then
   eprint "EMPTY PARAMETER"
   exit
 fi
}

function report_devlog() {
    echo "$@" >> "${LOGDIR}/devlog.txt"
}

function get {
    report_devlog "get: $@"
    ov_transport_get "$@"
}

function put {
    report_devlog "put: $@"
    ov_transport_put "$@"
}

# These are supporting functions for target command running
# FIXTHIS: Add descriptions for parameters in every function
function cmd {
    report_devlog "cmd: $@"
    ov_transport_cmd "$@"
}

function safe_cmd {
# $1 - command to execute

  ov_rootfs_oom
  cmd "$@"
}

function report {
# $1 - remote shell command, $2 - test log file on target
# $2 - optional, default: $TESTDIR/$TESTDIR.log

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}

  if [ -z $2 ]; then
    vprint "in 'report' function: test log file parameter empty, so will use default"
    safe_cmd "{ echo 125 >$RETCODE; $1; echo \$? > $RETCODE; } 2>&1 | tee $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR.log"
  else
    safe_cmd "{ echo 125 >$RETCODE; $1; echo \$? > $RETCODE; } 2>&1 | tee $2"
  fi

  RESULT=$(cmd "cat $RETCODE ; rm -f $RETCODE 2>/dev/null")
  export REPORT_RETURN_VALUE=${RESULT}
  return ${RESULT}
}

function report_append {
# $1 - remote shell command, $2 - test log file.

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}

  if [ -z $2 ]; then
    echo "WARNING: test log file parameter empty, so will use default"
    safe_cmd "{ echo 125 >$RETCODE; $1; echo \$? > $RETCODE; } 2>&1 | tee -a $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR.log"
  else
    safe_cmd "{ echo 125 >$RETCODE; $1; echo \$? > $RETCODE; } 2>&1 | tee -a $2"
  fi

  RESULT=$(cmd "cat $RETCODE ; rm -f $RETCODE 2>/dev/null")
  export REPORT_RETURN_VALUE=${RESULT}
  return ${RESULT}
}

# $1 - local shell command
function log_this {
  is_empty $1

  # catch too many arguments
  if [ -n "$2" ] ; then
      eprint "TOO MANY ARGUMENTS to log_this"
      exit
  fi

  RETCODE=/tmp/$$-${RANDOM}
  touch $RETCODE

  # save shell xtrace setting
  if [[ "$SHELLOPTS" =~ xtrace ]] ; then
      x_was_set=1
  else
      x_was_set=0
  fi

  # we don't want shell debugging to end up in the hostlog
  set +x

  # use a new shell to execute this - this is closer to
  # the execution behavior of remote operations
  bash -c "{ $1; echo \$? > $RETCODE ; } 2>&1 | tee -a ${LOGDIR}/hostlog.txt"

  # restore shell xtrace setting
  if [ $x_was_set = 1 ] ; then
      set -x
  fi

  RESULT=$(cat $RETCODE)
  rm -f $RETCODE
  export REPORT_RETURN_VALUE=${RESULT}
  return ${RESULT}
}

function report_live {
# $1 - remote shell command
  is_empty $1

  LIVELOG=$LOGDIR/livelog.txt
  touch $LIVELOG

  # save shell errexit setting
  if [[ "$SHELLOPTS" =~ errexit ]] ; then
      e_was_set=1
  else
      e_was_set=0
  fi
  set +e
  cmd "{ $1; } 2>&1" | tee -a $LIVELOG

  RESULT=${PIPESTATUS[0]}

  # restore shell errexit setting
  if [ $e_was_set = 1 ] ; then
      set -e
  fi

  export REPORT_RETURN_VALUE=${RESULT}
  return ${RESULT}
}

function report_live_bare {
# $1 - remote shell command
  is_empty $1

  LIVELOG=$LOGDIR/livelog.txt
  touch $LIVELOG

  # save shell errexit setting
  if [[ "$SHELLOPTS" =~ errexit ]] ; then
      e_was_set=1
  else
      e_was_set=0
  fi
  set +e
  cmd "$1" | tee -a $LIVELOG

  RESULT=${PIPESTATUS[0]}

  # restore shell errexit setting
  if [ $e_was_set = 1 ] ; then
      set -e
  fi

  export REPORT_RETURN_VALUE=${RESULT}
  return ${RESULT}
}

function dump_syslogs {
# 1 - tmp dir, 2 - before/after

  is_empty $1
  is_empty $2

  ov_rootfs_logread "$@"
}

function get_syslog {
# 1 - tmp dir, 2 - before/after, 3 - destination file on host

  is_empty $1
  is_empty $2
  is_empty $3

  ov_rootfs_getlog "$@"
}

# implement a file-based build lock
# FIXTHIS - locking here only prevents concurrency during the build phase
# There should also be locking for the full test, on a single board
function lock_build_dir {
  BUILD_LOCKFILE="$WORKSPACE/${JOB_BUILD_DIR}/fuego.build.lock"
  lock_try_count=0

  while [ -e ${BUILD_LOCKFILE} ] ; do
    if [ -z "$wait_message_printed" ] ; then
       echo "Waiting for build to complete in directory $WORKSPACE/${JOB_BUILD_DIR}"
       wait_message_printed=1
    fi
    sleep 5
    lock_try_count=$(($lock_try_count+1))
    # show activity in console log
    echo -n "."
    if (( $lock_try_count % 60 == 0 )) ; then
      echo
    fi
  done
  if [ "$lock_try_count" != "0" ] ; then
      echo
  fi

  # indicate who's holding the lock
  echo "${BUILD_URL}" >$BUILD_LOCKFILE
}

function unlock_build_dir {
  if [ -e "$BUILD_LOCKFILE" ] ; then
    # only unlock if I'm the one holding it
    if [ "$(cat $BUILD_LOCKFILE)" = "${BUILD_URL}" ] ; then
      rm -f $BUILD_LOCKFILE
    fi
  fi
}

# Wait if an instance of the same job (board-test-spec) is running
#function concurrent_check {
#  LOCKFILE="$WORKSPACE/${JOB_BUILD_DIR}.build.lock"
#
#  if [ -e ${LOCKFILE} ]; then
#
#    while $(wget -qO- "$(cat ${LOCKFILE})/api/xml?xpath=*/building/text%28%29") && [ ! -e fuego_test_successfully_built ]
#    do
#      sleep 5
#    done
#  fi
#
#  echo "${BUILD_URL}" > ${LOCKFILE}
#}

function build_error {
    abort_job "Build failed: $@"
}

# process Rebuild flag, and unpack test sources if necessary.
function pre_build {
    cd ${WORKSPACE}

    # If the spec requests it, create a job-specifc
    # build directory.
    # e.g. "PER_JOB_BUILD": "true" in spec file
    upName="${TESTDIR^^}"
    pjName="${upName//[-,.]/_}_PER_JOB_BUILD"
    if [ "${!pjName}" == "true" ] ; then
        mkdir -p $JOB_BUILD_DIR
    else
        mkdir -p $TEST_BUILD_DIR
        if [ ! -L $JOB_BUILD_DIR -a ! -d $JOB_BUILD_DIR ] ; then
            ln -s $TEST_BUILD_DIR $JOB_BUILD_DIR
        fi
    fi
    cd $JOB_BUILD_DIR

    lock_build_dir

    if [ "$Rebuild" = "false" ] && [ -e fuego_test_successfully_built ]; then
        iprint "The test is already built"
    else
        find . -maxdepth 1 -mindepth 1 ! -name "fuego.build.lock" -print0 | xargs -0 rm -rf
        unpack || abort_job "Error while unpacking"
    fi

    # Record the version of the test source
    if [ -e fuego_tarball_src_md5sum ]; then
        TESTSUITE_VERSION=$(cat fuego_tarball_src_md5sum | awk '{ print $1 }')
    elif [ -d ".git" ]; then
        TESTSUITE_VERSION=$(git log -1 --format="%H")
    else
        TESTSUITE_VERSION="unknown"
    fi
    export TESTSUITE_VERSION
    vprint "Testsuite version: $TESTSUITE_VERSION"
}

function build {
    local ret

    if [ "$USE_BINARY_PACKAGES" == "true" ] ; then
        BP_FILE="${TOOLCHAIN}-${TESTDIR}-binary-package.ftbp"
        BP_FILEPATH="$FUEGO_RW/cache/$BP_FILE"
        if [ -f $BP_FILEPATH ] ; then
            build_duration=0
            iprint "Using binary package $BP_FILEPATH"
            iprint "Fuego test_build duration=$build_duration seconds"
            return 0
        fi
    fi

    pre_build
    if [ -e fuego_test_successfully_built ]; then
        build_duration=0
    else
        build_start_time=$(date +"%s.%N")
        call_if_present test_build
        ret=$?
        build_end_time=$(date +"%s.%N")
        build_duration=$(python -c "print $build_end_time - $build_start_time")

        # test_build may change the current dir
        # get back to root of build dir, before 'touch'
        cd ${WORKSPACE}/${JOB_BUILD_DIR}
        if [ $ret -eq 0 ]; then
            touch fuego_test_successfully_built
        else
            abort_job "ERROR: test_build returned $ret"
        fi
    fi
    iprint "Fuego test_build duration=$build_duration seconds"
    post_build
}

function post_build {
    # Normally, we wait until post_deploy to unlock the build dir
    # (so another test can't remove the build directory before we deploy
    # stuff to the board).
    # However, if we're not doing a deploy phase, then unlock now
    if [[ ! "$FUEGO_TEST_PHASES" == *deploy* ]] ; then
        unlock_build_dir
    fi
}

function deploy {
  pre_deploy

  if [[ "$FUEGO_TEST_PHASES" == *makepkg* ]] ; then
    # force deploy to local area
    TRANSPORT=local
    BOARD_TESTDIR=$FUEGO_RW/stage
    mkdir -p $BOARD_TESTDIR/fuego.$TESTDIR
  fi

  if [ "$USE_BINARY_PACKAGES" == "true" ] ; then
    BP_FILE="${TOOLCHAIN}-${TESTDIR}-binary-package.ftbp"
    BP_FILEPATH="$FUEGO_RW/cache/$BP_FILE"
    if [ -f $BP_FILEPATH ] ; then
      put $BP_FILEPATH $BOARD_TESTDIR/fuego.$TESTDIR/$BP_FILE
      cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR ; tar -xf $BP_FILE"
      rm "$BOARD_TESTDIR/fuego.$TESTDIR/$BP_FILE"
      rm "$BOARD_TESTDIR/fuego.$TESTDIR/binary-package.json"
      post_deploy
      return
    fi
  fi

  call_if_present test_deploy
  post_deploy
}

function pre_deploy {
  cd "$WORKSPACE/$JOB_BUILD_DIR"
}

function post_deploy {
  unlock_build_dir
}

function makepkg {
  CACHE_DIR=$FUEGO_RW/cache
  mkdir -p $CACHE_DIR

  DEPLOY_DIR=$BOARD_TESTDIR/fuego.$TESTDIR
  # make the binary-package.json file
  cat > $DEPLOY_DIR/binary-package.json <<BP_JSON_TEMPLATE
{
    "board": "$NODE_NAME",
    "build_host": "$FUEGO_HOST",
    "fuego_core_version": "$FUEGO_CORE_VERSION",
    "fuego_version": "$FUEGO_VERSION",
    "schema_version": "1.0",
    "test_name": "$TESTDIR",
    "testsuite_version": "$TESTSUITE_VERSION",
    "timestamp": "$BUILD_TIMESTAMP",
    "toolchain:": "$TOOLCHAIN"
}
BP_JSON_TEMPLATE

  TAR_FILENAME=$CACHE_DIR/$TOOLCHAIN-$TESTDIR-binary-package.ftbp
  tar czv -C $DEPLOY_DIR -f $TAR_FILENAME ./
}

# take a snapshot of system state to save with run data
function snapshot {
    # Get target device firmware.
    firmware
    iprint "Firmware revision: $FWVER"

    # call 'test_snapshot' if declared, otherwise get regular stuff
    export SNAPSHOT_FILENAME="$LOGDIR/machine-snapshot.txt"
    echo "Test host environment:" >$SNAPSHOT_FILENAME
    env | sort >>$SNAPSHOT_FILENAME
    echo "=================================================================" >>$SNAPSHOT_FILENAME
    # save DUT Firmware version
    echo "Firmware revision: $FWVER" >>$SNAPSHOT_FILENAME

    if declare -f -F test_snapshot >/dev/null ; then
        test_snapshot ;
    else
        echo "Board state information:" >>$SNAPSHOT_FILENAME
        # record memory and disk status as well as non-kernel processes, and interrupts
        ov_rootfs_state >>$SNAPSHOT_FILENAME
        return 0
    fi
}

function firmware {
  ov_get_firmware
  export FWVER="$FW"
}

function target_setup_route_to_host () {
    # $1 - subnet address
    # $2 - netmask
    # $3 - gateway address
    # $4 - network interface

    cmd "true" || abort_job "Cannot connect to $NODE_NAME via $TRANSPORT"
    cmd "if /sbin/route | grep $1; then echo \"route to $1 already configured\"; else /sbin/route add -net $1 netmask $2 gw $3 dev $4; fi"
}

function pre_test {
    export SSHPASS=$PASSWORD

    if [ "$Reboot" == "true" ]; then
        target_reboot ${MAX_REBOOT_RETRIES:-20}
    fi

    # Make sure the target is alive, and prepare workspace for the test
    source $FUEGO_RO/toolchains/tools.sh

    is_empty $TESTDIR

    # create a stub run.json file with some of the test information
    make_pre_run_file

    # Setup routing to target if needed
    if [ -n "$TARGET_SETUP_LINK" ] ; then
        $TARGET_SETUP_LINK || abort_job "Cannot connect setup LINK to $NODE_NAME via $TARGET_SETUP_LINK "
    fi

    # manage any errors directly
    # that is, don't let a connection failure trigger the signal_handler
    set +e

    # Setup routing to target and login to target, if needed
    ov_transport_connect

    # Check if DUT is operational
    if ! cmd "true" ; then
        # if not, try a hardware reboot
        echo "Warning: Board not responsive - trying hardware reboot"
        if [ -n "${BOARD_CONTROL}" ] ; then
            ov_board_control_reboot
            ov_transport_connect ${MAX_REBOOT_RETRIES:-20}
        fi
        cmd "true" || abort_job "Cannot connect to $NODE_NAME via $TRANSPORT"
    fi
    set -e

    # Target cleanup flag check
    [ "$Target_PreCleanup" = "true" ] && target_cleanup $TESTDIR || true

    export LOGDIR="$FUEGO_RW/logs/$TESTDIR/${NODE_NAME}.${TESTSPEC}.${BUILD_NUMBER}.${BUILD_ID}"

    # see if test dependencies (expressed by NEED_ vars) are met
    check_needs || abort_job "Test dependencies (expressed by NEED variables) not met"

    if [[ "$FUEGO_TEST_PHASES" == *pre_check* ]] ; then
        call_if_present test_pre_check
    fi

    # FIXTHIS: Sync date/time between target device and framework host

    # make $BOARD_TESTDIR, if one is specified
    #ov_init_dir $BOARD_TESTDIR || \
    #    abort_job "ERROR: cannot find nor create $BOARD_TESTDIR"

    local fuego_test_dir=$BOARD_TESTDIR/fuego.$TESTDIR

    # use a /tmp dir in case logs should be on a different partition
    # a board file can override the default of /tmp by setting FUEGO_TARGET_TMP
    local fuego_test_tmp=${FUEGO_TARGET_TMP:-/tmp}/fuego.$TESTDIR

    ov_remove_and_init_dir ${fuego_test_dir} ${fuego_test_tmp} || \
        abort_job "Could not create directory on $NODE_NAME (either ${fuego_test_dir} or $fuego_test_tmp})"

    # note that dump_syslogs (below) creates ${fuego_test_tmp} if needed

    # Log test name
    ov_logger "Starting test ${JOB_NAME}"

    dump_syslogs ${fuego_test_tmp} "before"

    # flush buffers to physical media and drop filesystem caches to make system load more predictable during test execution
    ov_rootfs_sync

    ov_rootfs_drop_caches
}

function fetch_results {
    local fuego_test_tmp=${FUEGO_TARGET_TMP:-/tmp}/fuego.$TESTDIR

    # get FWVER if we haven't already
    if [ -z "$FWVER" ] ; then
        firmware
    fi
    get $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR.log ${LOGDIR}/testlog.txt || \
        echo "INFO: the test did not produce a test log on the target" | tee -a ${LOGDIR}/testlog.txt

    if [ -f ${LOGDIR}/livelog.txt ] ; then
        cat ${LOGDIR}/livelog.txt >> ${LOGDIR}/testlog.txt
    fi

    if [ -f ${LOGDIR}/hostlog.txt ] ; then
        cat ${LOGDIR}/hostlog.txt >> ${LOGDIR}/testlog.txt
    fi

    # Get syslogs
    dump_syslogs ${fuego_test_tmp} "after"

    # Don't fail if 'before' syslog is missing
    #  This can happen if the board reboots, and the board log directory
    #  was a true temporary (ephemeral) directory
    get_syslog ${fuego_test_tmp} "before" ${LOGDIR}/syslog.before.txt || echo "Fuego error: Could not read 'before' syslog from board" >${LOGDIR}/syslog.before.txt
    get_syslog ${fuego_test_tmp} "after" ${LOGDIR}/syslog.after.txt

    if [ ! -f ${LOGDIR}/syslog.before.txt ] ; then
        wprint "Can't read 'before' system log, possibly because /tmp was cleared on boot"
        echo "Consider setting FUEGO_TARGET_TMP in your board file to a directory on target that won't get cleared on boot"
        touch ${LOGDIR}/syslog.before.txt
    fi

    call_if_present test_fetch_results
}

# process the fetched data
function processing {
    if [ "$REPORT_RETURN_VALUE" != "0" ] ; then
        wprint "Program returned exit code '$REPORT_RETURN_VALUE'"
        wprint "Log evaluation may be invalid"
        RETURN_VALUE=$REPORT_RETURN_VALUE
    fi

    # FIXTHIS - should review this order for failure evaluation
    # I'm not sure it's the best order
    call_if_present test_processing && rc=0 || rc=$?
    if [ $rc -ne 0 ]; then
        echo "ERROR: test_processing returned an error"
        RETURN_VALUE=1
    fi

    fail_check_cases && rc=0 || rc=$?
    if [ $rc -ne 0 ]; then
        echo "ERROR: fail_check_cases returned an error"
        RETURN_VALUE=1
    fi

    syslog_cmp && rc=0 || rc=$?
    if [ $rc -ne 0 ]; then
        echo "ERROR: syslog_cmp returned an error"
        RETURN_VALUE=1
    fi

    # make a convenience link to the Jenkins console log, if the log doesn't exist
    # this code assumes that if consolelog.txt doesn't exist, this was a Jenkins job build,
    # and the console log is over in the jenkins build directory.
    if [ ! -e $LOGDIR/consolelog.txt ] ; then
        ln -s "/var/lib/jenkins/jobs/${JOB_NAME}/builds/${BUILD_NUMBER}/log" $LOGDIR/consolelog.txt
    fi

    # the parser generates a single 'run.json' file for each run
    # RETURN_VALUE should be set now

    # NOTE: if RETURN_VALUE is not set, or not an integer, then
    #   generic_parser will flag the result as an ERROR.
    PYTHON_ARGS="-W ignore::DeprecationWarning -W ignore::UserWarning"
    if [[ ! "$PYTHONPATH" =~ "$FUEGO_CORE/scripts/parser" ]] ; then
        export PYTHONPATH="$FUEGO_CORE/scripts/parser:$PYTHONPATH"
    fi
    if [ -e "$TEST_HOME/parser.py" ] ; then
        run_python $PYTHON_ARGS $TEST_HOME/parser.py && rc=0 || rc=$?
    else
        run_python $PYTHON_ARGS $FUEGO_CORE/scripts/generic_parser.py && rc=0 || rc=$?
    fi

    if [ $rc -eq 1 ]; then
        echo "ERROR: results did not satisfy the threshold"
        RETURN_VALUE=1
    fi
}

# search in testlog.txt and syslog (if use_syslog is defined in the spec)
# for {!TESTDIR}_FAIL_REGEXP_n fail cases (fail_regexp in the spec) and
# abort with message {!TESTDIR}_FAIL_MESSAGE_n (fail_message in the spec)
# if found.
function fail_check_cases () {
    testlog="${LOGDIR}/testlog.txt"
    slog_prefix="${LOGDIR}/syslog"

    upName="${TESTDIR^^}"
    upNameUnder="${upName//[-,.]/_}"
    fcname="${upNameUnder}_FAIL_CASE_COUNT"

    fcc="${!fcname}"

    if [ -z "$fcc" ]; then
        return 0
    fi

    echo "Going to check $fcc fail cases for $JOB_NAME"

    fcc=$(( $fcc - 1 ))

    for n in $(seq 0 $fcc)
    do
        fpvarname="${upNameUnder}"_FAIL_REGEXP_"${n}"
        fpvarmsg="${upNameUnder}"_FAIL_MESSAGE_"${n}"
        fpvarslog="${upNameUnder}"_FAIL_"${n}"_SYSLOG

        fptemplate="${!fpvarname}"
        fpmessage="${!fpvarmsg}"
        fpslog="${!fpvarslog}"

        if [ ! -z "$fpslog" ]
        then

            if diff -ua ${slog_prefix}.before.txt ${slog_prefix}.after.txt | grep -vEf "$FUEGO_CORE/scripts/syslog.ignore" | grep -E -e $fptemplate;
            then
                echo "Detected fail message in syslog diff: $fpmessage"
                return 1
            else
                continue
            fi
        fi

        if grep -e "$fptemplate" $testlog ;
        then
            echo "Detected fail message in $testlog: $fpmessage"
            return 1
        fi
    done

    echo "No fail cases detected for $JOB_NAME"
}

# $@ are process names to kill on the target
function kill_procs {
    ov_rootfs_kill "$@"
}

# $1 is the function to call, $2... have arguments to the function
function call_if_present {
    if declare -f -F $1 >/dev/null ; then
        $@ ;
    else
        return 0
    fi
}

# create the run.json file
# NOTE: it will get re-created at the end of the test with results, but
# this is a placeholder in case the test aborts before that happens.
# this routine uses a whole lot of variables
function make_pre_run_file {
    cat > $LOGDIR/run.json <<RUN_JSON_TEMPLATE
{
    "duration_ms": 0,
    "metadata" : {
        "attachments": [],
        "batch_id": "$FUEGO_BATCH_ID",
        "board": "$NODE_NAME",
        "build_number": "$BUILD_NUMBER",
        "compiled_on": "docker",
        "fuego_core_version": "$FUEGO_CORE_VERSION",
        "fuego_version": "$FUEGO_VERSION",
        "host_name": "$FUEGO_HOST",
        "keep_log": "true",
        "reboot": "$Reboot",
        "rebuild": "$Rebuild",
        "start_time": $FUEGO_START_TIME,
        "target_postleanup": "$Target_PostCleanup",
        "target_precleanup": "$Target_PreCleanup",
        "test_plan": "$TESTPLAN",
        "test_spec": "$TESTSPEC",
        "timestamp": "$BUILD_TIMESTAMP",
        "testsuite_version": "$TESTSUITE_VERSION",
        "toolchain:": "$TOOLCHAIN",
        "workspace": "$WORKSPACE"
    },
    "name": "$TESTDIR",
    "schema_version": "1.0",
    "status": "ERROR"
}
RUN_JSON_TEMPLATE
}


# capture SIGTERM during post_test
# if Jenkins is trying to abort us, it sends SIGTERM multiple times
# but we'd rather finish up post_test than stop
function post_term_handler {
  echo "Received SIGTERM during post_test - ignoring it"
}

function post_test {
    # reset the signal handler to avoid an infinite loop
    trap post_term_handler SIGTERM
    trap - SIGHUP SIGALRM SIGINT ERR EXIT

    if [ "${FUEGO_RECEIVED_SIGNAL}" = "true" ] ; then
        # the board may have hung (a kernel oops)
        # see if the board is responsive, and if not, try to reboot it
        set +e
        if ! cmd "true" ; then
            if [ -n "${BOARD_CONTROL}" ] ; then
                ov_board_control_reboot
                ov_transport_connect ${MAX_REBOOT_RETRIES:-20}
                cmd "true" || abort_job "ERROR: Cannot connect to board after reboot\n"
            else
                abort_job "ERROR: Cannot connect to board for test post-processing"
            fi
        fi
        set -e
    fi

    # log test completion message.
    # but don't let user confuse termination with success
    ov_logger "Test $TESTDIR is finished - maybe successfully"

    # we try to fetch the logs, even if we're called by the
    # signal handler.  It's possible there are partially-complete
    # results that could be post-processed.
    fetch_results
    cleanup
}

function cleanup {
  # call test_cleanup if defined
  # this is for killing runaway processes, and clearing out
  # anything outside the test directories
  call_if_present test_cleanup

  # Remove work and log dirs
  [ "$Target_PostCleanup" = "true" ] && target_cleanup $TESTDIR || true

  vprint "Teardown board link"

  # NOTE: this only needs an "|| true" because it's the last statement
  # in the function.  If the function changes, this could be removed.
  [ -n "$TARGET_TEARDOWN_LINK" ] && $TARGET_TEARDOWN_LINK || true

  ov_transport_disconnect || true
}

function target_cleanup {
  local fuego_test_tmp=${FUEGO_TARGET_TMP:-/tmp}/fuego.$1

  cmd "rm -rf $BOARD_TESTDIR/fuego.$1 ${fuego_test_tmp}"
}

# Reboot the target and wait until we reconnect to it
#
# Usage: target_reboot MAX_REBOOT_RETRIES
#
# [Note] retries are used instead of a simple timeout because
# 'cmd' may have an underlying timeout (e.g.: ConnectTimeout=15
# for ssh).
function target_reboot {
  # set +e because some operations may fail before reboot completes
  set +e

  ov_rootfs_reboot
  # give time for reboot to turn off networking, before waiting
  # for target to come back up
  sleep 15

  # pass max_retries to ov_transport_connect
  ov_transport_connect $1
  set -e
  cmd "true" || abort_job "ERROR: Cannot connect to board after reboot\n"
}

# $1 - tarball template
function build_cleanup {
 rm -rf ${1}-${TOOLCHAIN}
}

# FIXTHIS: log_compare is not using $1
function log_compare {
    # 1 - $TESTDIR, 2 - number of results, 3 - Regex, 4 - n, p (i.e. negative or positive)
    local RETURN_VALUE=0
    local PARSED_LOGFILE="testlog.${4}.txt"

    if [ -f ${LOGDIR}/testlog.txt ]; then
        current_count=`cat ${LOGDIR}/testlog.txt | grep -E "${3}" 2>&1 | wc -l`
        if [ "$4" = "p" ]; then
            if [ $current_count -ge $2 ] ; then
                iprint "log_compare: pattern '$3' found $current_count times (expected greater or equal than $2)"
            else
                eprint "ERROR: log_compare: pattern '$3' found $current_count times (expected greater or equal than $2)"
                RETURN_VALUE=1
            fi
        fi

        if [ "$4" = "n" ]; then
            if [ $current_count -le $2 ] ; then
                echo "log_compare: pattern '$3' found $current_count times (expected less or equal than $2)"
            else
                echo "ERROR: log_compare: pattern '$3' found $current_count times (expected less or equal than $2)"
                RETURN_VALUE=1
            fi
        fi
    else
        echo -e "\nFuego error reason: '$LOGDIR/testlog.txt' is missing.\n"
        RETURN_VALUE=1
    fi

    return $RETURN_VALUE
}

function syslog_cmp {
  PREFIX="$LOGDIR/syslog"
  rc=0
  if [ -f ${PREFIX}.before.txt ]; then
    if diff -ua ${PREFIX}.before.txt ${PREFIX}.after.txt | grep -vEf "$FUEGO_CORE/scripts/syslog.ignore" | grep -E -e '\.(Bug:|Oops)'; then
      rc=1
    fi
  # else # special case for "reboot" test
    # if grep -vE -e '\.(info|notice|debug|warn)|Boot Reason: Warm' -f "$FUEGO_CORE/scripts/syslog.ignore" ${PREFIX}.after; then
    #   rc=1
    # fi
  fi
  [ $rc -eq 1 ] && echo -e "\nFuego error reason: Unexpected syslog messages.\n"
  return $rc
}

# check is variable is set and fail if otherwise
function check_capability () {
    varname=CAP_$1
    if [ -z "${!varname}" ]
    then
        abort_job "CAP_$1 is not defined. Make sure you use correct overlay with required specs for this test/benchmark"
    fi
}

# check that a process ($1 is the process name) is running
function check_process_is_running {
    pgrep $1 && rc=0 || rc=$?
    if [ $rc -eq 1 ]; then
        abort_job "process $1 is not running"
    fi
}

function hd_test_mount_prepare () {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ]
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV || /bin/true"
        cmd "mount $HD_MOUNT_BLOCKDEV $HD_MOUNT_POINT"
    fi

    cmd "mkdir -p $HD_MOUNT_POINT/fuego.$TESTDIR"
}

function hd_test_clean_umount() {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    cmd "rm -rf $HD_MOUNT_POINT/fuego.$TESTDIR"

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ]
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV"
    fi
}

# check for a program or file on the target, and set a variable if it's present
# $1 - file, dir or program on target
# $2 - variable to set during the build
# $3 - (optional) set of paths to look for on target
#      if $3 is not specified, a find is done from the root
#      this requires the 'find' command on the target
function is_on_target {
    # FIXTHIS: race condition
    tmpfile=$(mktemp /tmp/found_loc.XXXXXX)
    cmd "touch $tmpfile"
    if [ -z "$3" ] ; then
        safe_cmd "find / -name \"$1\" | head -n 1 >$tmpfile"
    else
        # split search path on colon
        for d in $(echo "$3" | tr ":" "\n") ; do
            # execute a command on the target to detect $d/$1
            cmd "if [ -z \"\$(cat $tmpfile)\" -a -e \"$d/$1\" ] ; then echo \"$d/$1\" >$tmpfile ; fi"
        done
    fi
    get $tmpfile $tmpfile
    LOCATION=$(cat $tmpfile)
    export $2=$LOCATION
    cmd "rm $tmpfile"
    rm -f $tmpfile # -f for tests running on the host
}

# check for a program or file on a directory listed on the PATH on the target,
# and set a variable if it's present
# $1 - file, dir or program on target
# $2 - variable to set during the build
function is_on_target_path {
    TARGET_PATH=$(cmd "echo \$PATH")
    is_on_target $1 $2 $TARGET_PATH
}

# check for a library on the SDK, and set a variable if it's present
# $1 - the file (library) we want to search for in the SDK
# $2 - variable to set the location (if the file is found)
# $3 - (optional) set of paths joined by colons to look for on the SDK
# returns with the desired variable ($2) set, if the item was found
# also, sets the path in the variable LOCATION
function is_on_sdk {
    # FIXTHIS: race condition
    tmpfile=$(mktemp /tmp/found_loc.XXXXXX)
    touch $tmpfile
    if [ -z "$3" ] ; then
        find / -name "$1" | head -n 1 >$tmpfile
    else
        # split search path on colon
        for d in $(echo "$3" | tr ":" "\n") ; do
            echo "search $d"
            find "$SDKROOT$d" -name "$1" | head -n 1 >>$tmpfile
        done
    fi
    LOCATION=$(head -n 1 $tmpfile)
    export $2=$LOCATION
    rm -f $tmpfile
}

# check for a program on the target, and abort if it's missing
# $1 has the program that is required on the target board
# this has the side effect of defining PROGRAM_$1 (uppercased)
# with the value as the directory where $1 is found on the board.
function assert_has_program_slow {
   upName=${1^^}
   progVar=PROGRAM_${upName//[-,.]/_}
   is_on_target_path $1 ${progVar}
   assert_define ${progVar} "Missing '$1' program on target board"
}

# check for a program on the target, using the dependency cache
# $1 has the program that is required on the target board
# this has the side effect of defining PROGRAM_$1 (uppercased)
# with the value as the directory where $1 is found on the board.
function check_has_program {
   upName=${1^^}
   progVar=PROGRAM_${upName//[-,.+]/_}

   # check board variables to see if it is already set
   if [ -n "${!progVar}" -a "${!progVar}" != "<missing>" ] ; then
      return
   fi
   LOCATION=$(cmd "command -v $1 || true")
   if startswith "$LOCATION" "alias" ; then
      # ugh, caught an alias, find program the slow way
      is_on_target_path $1 ${progVar}
      LOCATION=${!progVar}
   else
      export $progVar=$LOCATION
   fi

   if [ -z "$LOCATION" ] ; then
      LOCATION="<missing>"
      export $progVar=$LOCATION
   fi

   if [ "$CACHE_DEPENDENCIES" = "1" ] ; then
      # set board variable value to "<missing>" if program is missing
      ftc set-var -b $NODE_NAME $progVar=$LOCATION
   fi
}

# check for a program on the target, and abort if the program is missing
# $1 has the program that is required on the target board
# this has the side effect of defining PROGRAM_$1 (uppercased)
# with the value as the directory where $1 is found on the board.
function assert_has_program {
   upName=${1^^}
   progVar=PROGRAM_${upName//[-,.+]/_}

   check_has_program $1
   if [ -z "${!progVar}" -o "${!progVar}" = "<missing>" ] ; then
      abort_job "Missing '$1' program on target board"
   fi
}

# check for a module on the target, and abort if it is missing
# $1 has the module that is required on the target board
# this has the side effect of defining MODULE_$1 (uppercased)
# with the value as the directory where $1 is found on the board.
function assert_has_module {
   upName=${1^^}
   modVar=MODULE_${upName//[-,.]/_}
   is_on_target_module $1 ${modVar} || \
       abort_job "Missing '$1' module on target board"
}

# check if the path is an absolute path or a relative path
# $1 - the path to check
function is_abs_path {
    if [ ${1:0:1} == "/" ]; then
        # absolute path because it starts with "/"
        return 0
    else
        return 1
    fi
}

# check for a module on a directory listed on the /lib/modules in the target,
# and set a variable if it's present
# $1 - file, module on target
# $2 - variable to set during the build
function is_on_target_module {
    # clear variables, in case this is called multiple times
    LOCATION=""
    export $2=$LOCATION

    # this is really unlikely to be missing, but check just in case
    TARGET_PATH="/lib/modules"
    if [ $(cmd "test -d $TARGET_PATH ; echo \$?") != "0" ] ; then
        return 1
    fi

    # check for module builtin to kernel
    LOCATION=$(cmd "grep \"/$1.ko\" $TARGET_PATH/\$(uname -r)/modules.builtin") || true
    if [ -z "$LOCATION" ] ; then
        # check for module outside of kernel
        LOCATION2=$(cmd "grep \"/$1.ko\" $TARGET_PATH/\$(uname -r)/modules.order") || true
        if [ -n "$LOCATION2" ] ; then
            # check that module file is there
            if cmd "test -f $TARGET_PATH/\$(uname -r)/$LOCATION2" ; then
                LOCATION=$LOCATION2
            else
                return 1
            fi
        else
            return 1
        fi
    fi

    export $2=$LOCATION
    return 0
}

function allocate_next_batch_id {
    local filename=$FUEGO_RW/logs/next_batch_id
    if [ -f $filename ] ; then
        batch_id=$(cat $filename)
    else
        batch_id=1
    fi
    echo -n $(( $batch_id + 1 )) >$filename
    echo $batch_id
}

# run_test - run a sub-test using ftc
# $1 is the test name
# other positional arguments are arguments to 'ftc run-test'
# The following are optional:
#   $TC_NUM has the testcase number
#   $TC_NAME has the testcase name
#     if not defined, a testcase name is created automatically
#   $DEFAULT_TIMEOUT has the default timeout for each test
#   $FUEGO_BATCH_ID has a valid value
# At the conclusion of the test, put a line with
# status information into the host-side log (using 'log_this')
# This line is "TAP13"-like, with the following format:
# [[$FUEGO_BATCH_ID]] (ok|not ok) $TC_NUM $TC_NAME
# NOTE: The reason to prefix the line with the batch id is that
# we might be running a sub-test (even another batch test), that
# emits TAP13-style output, and we need to only read lines that
# reflect the parent test results.
function run_test {
    TEST_NAME=$1
    shift
    SAVED_POSITIONAL_ARGS="$@"

    # handle missing testcase number
    if [ -z "$TC_NUM" ] ; then
        export TC_NUM=1
    fi

    # set default timeout
    # FIXTHIS - I'm not sure a default timeout is needed here (in run_test)
    # would it be better to parse this from the json?
    if [ -z "$DEFAULT_TIMEOUT" ] ; then
        DEFAULT_TIMEOUT="30m"
    fi
    RUN_TEST_ARGS=""
    if [[ ! "$@" =~ "--timeout" ]] ; then
        RUN_TEST_ARGS="--timeout $DEFAULT_TIMEOUT"
    fi

    # handle missing batch id
    if [ -z "$FUEGO_BATCH_ID" ] ; then
        FUEGO_BATCH_ID="$(allocate_next_batch_id)"
    fi

    # prepare to receive special data from the sub-test
    FUEGO_SPECIAL_DATA_FILE=$(mktemp /tmp/special_data.XXXXXX)
    export FUEGO_SPECIAL_DATA_FILE

    # make jenkins the file owner, so that a nested ftc (running as user
    # 'jenkins') can access it (also, don't abort on failure)
    chown jenkins.jenkins $FUEGO_SPECIAL_DATA_FILE || true

    export FUEGO_CALLER="nested"
    log_this "echo \"Running test $TEST_NAME\""
    ftc run-test -b $NODE_NAME -t $TEST_NAME $RUN_TEST_ARGS $@
    local result=$?

    # get build_number and spec for the test just executed, so we
    # can put a reference in the batch testlog

    build_number=$(cat $FUEGO_SPECIAL_DATA_FILE | grep ^BUILD_NUMBER= | sed s/BUILD_NUMBER=//)
    spec=$(cat $FUEGO_SPECIAL_DATA_FILE | grep ^TESTSPEC= | sed s/TESTSPEC=//)
    rm $FUEGO_SPECIAL_DATA_FILE || true
    unset FUEGO_SPECIAL_DATA_FILE
    log_this "echo \"Log dir: logs/$TEST_NAME/$NODE_NAME.$spec.$build_number.$build_number\""
    if [ -n "$JENKINS_URL" ] ; then
        log_this "echo \"Job page: ${JENKINS_URL}job/$NODE_NAME.$spec.$TEST_NAME/$build_number/\""
    fi

    # Record the result in the batch testlog
    if [ -z "$TC_NAME" ] ; then
        TC_NAME="$TEST_NAME"
        if [ -n "$SAVED_POSITIONAL_ARGS" ] ; then
            TC_NAME="$TEST_NAME $SAVED_POSITIONAL_ARGS"
        fi
    fi

    if [ $result == "0" ] ; then
        RESULT_STR="ok"
    else
        RESULT_STR="not ok"
    fi

    log_this "echo \"[[$FUEGO_BATCH_ID]] $RESULT_STR $TC_NUM - $TC_NAME\""
    TC_NUM=$(( TC_NUM + 1 ))
    TC_NAME=""
    return $result
}

# add some 'core' variables for Fuego self-test
if [ $TESTDIR = "Functional.fuego_test_variables" ] ; then
    # these should override values from other places
    export FTV_VAR_07="core seven"
    export FTV_VAR_13="core thirteen"
    export FTV_VAR_18="core eighteen"
    export FTV_VAR_22="core twenty-two"
    export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_25="core twenty-five"
    export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_27="core twenty-seven"

    # this should be overriden by a value from fuego_test.sh
    export FUNCTIONAL_FUEGO_TEST_VARIABLES_VAR_29="core twenty-nine"
fi
