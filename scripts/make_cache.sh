#!/bin/bash
#
# make_cache - make a binary test program cache (set of tar files) for
# the indicated board.  This will build a cache of all tests.
#
# Usage: make_cache <board> [<tests>]

usage() {
    echo "Usage: make_cache [<options>] <board> [test1 [test2...]]"
    echo
    echo "If no tests are specified, make binary packages for all tests."
    echo
    echo "Options:"
    echo "-h = show this usage help"
    exit 1
}

if [ -z "$1" ] ; then
    echo "Error: Missing arguments"
    usage
fi

# parse command line options
while [ $(echo -- $1 | cut -b 4) = "-" ] ; do
    case $1 in
        -h|--help) usage;;
        --) shift; break;;
        -*) echo "Invalid option: $1" ; usage ;;
    esac
done

if [ -z "$FUEGO_RW" ] ; then
    export FUEGO_RW=/fuego-rw
fi

# get board
board=$1
if [ -z "$board" ] ; then
    echo "Error: Missing board"
    usage
fi

shift

# see if one or more tests was specified
if [ -n "$1" ] ; then
	tests="$*"
else
	tests=$(ftc -q list-tests)
fi

mkdir -p $FUEGO_RW/cache
chown jenkins.jenkins $FUEGO_RW/cache
CACHE_LOG=$FUEGO_RW/cache/cache.log

#
# FIXTHIS - need to mark this as NO_BOARD_CONTACT
echo "=============================================" | tee -a $CACHE_LOG
date | tee -a $CACHE_LOG
for test in $tests ; do
    echo "Making cache package for $test" | tee -a $CACHE_LOG
    ftc run-test -b $board -t $test -p "pbdm" >> $CACHE_LOG
done
