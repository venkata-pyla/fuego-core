# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains overlay functionality: checking necessary overlay variables and generating prolog.sh file


assert_define "NODE_NAME"
assert_define "TESTSPEC"
assert_define "BUILD_NUMBER"
assert_define "TESTDIR"

OF_CLASSDIRS="$FUEGO_CORE/overlays/base"
OF_OVGEN="$FUEGO_CORE/scripts/ovgen.py"

#OF_DEBUG_ARGS="--debug 3"
OF_DEBUG_ARGS=""

function set_overlay_vars() {
    # By convention board configuration filenames should be the same as node names
    OF_BOARD_FILE="$FUEGO_RO/boards/$NODE_NAME.board"
    if [ ! -f $OF_BOARD_FILE ] ; then
        abort_job "$OF_BOARD_FILE does not exist"
    fi
    OF_BOARD_VAR_FILE="$FUEGO_RW/boards/$NODE_NAME.vars"
    if [ ! -f $OF_BOARD_VAR_FILE ] ; then
        OF_BOARD_VAR_FILE=""
    fi

    # check for DISTRIB, which has to be handed to the overlay generator
    # in order to select the right "distribution" class of functions
    #
    # DISTRIB could come from several places:
    #   ftc could set it using auto-detection (in which case, it's already set)
    #   it could be in the board.var file (detect that with highest precedence)
    #   it could be in the board file (detect that next)
    #   nobody set it = then set it to "nosyslogd.dist"
    set +e
    if [ -z "$DISTRIB" -a -n "$OF_BOARD_VAR_FILE" ] ; then
        # check board stored variables file
        DISTRIB_LINE=$(grep ^DISTRIB= $OF_BOARD_VAR_FILE)
        if [ -n "$DISTRIB_LINE" ] ; then
            distrib_array=(${DISTRIB_LINE//=/ })
            d_value=${distrib_array[1]}
            # strip leading and trailing quotes
            d_valtemp=${d_value#'"'}
            d_value=${d_valtemp%'"'}
            DISTRIB="${d_value}"
        fi
    fi
    if [ -z "$DISTRIB" ] ; then
        # check board file
        DISTRIB_LINE=$(grep ^DISTRIB= $OF_BOARD_FILE)
        if [ -n "$DISTRIB_LINE" ] ; then
            distrib_array=(${DISTRIB_LINE//=/ })
            d_value=${distrib_array[1]}
            # strip leading and trailing quotes
            d_valtemp=${d_value#'"'}
            d_value=${d_valtemp%'"'}
            DISTRIB="${d_value}"
        fi
    fi
    set -e

    if [ -z "$DISTRIB" ]; then
        # FIXTHIS: automatically discover the best option
        # (actually, should do this in ftc, not here)
        DISTRIB="nosyslogd.dist"
    fi
    OF_DISTRIB_FILE="$FUEGO_CORE/overlays/distribs/$DISTRIB"
    if [ ! -f $OF_DISTRIB_FILE ] ; then
        abort_job "$OF_DISTRIB_FILE does not exist"
    fi
    vprint "Using $DISTRIB distribution overlay"

    # Create the log directory for this test run here so we can place the prolog.sh
    export LOGDIR="$FUEGO_RW/logs/$TESTDIR/${NODE_NAME}.${TESTSPEC}.${BUILD_NUMBER}.${BUILD_ID}"
    mkdir -p $LOGDIR

    rm -f $LOGDIR/prolog.sh

    # The overlay generator places prolog.sh into the specified logdir,
    # as well as the spec.json file used for this run
    export TEST_HOME
    run_python $OF_OVGEN $OF_DEBUG_ARGS --classdir $OF_CLASSDIRS --ovfiles $OF_DISTRIB_FILE $OF_BOARD_FILE $OF_BOARD_VAR_FILE --testdir $TESTDIR --testspec $TESTSPEC --logdir $LOGDIR || abort_job "Error generating prolog.sh file"

    if [ ! -f "$LOGDIR/prolog.sh" ]
    then
        abort_job "$LOGDIR/prolog.sh not found"
    fi

    # FIXTHIS: add BUILD_TIMESTAMP and other variables to prolog.sh for --replay
    source $LOGDIR/prolog.sh
}

