#!/usr/bin/python
# deorphan-runs.py - find Fuego runs that are not in Jenkins and add them
#
# Usage: deorphan-runs.py [-h] [-b] [-j] <pattern>
#
# This should be run inside the docker container,
#  with FUEGO_RW at /fuego-rw, FUEGO_RO at /fuego-ro
#  and jenkins at /var/lib/jenkins
#
# steps:
#  identify an orphan run
#     indicate if run has no jenkins job
#     indicate if run has no jenkins build
#  create jenkin materials for run

import sys
import os
import re
import datetime
import json
import subprocess

FUEGO_RW="/fuego-rw"
FUEGO_RO="/fuego-ro"
JENKINS_HOME="/var/lib/jenkins"

# setup server later
server = None

def usage():
    print """Usage: deorphan-runs.py [-h] [-j] [-b] [<pattern>]
This program finds Fuego test runs that do not have corresponding Jenkins
builds (so-called 'orphan runs'). It can be used create Jenkins jobs
and builds for this orphan runs.

With no arguments, show the list of missing jobs and builds for the
currently existing Fuego test runs.  If a pattern is specified, only
show data for orphan runs matching that pattern.

This script should be run inside the docker container where Jenkins resides.

Options:
 -h, --help  Show this usage help.
 -j          Create missing jobs for selected orphan runs.
 -b          Create missing builds for selected orphan runs.

Examples:
    deorphan-run.py hello_world
       Show the missing jobs and builds for the 'hello_world' tests.

    deorphan-run.py hello_world.*docker
       Show the missing items for the hello_world tests on the docker board.

    deorphan-run.py -j -b hello_world.*docker
       Create any needed jobs and builds for the indicated orphan runs.
"""
    sys.exit(0)

class data_class:
    def __init__(self):
        self.data = {}

    def __getitem__(self, key):
        # return value for key
        if self.data.has_key(key):
            item = self.data[key]
        elif hasattr(self, key):
            item = getattr(self, key)
        else:
            if self.data.has_key("default"):
                item = self.data["default"]
            else:
                item = "missing data value for key '%s'" % key

        if callable(item):
            return item(self)
        else:
            return item

def populate_build_data():
    # construct the build_data map to hold information about this build
    build_data = data_class()
    build_data.board_name = board_name
    build_data.test_name = test_name
    build_data.testplan_name = "None"
    build_data.spec_name = test.spec
    build_data.test_home = get_test_home_dir(conf, test_name)
    test_name = os.path.basename(test_name)
    build_data.job_name = "%s.%s.%s" % (board_name, test.spec, test_name)
    build_data.workspace = conf.FUEGO_RW+"/buildzone"
    build_data.start_time = long(time.time() * 1000)
    build_data.reboot_flag = test.reboot
    build_data.rebuild_flag = test.rebuild
    build_data.precleanup_flag = test.precleanup
    build_data.postcleanup_flag = test.postcleanup

    # FIXTHIS - do_run_test: set job description
    # set job description in run json file
    build_data.description = "Test %s run by ftc" % build_data.test_name
    return build_data


class run_class:
    def __init__(self, spec, test_name, board, rundir, num):
        self.spec = spec
        self.test_name = test_name
        self.type = test_name.split(".")[0]
        self.test = test_name.split(".")[1]
        self.board = board
        self.rundir = rundir    # place with run.json file and console log
        self.job_id = "%s.%s.%s" % (board, spec, test_name)
        self.jobdir = JENKINS_HOME + "/jobs/%s" % (self.job_id)
        self.has_job = os.path.isdir(self.jobdir)
        self.builddir = self.jobdir+"/builds/%s" % num
        self.buildxml_path = self.builddir+"/build.xml"
        self.has_build = os.path.isfile(self.buildxml_path)
        self.log_dir_name = "%s.%s.%s.%s" % (board, spec, num, num)
        self.logdir = FUEGO_RW+"/logs/%s/%s" % (test_name, self.log_dir_name)
        self.num = num          # jenkins local run number
        self.run_id = "%s-%s-%s-%s" % (test_name, spec, num, board)
        self.status = "<data not loaded>"
        self.json_data = "<data not loaded>"
        self.timestamp = "<data not loaded>"
        self.duration_ms = "<data not loaded>"
        self.charset = "US-ASCII"
        self.keep_log = "false"
        self.built_on = self.board
        self.run_data = None

    def __getitem__(self, key):
        # return value for key
        if hasattr(self, key):
            return getattr(self, key)
        else:
            raise KeyError(key)

    def get_run_data_from_json_file(self):
        jpath = self.rundir+os.sep+"run.json"
        if not os.path.isfile(jpath):
            print ("Error: missing run.json file for run %s" % self.run_id)
            return False

        infile = open(jpath, "r")
        run_data = json.load(infile)
        infile.close()

        self.run_data = run_data

        # FIXTHIS - should load more instance data here
        self.status = run_data["status"]
        # build_result has the jenkins string (status has the Fuego string)
        self.build_result = "FAILED"
        if self.status == "PASS":
            self.build_result = "SUCCESS"
        if self.status == "FAIL":
            self.build_result = "FAILED"
        if self.status == "ERROR":
            self.build_result = "ABORTED"
        if self.status == "SKIP":
            self.build_result = "FAILED"
        self.start_time = run_data["metadata"]["start_time"]
        self.build_number = run_data["metadata"]["build_number"]
        self.batch_id = run_data["metadata"].get("batch_id", "none")
        self.kernel_version = run_data["metadata"].get("kernel_version", "unknown")
        self.kernel = self.kernel_version
        self.testsuite_version = run_data["metadata"].get("testsuite_version", "unknown")
        self.workspace = run_data["metadata"].get("workspace", "unknown")
        self.duration_ms = str(run_data["duration_ms"])

        # convert start time to human-readable timestamp
        dt = datetime.datetime.fromtimestamp(float(self.start_time)/1000)
        self.timestamp = dt.strftime('%Y-%m-%d_%H:%M:%S')
        return True


# returns a map of {"<run-name>": run_object}
def get_runs(pattern):
    run_list = []
    log_path= FUEGO_RW + "/logs"
    tdir_list = os.listdir(log_path)
    for test_name in tdir_list:
        tpath = log_path + os.sep + test_name
        if not os.path.isdir(tpath):
            continue
        rundir_list = os.listdir(tpath)
        for run_item in rundir_list:
            rundir = tpath + os.sep + run_item
            if not os.path.isdir(rundir):
                continue
            # skip symlinks
            if os.path.islink(rundir):
                continue
            try:
                board, spec, num, junk = run_item.split(".",3)
            except ValueError:
                # ignore non-standard-named dirs
                continue
            run = run_class(spec, test_name, board, rundir, num)

            if not pattern:
                run_list.append(run)

            if pattern and re.search(pattern, run.run_id):
                run_list.append(run)

    return run_list

# this is for Jenkins versions 2.32.1 (Fuego version 1.1-1.4)
def write_build_xml_file_v2(run):
    ### the format str needs build_data to have the following attributes:
    # +test_name
    # +log_dir_name - e.g. myboard.default.1.1
    # +start_time - in milliseconds
    # +duration_ms - in milliseconds
    # description
    # +build_result - can be 'FAILURE', 'SUCCESS', or 'ABORTED'
    # +charset
    # +workspace
    # +keep_log

    # FIXTHIS: description and descriptionSetter data are for the success case only

    filename = run.builddir + "/build.xml"
    fd = open(filename, "w+")

    fd.write("""<?xml version='1.0' encoding='UTF-8'?>
<build>
  <actions>
    <hudson.model.CauseAction>
      <causeBag class="linked-hash-map">
        <entry>
          <hudson.model.Cause_-UserIdCause/>
          <int>1</int>
        </entry>
      </causeBag>
    </hudson.model.CauseAction>
    <hudson.plugins.descriptionsetter.DescriptionSetterAction plugin="description-setter@1.10">
      <description> by ftc: &lt;a href=&quot;/fuego/userContent/fuego.logs/%(test_name)s/%(log_dir_name)s/testlog.txt&quot;&gt;testlog&lt;/a&gt; &lt;a href=&quot;/fuego/userContent/fuego.logs/%(test_name)s/%(log_dir_name)s/run.json&quot;&gt;run.json&lt;/a&gt;</description>
    </hudson.plugins.descriptionsetter.DescriptionSetterAction>
    <hudson.model.ParametersAction>
      <safeParameters class="sorted-set"/>
      <parameters>
        <hudson.model.StringParameterValue>
          <name>DESCRIPTION_SETTER_DESCRIPTION</name>
          <value> &lt;a href=&quot;/fuego/userContent/fuego.logs/%(test_name)s/%(log_dir_name)s/testlog.txt&quot;&gt;testlog&lt;/a&gt; &lt;a href=&quot;/fuego/userContent/fuego.logs/%(test_name)s/%(log_dir_name)s/run.json&quot;&gt;run.json&lt;/a&gt;</value>
        </hudson.model.StringParameterValue>
      </parameters>
      <parameterDefinitionNames class="empty-list"/>
    </hudson.model.ParametersAction>
  </actions>
  <queueId>1</queueId>
  <timestamp>%(start_time)s</timestamp>
  <startTime>%(start_time)s</startTime>
  <result>%(build_result)s</result>
  <description> by ftc: &lt;a href=&quot;/fuego/userContent/fuego.logs/%(test_name)s/%(log_dir_name)s/testlog.txt&quot;&gt;testlog&lt;/a&gt; &lt;a href=&quot;/fuego/userContent/fuego.logs/%(test_name)s/%(log_dir_name)s/run.json&quot;&gt;run.json&lt;/a&gt;</description>
  <duration>%(duration_ms)s</duration>
  <charset>%(charset)s</charset>
  <keepLog>%(keep_log)s</keepLog>
  <builtOn>%(built_on)s</builtOn>
  <workspace>%(workspace)s</workspace>
  <hudsonVersion>2.32.1</hudsonVersion>
  <scm class="hudson.scm.NullChangeLogParser"/>
  <culprits class="com.google.common.collect.EmptyImmutableSortedSet"/>
</build>""" % run)

    fd.close()

def get_config(var):
    # ask ftc for config value
    cmd = "ftc config %s" % var
    p = subprocess.Popen(["ftc", "config", var], stdout=subprocess.PIPE)
    value = p.stdout.readline().strip()
    return value

def update_link(job_dir, name, build_num):
    linkname = job_dir + "/builds/" + name
    try:
        cur_num = os.readlink(linkname)
    except:
        cur_num = -1

    if int(build_num) > int(cur_num):
        try:
            os.unlink(linkname)
        except:
            pass
        os.symlink(build_num, linkname)


# create_build() populates the jenkins directory with build data from a
# run.  It assumes that the jenkins job directory already exists.
#
def create_build(run):
    global server

    job_dir = run.jobdir

    get_ok = run.get_run_data_from_json_file()
    if not get_ok:
        print "Error: Cannot create enkins build.xml file"
        return

    # update nextBuildNumber
    build_number = run.num
    nb_filename = job_dir + "/nextBuildNumber"
    next_build_number = str(int(build_number)+1)
    try:
        cur_nb = int(open(nb_filename).read())
    except:
        cur_nb = 1

    if int(next_build_number) > cur_nb:
        try:
            with open(nb_filename, "w+") as fd:
                fd.write(next_build_number+'\n')
        except:
            print "Error: problem writing to file %s" % nb_filename

    # create jenkins directory for this run
    build_dir = run.builddir
    if not os.path.isdir(build_dir):
        os.makedirs(build_dir)

    # write a build file
    write_build_xml_file_v2(run)

    # write changelog.xml file
    filename = build_dir + "/changelog.xml"
    with open(filename, "w+") as fd:
        fd.write("<log/>")

    # symlink the log file to consolelog.txt
    jenkins_log_filename = build_dir + "/log"
    log_filename = run.logdir + '/consolelog.txt'
    os.symlink(log_filename, jenkins_log_filename)

    # update last* symlinks, where needed
    # lastStableBuild - compiled ok, test ok
    # lastSuccessfulBuild - compiled ok
    # lastUnstableBuild - test not ok
    # lastUnsuccessfulBuild - aborted or error (or failure?)?
    # lastFailedBuild - compile not ok
    if run.build_result == "SUCCESS":
        update_link(job_dir, "lastSuccessfulBuild", build_number)
        update_link(job_dir, "lastStableBuild", build_number)

    if run.build_result == "FAILED":
        update_link(job_dir, "lastFailedBuild", build_number)
        update_link(job_dir, "lastUnstableBuild", build_number)
        update_link(job_dir, "lastUnsuccessBuild", build_number)

    if run.build_result == "ABORTED":
        update_link(job_dir, "lastUnsuccessfulBuild", build_number)

    # make sure everything is owned by jenkins.jenkins
    cmd = "chown -R jenkins.jenkins %s" % run.jobdir
    os.system(cmd)

    print("Build created for run %s" % run.run_id)

    # Make Jenkins reload the job data by resetting the job config
    # Note: The python 'jenkins' module doesn't appear to have a
    # reload_job() function
    if not server:
        import jenkins
        JENKINS_HOSTNAME = get_config("JENKINS_HOSTNAME")
        JENKINS_PORT = get_config("JENKINS_PORT")
        JENKINS_URL = "http://%s:%s/fuego" % (JENKINS_HOSTNAME, JENKINS_PORT)
        #print("JENKINS_URL=%s" % JENKINS_URL)
        server = jenkins.Jenkins(JENKINS_URL)

    try:
        job_config = server.get_job_config(run.job_id)
        server.reconfig_job(run.job_id, job_config)
    except:
        print("Couldn't re-sync run data for Jenkins job %s" % run.job_id)


def main():
    # parse command line arguments
    fix_builds = False
    fix_jobs = False
    if "-h" in sys.argv or "--help" in sys.argv:
        usage()

    if "-b" in sys.argv:
        sys.argv.remove("-b")
        fix_builds = True

    if "-j" in sys.argv:
        sys.argv.remove("-j")
        fix_jobs = True

    try:
        pattern = sys.argv[1]
    except IndexError:
        pattern = None

    # for debugging command line parsing
    #print("fix_builds=%s" % fix_builds)
    #print("fix_jobs=%s" % fix_jobs)
    #print("pattern=%s" % pattern)
    #print("sys.argv=%s" % sys.argv)

    runs = get_runs(pattern)
    missing_jobs = []
    missing_builds = []
    for run in runs:
        if not run.has_job:
            if run.job_id not in missing_jobs:
                missing_jobs.append(run.job_id)

        if not run.has_build:
            missing_builds.append(run)

    missing_jobs.sort()
    for job_id in missing_jobs:
        print("Missing job: %s" % job_id)
        if fix_jobs:
            board, spec, test_name = job_id.split(".",2)
            cmd = "ftc add-job -b %s -t %s -s %s" % (board, test_name, spec)
            rcode = os.system(cmd)

    missing_builds.sort()
    for run in missing_builds:
        print("Missing build: %s" % run.run_id)
        if fix_builds:
            # double-check that job exists
            # re-calculate has_job, because we may have just created the job
            run.has_job = os.path.isdir(run.jobdir)
            if not run.has_job:
                print("Error: can't create build for run due to missing job")
                continue

            create_build(run)

if __name__=="__main__":
    main()
