#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017 Sony
# Copyright (c) 2017 Toshiba corp.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# todo:
#  - add reference data to
#  - make an option to rebuild the flat_plot_data.txt from all run.json files

"""
prepare_chart_data.py - This module prepares the data used for charting
the results of tests in the Jenkins interface.
By Tim Bird (September 2017)
"""

import sys, os, re, json, collections
from filelock import FileLock
from operator import itemgetter
from fuego_parser_utils import split_test_id, get_test_case
from common import dprint, vprint, iprint, wprint, eprint, data_compare

# board testname spec build_number timestamp kernel tguid ref result
#  0       1       2     3            4        5      6    7    8
# FIXTHIS - could remove testname from flat_plot_data.txt
class flat_entry:
    def __init__(self, line):
        # default all attributes to 'undefined'
        self.board = \
        self.testname = \
        self.spec = \
        self.build_number = \
        self.timestamp = \
        self.kernel = \
        self.tguid = \
        self.test_set = \
        self.ref = \
        self.op = \
        self.result = "ERROR-undefined"
        parts = line.split()
        if len(parts) < 9:
            iprint("Error reading line '%s' in prepare_chart_data.py" % line)
            iprint("Possible data corruption in flat_plot_data.txt")

        try:
            self.board = parts[0]
            self.testname = parts[1]
            self.spec = parts[2]
            self.build_number = int(parts[3])
            self.timestamp = parts[4]
            self.kernel = parts[5]
            self.tguid = parts[6]
            self.ref = parts[7]
            self.result = parts[8]
            # old files might not have the 'op' field, parse it separately
            try:
                self.op = parts[9]
            except:
                pass

            # break apart tguid
            parts = self.tguid.split(".")
            # if the result is numeric, the item has a measure part
            try:
                dummy = float(self.result)
                self.measure = parts[-1]
                del(parts[-1])
            except:
                self.measure = None

            if len(parts)==1:
                self.test_set = "default"
            else:
                self.test_set = ".".join(parts[:-1])

            self.test_case = parts[-1]
        except:
            pass

    def __str__(self):
        return "%s %s %s %s %s %s %s %s %s\n" % \
                (self.board, self.testname, self.spec, self.build_number,
                  self.timestamp, self.kernel, self.tguid, self.ref,
                  self.result, self.op)

def get_chart_config(TESTDIR, chart_config_filename):
    # check for reading and json errors, and content errors

    # NOTE: some old chart_config files were converted to json from
    # reference.log files, but no work was done to convert them to
    # a proper format.  They are kept around to indicate the data
    # that used to be displayed under JTA.
    # FIXTHIS: old chart_config files should be updated with proper chart_types and tguids.
    chart_config=None
    if os.path.exists(chart_config_filename):
        try:
            with open(chart_config_filename) as cc_file:
                chart_config = json.load(cc_file, object_pairs_hook=collections.OrderedDict)
        except IOError:
            eprint("Problem opening chart config file (%s)" % chart_config_filename)
            sys.exit(1)
        except:
            eprint("Problem reading data from chart config file (%s)" % chart_config_filename)
            sys.exit(2)
        try:
            chart_type = chart_config["chart_type"]
        except:
            eprint("Missing chart_type in chart config file (%s)" % chart_config_filename)
            iprint("Please add 'chart_type' entry in the file")
            chart_config = None
    else:
        vprint("Missing chart_config file (%s)" % chart_config_filename)

    if not chart_config:
        iprint("chart config not found. Using default values.")
        # determine test type (Benchmark or Functional)
        try:
            test_type = TESTDIR.split(".")[0]
        except:
            test_type = "Functional"

        if test_type=="Benchmark":
            chart_type="measure_plot"
        else:
            chart_type="testcase_table"
        chart_config = { "chart_type": chart_type }

    # FIXTHIS - declare more config info later (such as groupings, etc.)

    # FIXTHIS - could do some chart_config sanity checking in get_chart_config
    dprint("chart_config=%s" % chart_config)
    return chart_config


# FIXTHIS - read all run.json files and re-generate the flat_plot_data.txt file
def reread_run_json_files(TESTDIR):
    test_logdir = FUEGO_RW + '/logs/' + TESTDIR
    CHART_CONFIG_JSON  = TEST_HOME + '/chart_config.json'

    # truncate existing flat_plot_data.txt file
    open(test_logdir+"/flat_plot_data.txt", "w").close()

    # find reference data
    # (NOTE: load_criteria needs FUEGO_RO, FUEGO_CORE, NODE_NAME and TESTDIR,
    #  and possibly FUEGO_CRITERIA_JSON_PATH)
    criteria_data = load_criteria()

    # find all run.json files underneath the logdir
    run_dirs = os.listdir(test_logdir)
    for d in run_dirs:
        run_filename = d+"/run.json"
        if os.path.exists(run_filename):
            # read run_data
            with open(run_filename) as run_file:
                run_data = json.load(run_file, object_pairs_hook=collections.OrderedDict)
            store_flat_results(test_logdir, run_data, criteria_data)

# Store tguid results in a flat list, space-separated
# this is stored in a file called flat_plot_data.txt in the test directory
# in the log area (not in an individual run directory).
# the data line has:
#  0       1       2     3            4        5      6    7    8
# board testname spec build_number timestamp kernel tguid ref result
#
# returns the list of lines in the updated flat results file

def store_flat_results(test_logdir, run_data, criteria_data):
    plot_data_filename=test_logdir+"/flat_plot_data.txt"
    vprint("Writing flat results to %s" % plot_data_filename)

    # get dictionaries of reference values and operations, keyed by tguid
    ref_map = {}
    op_map = {}
    for crit in criteria_data["criteria"]:
        # not all criteria are measure reference thresholds
        try:
            ref_map[crit["tguid"]] = crit["reference"]["value"]
            op_map[crit["tguid"]] = crit["reference"]["operator"]
        except:
            # ignore other data
            pass

    board = run_data["metadata"]["board"]
    test_name = run_data["name"]
    test_spec = run_data["metadata"]["test_spec"]
    build_number = run_data["metadata"]["build_number"]
    timestamp = run_data["metadata"]["timestamp"]
    kernel = run_data["metadata"]["kernel_version"]

    if os.path.isfile(plot_data_filename):
        plot_file = open(plot_data_filename,"r+") # read/write
        data_lines = plot_file.readlines()
        if len(data_lines): # file exists and not empty
            last=flat_entry(data_lines[-1])
            if last.board==board and last.spec==test_spec and \
                    last.timestamp==timestamp:
                wprint("Warning: %s already contains data for run %s-%s-%s-%s" % \
                        (plot_data_filename, board,test_spec,test_name,timestamp))
    else:
        plot_file = open(plot_data_filename,"w") # read/write
        data_lines = []

    # put data for this test in the file
    # (either at end of current file or start of new file)

    # traverse run_data, and put each test_case or measure in the file
    # on it's own line
    try:
        test_sets = run_data["test_sets"]
    except:
        eprint("run data has no test_sets")
        plot_file.close()
        return

    dprint("ref_map=%s" % ref_map)
    dprint("op_map=%s" % op_map)
    for test_set in test_sets:
        ts_name = test_set["name"]
        test_cases = test_set["test_cases"]
        for test_case in test_cases:
            tc_name = test_case["name"]
            tguid = ts_name + "." + tc_name
            status = test_case["status"]
            line = "%s %s %s %s %s %s %s %s %s\n" % \
                (board, test_name, test_spec, build_number, timestamp,
                    kernel, tguid, 'PASS', status)
            plot_file.write(line)
            data_lines.append(line)

            if test_case.has_key("measurements"):
                measurements = test_case["measurements"]
                for measure in measurements:
                    m_name = measure["name"]
                    try:
                        m_result = measure["measure"]
                    except:
                        try:
                            m_result = measure["status"]
                        except:
                            m_result = "ERROR-missing-result"

                    tguid = ts_name + "." + tc_name + "." + m_name

                    try:
                        m_ref = measure["ref"]
                    except:
                        try:
                            m_ref = ref_map[tguid]
                            m_op = op_map[tguid]
                        except:
                            m_ref = 0
                            m_op = "none"
                    line = "%s %s %s %s %s %s %s %s %s %s\n" % \
                        (board, test_name, test_spec, build_number, timestamp,
                            kernel, tguid, m_ref, m_result, m_op)

                    dprint("Writing line '%s' to (%s)" % (line, plot_data_filename))
                    plot_file.write(line)
                    data_lines.append(line)

    plot_file.close()
    return data_lines

def make_measure_plots(test_name, chart_config, entries):
    # make a measure_plot for every measure found
    chart_list = []

    try:
        requested_tguids = chart_config["measures"]
    except:
        requested_tguids = []

    # get a list of measure tguids in the data
    m_tguid_list = []
    for entry in entries:
        # if the result can be converted to a number, it's a measure
        try:
            dummy = float(entry.result)
            # filter to only the tguids in chart_config, if present
            if requested_tguids and entry.tguid not in requested_tguids:
                dprint("Dropping tguid %s, not in measure list in chart_config" % entry.tguid)
                continue

            if entry.tguid not in m_tguid_list:
                m_tguid_list.append(entry.tguid)
        except:
            pass

    # FIXTHIS - put measures into board groups, if chart_config specifies so
    # Note: this requires plot filtering in flot

    # now make a chart for each measure:
    for tguid in m_tguid_list:
        # create a series for each combination of board,spec,test,kernel,tguid
        vprint("Making a chart for measure: %s" % tguid)
        series_list = []
        title = "%s-%s" % (test_name, tguid)

        # get list of entries and refs for this measure
        m_entries = [entry for entry in entries if entry.tguid == tguid]

        # now make the series in the data file
        series_map = {}
        for entry in m_entries:
            series_key = "%s-%s-%s" % (entry.board, entry.spec,
                    entry.kernel)
            # ADDTHIS: should be able to create different data buckets
            # by using different series keys (specified by chart_config)
            # could use something like 'get_series_key(...)' here

            dprint("series_key=%s" % series_key)
            if series_key not in series_map:
                dprint("making a new series for '%s'" % series_key)
                series_map[series_key] = { "label": series_key,
                    "data": [],
                    "points": {"symbol": "circle"} }
            # add a plot point for this entry
            try:
                value = float(entry.result)
            except:
                value = 0
            point = [entry.build_number, value]
            series_map[series_key]["data"].append(point)

            ref_series_key = series_key + "-ref"

            if entry.op == "bt":
                ref_min_series_key = ref_series_key + "-min"
                dprint("ref_min_series_key=%s" % ref_min_series_key)
                if ref_min_series_key not in series_map:
                    # add a plot point for this ref min entry
                    dprint("making a new series for '%s'" % ref_min_series_key)
                    series_map[ref_min_series_key] = { "label": ref_min_series_key,
                        "data": [],
                        "points": {"symbol": "cross"} }
                try:
                    min_value = float(entry.ref.split(',')[0])
                except:
                    min_value = 0
                point = [entry.build_number, min_value]
                series_map[ref_min_series_key]["data"].append(point)
                try:
                    max_value = float(entry.ref.split(',')[1])
                except:
                    max_value = None
                ref_max_series_key = ref_series_key + "-max"
                dprint("ref_max_series_key=%s" % ref_max_series_key)
                if ref_max_series_key not in series_map:
                    # add a plot point for this ref max entry
                    dprint("making a new series for '%s'" % ref_max_series_key)
                    series_map[ref_max_series_key] = { "label": ref_max_series_key,
                        "data": [],
                        "points": {"symbol": "cross"} }
                point = [entry.build_number, max_value]
                series_map[ref_max_series_key]["data"].append(point)
            else:
                dprint("ref_series_key=%s" % ref_series_key)
                if ref_series_key not in series_map:
                    # add a plot point for this ref entry
                    dprint("making a new series for '%s'" % ref_series_key)
                    series_map[ref_series_key] = { "label": ref_series_key,
                        "data": [],
                        "points": {"symbol": "cross"} }
                try:
                    value = float(entry.ref)
                except:
                    value = 0
                point = [entry.build_number, value]
                series_map[ref_series_key]["data"].append(point)

        flot_data = series_map.values()
        flot_data.sort(key=itemgetter('label'))

        flot_options = {
                "lines" : { "show": "true", "lineWidth": 1.2 },
                "points": { "show": "true" },
                "xaxis" : { "autoscaleMargin": 0.02 },
                "grid"  : { "hoverable": "true", "clickable": "true",
                            "backgroundColor": "#f5f5f5", "borderWidth": 0.5 },
                "legend": { "position": 'nw',
                            "noColumns": 2,
                            "container": "jQuery('#legend_item_'+index)" },
                "colors": [ "#008f00","#73fa79","#009193","#73fcd6","#ff9300",
                            "#ffd479","#942193","#d783ff","#424242","#a9a9a9",
                            "#011993","#76d6ff","#929000","#fffc79","#941100",
                            "#ff7e79" ],
        }

        chart = {
                "chart_type": "measure_plot",
                "title": title,
                "data": flot_data
                #"options": flot_options
                }
        chart_list.append(chart)
    return chart_list

def make_measure_tables(test_name, chart_config, entries):
    # make a table of testcase results for every testcase
    chart_list = []
    # the value of 'JENKINS_URL' is "http://localhost:8090/fuego/", which is not we want.
    jenkins_url_prefix = "/fuego"

    # get a list of (board, test specs) in the data
    # FIXTHIS - use list of test sets in chart_config, if present
    bsp_map = {}
    for entry in entries:
        bsp_key = entry.board + "." + entry.spec + "." + entry.kernel
        bsp_map[bsp_key] = ((entry.board, entry.spec, entry.kernel))
    bsp_list = bsp_map.values()

    # now make a chart for each one:
    for board, spec, kver in bsp_list:
        # create a series for each combination of board,spec,test,kernel,tguid
        dprint("Making a chart for board: %s, test spec: %s, kernel: %s" \
               % (board, spec, kver))
        series_list = []
        title = "%s-%s-%s (%s)" % (board, test_name, spec, kver)

        # get list of test cases for this board and test spec
        tc_entries = []
        for entry in entries:
            if entry.board == board and entry.spec == spec and \
               entry.kernel == kver and entry.op != "ERROR-undefined":
                tc_entries.append(entry)

        # determine how many build numbers are represented in the data
        # and prepare to count the values in each one
        # count offfsets in the count array are:
        #   0 = PASS, 1 = FAIL, 2 = SKIP, 3 = ERR
        build_num_map = {}
        for entry in tc_entries:
            build_num_map[entry.build_number] = [0,0,0,0]

        # gather the data for each row
        result_map = {}
        for entry in tc_entries:
            row_key = entry.tguid

            dprint("row_key=%s" % row_key)
            if row_key not in result_map:
                dprint("making a new row for '%s'" % row_key)
                result_map[row_key] = {}

            # add a data point (result) for this entry
            result_map[row_key][entry.build_number] = entry.result,entry.op,entry.ref
            # count the result
            result = data_compare(entry.result, entry.ref, entry.op)
            if result=="PASS":
                build_num_map[entry.build_number][0] += 1
            elif result=="FAIL":
                build_num_map[entry.build_number][1] += 1
            elif result=="ERROR":
                build_num_map[entry.build_number][2] += 1
            else:
                build_num_map[entry.build_number][3] += 1

        bn_list = build_num_map.keys()
        bn_list.sort()

        # FIXTHIS - should read col_limit from chart_config
        col_limit = 10
        col_list = bn_list[-col_limit:]
        bn_col_count = len(col_list)

        # OK, now build the table
        html = '<table border=="1" cellspacing="0">' + \
            '<tr style="background-color:#cccccc">' + \
            '<th colspan=' + str(bn_col_count+2) + '" align="left">' + \
            'board: ' + board + '<br>' + \
            'test spec: ' + spec + '<br>' + \
            'kernel: ' + entry.kernel + '<br>' + \
            '</th></tr>' + \
            '<tr style="background-color:#cccccc">' + \
            '<th rowspan="3" align="left">measure item</th>' + \
            '<th rowspan="3" align="left">test set</th>' + \
            '<th align="center" colspan="' + str(bn_col_count) + '">results</th>' + \
            '</th></tr>' + \
            '<tr style="background-color:#cccccc">' + \
            '<th align="center" colspan="' + str(bn_col_count) + '">build_number</th>' + \
            '</th></tr>'


        row = '<tr style="background-color:#cccccc">'
        for bn in col_list:
            row += '<th>' + str(bn) + '</th>'
        row += '</tr>'
        html += row

        # one row per test case
        tg_list = result_map.keys()
        tg_list.sort(cmp_alpha_num)

        for tg in tg_list:
            # break apart tguid(tc) and divide into test set and test case
            parts = tg.split(".")
            ts = parts[0]
            tc = ".".join(parts[1:])

            # FIXTHIS: add a column for the unit of each measure item
            row_tc_head = '<tr><td>' + tc + '</td><td>' + ts + '</td>'
            row_ref_head = '<tr><td>' + tc + '(ref)</td><td>' + ts + '</td>'
            result = \
            row_tc = \
            row_ref = ""
            for bn in col_list:
                try:
                    value,op,ref = result_map[tg][bn]
                    dprint("tg=%s, bn=%s, value=%s, op=%s, ref=%s" % (tg,bn,value,op,ref))
                except:
                    value = ""
                result = data_compare(value, ref, op)
                if result=="PASS":
                    cell_attr = 'style="background-color:#ccffcc" align=\"center\"'
                elif result=="FAIL":
                    cell_attr = 'style="background-color:#ffcccc" align=\"center\"'
                else:
                    cell_attr = 'align="center"'
                    try:
                        value=float(value)
                    except:
                        value='-'

                row_tc += ("<td %s>" % cell_attr) + value + "</td>"
                row_ref += "<td align=\"center\">" + op  + " " + ref + "</td>"
            row_tail = '</tr>'

            # add a new line for each testcase
            html += row_tc_head + row_tc + row_tail
            # add a new line for the reference data of each testcase
            html += row_ref_head + row_ref + row_tail

        # now add the totals to the bottom of the table
        row = '<tr style="background-color:#cccccc"><th colspan="' + str(bn_col_count+2) + '" align="center">Totals</td></tr>'
        html += row

        summary_str = ["pass","fail","skip","error"]
        for i in range(4):
            row = '<tr><th colspan="2" align="left">' + summary_str[i] + '</td>'
            for bn in col_list:
                try:
                    result = build_num_map[bn][i]
                except:
                    result = ""
                row += "<td>" + str(result) + "</td>"
            row += '</tr>'
            html += row
        html += '</table>'
        dprint("HTML for this table is: '%s'" % html)

        chart = {
                    "title": title,
                    "chart_type": "measure_table",
                    "data": html
                }
        chart_list.append(chart)
    return chart_list

# define a comparison function for strings that might end with numbers
# like "test1, test2, ... test10"
# if items end in digits, and the leading strings are the same, then
# sort the items by the digits.  Otherwise just do an alpha sort.
an_regex=re.compile("(.*?)([0-9]+)$")
def cmp_alpha_num(a, b):
    if a and (a[-1] >= '0' and a[-1] <= '9') and \
        b and (b[-1] >= '0' and b[-1] <= '9'):
        ma = an_regex.match(a)
        mb = an_regex.match(b)
        if ma and mb and ma.groups()[0] == mb.groups()[0]:
            try:
                return cmp(int(ma.groups()[1]),int(mb.groups()[1]))
            except:
                pass
    return cmp(a,b)

# read a testcase log file, and return the URL on the line starting
# with "Job page: ".  If any error opening or reading file,
# or the line is not found, return None
def get_build_url(log_filepath):
    try:
        with open(log_filepath) as fd:
            for line in fd.readlines():
                if line.startswith("Job page: "):
                    return line[len("Job page:"):].strip()
    except IOError as error:
        return None

def make_testcase_table(test_name, chart_config, entries):
    FUEGO_RW = os.environ.get("FUEGO_RW",None)

    # make a table of testcase results for every testcase
    chart_list = []
    # the value of 'JENKINS_URL' is "http://localhost:8090/fuego/", which is not we want.
    jenkins_url_prefix = "/fuego"

    # get a list of (board,test sets) in the data
    # FIXTHIS - use list of test sets in chart_config, if present
    bts_map = {}
    for entry in entries:
        bts_key = entry.board + "." + entry.test_set
        bts_map[bts_key] = ((entry.board, entry.test_set))
    bts_list = bts_map.values()

    # now make a chart for each one:
    for board, ts in bts_list:
        # create a series for each combination of board,spec,test,kernel,tguid
        dprint("Making a chart for board: %s, test set: %s" % (board, ts))
        series_list = []
        title = "%s-%s-%s" % (board, test_name, ts)

        # get list of test cases for this board and test set
        tc_entries = []
        for entry in entries:
            if entry.board == board and entry.test_set == ts:
                tc_entries.append(entry)

        # determine how many build numbers are represented in the data
        # and prepare to count the values in each one
        # count offfsets in the count array are:
        #   0 = PASS, 1 = FAIL, 2 = SKIP, 3 = ERR
        build_num_map = {}
        for entry in tc_entries:
            build_num_map[entry.build_number] = [0,0,0,0]

        # gather the data for each row
        result_map = {}
        for entry in tc_entries:
            row_key = entry.test_case

            dprint("row_key=%s" % row_key)
            if row_key not in result_map:
                dprint("making a new row for '%s'" % row_key)
                result_map[row_key] = {}

            # break apart tguid
            parts = entry.tguid.split(".")
            test_set = ".".join(parts[:-1])
            test_case = parts[-1]

            # get the name that contains board, spec, build number, e.g. porter.default.1.1
            run_id = '%s.%s.%s.%s' % (entry.board, entry.spec, str(entry.build_number), str(entry.build_number))
            # separated log files, e.g. /userContent/fuego.logs/Functional.LTP/ubuntu.math.7.7/result/math/outputs/abs01.log
            testcase_log = '/userContent/fuego.logs/%s/%s/result/%s/outputs/%s.log' % (entry.testname, run_id, test_set, test_case)
            # testlog files, e.g. /userContent/fuego.logs/Functional.croco/porter.default.1.1/testlog.txt
            testset_log = '/userContent/fuego.logs/%s/%s/testlog.txt' % (entry.testname, run_id)

            url_result = entry.result
            # check if the separated log path exist
            if os.path.exists(os.environ["JENKINS_HOME"] + testcase_log):
                url_result = '<a href="%s%s">%s</a>' % (jenkins_url_prefix, testcase_log, entry.result)
            elif os.path.exists(os.environ["JENKINS_HOME"] + testset_log):
                url_result = '<a href="%s%s">%s</a>' % (jenkins_url_prefix, testset_log, entry.result)

            # add link to sub-test build page, if appropriate
            # for now, only do this for batch_ tests
            # FIXTHIS - support links to build pages for non-batch tests
            # outline:
            #   open the testcase log file
            #   parse the line containing "Job page:" out of the file
            #   format a url with the url from the file
            if ".batch_" in test_name and FUEGO_RW:
                testcase_log = '/userContent/fuego.logs/%s/%s/result/%s/outputs/%s.log' % (entry.testname, run_id, test_set, test_case)
                testcase_log = FUEGO_RW + "/logs/%s/%s/result/%s/outputs/%s.log" % (entry.testname, run_id, test_set, test_case)
                build_url = get_build_url(testcase_log)
                if build_url:
                    # add the url as an asterisk with a link
                    url_result += ' <a href="%s">*</a>' % build_url


            # add a data point (result) for this entry
            result_map[row_key][entry.build_number] = url_result
            # count the result
            if entry.result=="PASS":
                build_num_map[entry.build_number][0] += 1
            elif entry.result=="FAIL":
                build_num_map[entry.build_number][1] += 1
            elif entry.result=="SKIP":
                build_num_map[entry.build_number][2] += 1
            else:
                build_num_map[entry.build_number][3] += 1

        bn_list = build_num_map.keys()
        bn_list.sort()

        # FIXTHIS - should read col_limit from chart_config
        col_limit = 30
        col_list = bn_list[-col_limit:]
        bn_col_count = len(col_list)

        # OK, now build the table
        html = '<table border=="1" cellspacing="0">' + \
            '<tr style="background-color:#cccccc">' + \
            '<th colspan=' + str(bn_col_count+1) + '" align="left">' + \
            'board: ' + board + '<br>' + \
            'test set: ' + ts + '<br>' + \
            'kernel: ' + entry.kernel + '<br>' + \
            '</th></tr>' + \
            '<tr style="background-color:#cccccc">' + \
            '<th rowspan="3" align="left">test case</th>' + \
            '<th align="center" colspan="' + str(bn_col_count) + '">results</th>' + \
            '</th></tr>' + \
            '<tr style="background-color:#cccccc">' + \
            '<th align="center" colspan="' + str(bn_col_count) + '">build_number</th>' + \
            '</th></tr>'


        row = '<tr style="background-color:#cccccc">'
        for bn in col_list:
            row += '<th>' + str(bn) + '</th>'
        row += '</tr>'
        html += row

        # one row per test case
        tc_list = result_map.keys()
        tc_list.sort(cmp_alpha_num)

        for tc in tc_list:
            row = '<tr><td>' + tc + '</td>'
            for bn in col_list:
                try:
                    value = result_map[tc][bn]
                except:
                    value = ""
                if "PASS" in value:
                    cell_attr = 'style="background-color:#ccffcc"'
                elif "FAIL" in value:
                    cell_attr = 'style="background-color:#ffcccc"'
                elif "SKIP" in value:
                    cell_attr = 'style="background-color:#ffffcc"'
                elif "ERROR" in value:
                    cell_attr = 'style="background-color:#ff8888"'
                else:
                    cell_attr = 'align="center"'
                    value='-'
                row += ("<td %s>" % cell_attr) + value + "</td>"
            row += '</tr>'
            html += row

        # now add the totals to the bottom of the table
        row = '<tr style="background-color:#cccccc"><th colspan="' + str(bn_col_count+1) + '" align="center">Totals</td></tr>'
        html += row

        summary_str = ["pass","fail","skip","error"]
        for i in range(4):
            row = '<tr><td>' + summary_str[i] + '</td>'
            for bn in col_list:
                try:
                    result = build_num_map[bn][i]
                except:
                    result = ""
                row += "<td>" + str(result) + "</td>"
            row += '</tr>'
            html += row
        html += '</table>'
        dprint("HTML for this table is: '%s'" % html)

        chart = {
                    "title": title,
                    "chart_type": "testcase_table",
                    "data": html
                }
        chart_list.append(chart)
    return chart_list

# this is here because I saw it on stack overflow, and wanted to
# save it for future reference.  It is not currently used.
# it sorts a list of dictionaries by an arbitrary list of
# valus for the listed keys:
#def multikeysort(items, columns):
#    comparers = [((itemgetter(col[1:].strip()), -1) if col.startswith('-') else
#                  (itemgetter(col.strip()), 1)) for col in columns]
#    def comparer(left, right):
#        for fn, multiplier in comparers:
#            result = cmp(fn(left), fn(right))
#            if result:
#                return multiplier * result
#        return 0
#    return sorted(items, cmp=comparer)
#
# called like so:
# data = [{'Name': 'Tim', 'Points': 10},
#     {'Name': 'Charles', 'Points': 20},
#     {'Name': 'Tim',     'Points': 15},
#     {'Name': 'Charles', 'Points': 6},
#     {'Name': 'Charles', 'Points': 15},
#     ]
#
# a = multikeysort(data, ['-Points', 'Name'])
# for d in a:
#     print d["Points"],d["Name"]

def ssb_cmp(ssb, ssb2):
    spec,ts,bn = ssb.split(",")
    spec2,ts2,bn2 = ssb2.split(",")
    if spec == spec2:
        if bn == bn2:
            return cmp(ts,ts2)
        else:
            return cmp(int(bn),int(bn2))
    else:
        return cmp(spec, spec2)

def make_testset_summary_table(test_name, chart_config, entries):
    # make a chart for every board
    chart_list = []

    # get a list of boards in the data
    board_list = []
    for entry in entries:
        if entry.board not in board_list:
            board_list.append(entry.board)

    # FIXTHIS - use list of boards in chart_config, if present

    # now make a chart for each one:
    for board in board_list:
        # create a row for each combination of testset, build_number
        dprint("Making a chart for board: %s" % board)
        series_list = []
        title = "%s-%s" % (board, test_name)

        # collect the data into buckets
        #  there's bucket for each test_set, spec, build_number combination
        #    with sub-buckets for pass, fail, skip, and error counts
        # 'ssb' stands for spec,set,build_number
        ssb_map = {}
        kernel_list = []
        for entry in entries:
            if entry.board != board:
                continue

            if entry.kernel not in kernel_list:
                kernel_list.append(entry.kernel)

            ssb_key = entry.spec + "," + entry.test_set + "," + str(entry.build_number)
            if not ssb_map.has_key(ssb_key):
                ssb_map[ssb_key] = [0,0,0,0]

            if entry.result=="PASS":
                ssb_map[ssb_key][0] += 1
            elif entry.result=="FAIL":
                ssb_map[ssb_key][1] += 1
            elif entry.result=="SKIP":
                ssb_map[ssb_key][2] += 1
            else:
                ssb_map[ssb_key][3] += 1

        kernel = " ".join(kernel_list)

        # OK, now build the table
        html = '<table border=="1" cellspacing="0">' + \
            '<tr style="background-color:#cccccc">' + \
            '<th colspan="7" align="left">' + \
            'board: ' + board + '<br>' + \
            'kernel: ' + kernel + '<br>' + \
            '</th></tr>' + \
            '<tr style="background-color:#cccccc">' + \
            '<th colspan=3></th><th colspan=4 align="center">results totals</th>' + \
            '</tr>'

        # header for columns
        col_list = ["test spec", "build number", "test set","pass","fail","skip","err"]
        row = '<tr style="background-color:#cccccc">'
        for col in col_list:
            row += '<th>' + col + '</th>'
        row += '</tr>'
        html += row

        # one row per spec/build_number/test set
        ssb_list = ssb_map.keys()
        ssb_list.sort(ssb_cmp)

        # calculate rowspan for each spec and spec/build_number combo
        spec_rows = {}
        sbn_rows = {}
        for ssb in ssb_list:
            spec,ts,bn = ssb.split(",")
            if not spec in spec_rows:
                spec_rows[spec] = 1
            else:
                spec_rows[spec] += 1
            sbn = spec+","+bn
            if not sbn in sbn_rows:
                sbn_rows[sbn] = 1
            else:
                sbn_rows[sbn] += 1

        dprint("spec_rows=%s" % spec_rows)
        dprint("sbn_rows=%s" % sbn_rows)

        # this is a bit tricky...
        last_bn = ""
        last_spec = ""
        for ssb in ssb_list:
            spec, test_set, build_number = ssb.split(",")
            if spec != last_spec:
                last_spec = spec
                build_number = build_number
                last_bn = build_number
                spec_rowspan = spec_rows[spec]
                bn_rowspan = sbn_rows[spec+","+build_number]
                row = '<tr><td rowspan="' + str(spec_rowspan) + '">' + \
                        spec + '</td>' + \
                        '<td rowspan="' + str(bn_rowspan) + '">' + \
                        build_number + '</td>'
            elif build_number != last_bn:
                last_bn = build_number
                bn_rowspan = sbn_rows[spec+","+build_number]
                row = '<tr><td rowspan="' + str(bn_rowspan) + '">' + \
                        build_number + '</td>'
            else:
                row = '<tr>'

            row += '<td>' + test_set + "</td>"
            for i in range(4):
                count = ssb_map[ssb][i]
                row += "<td>" + str(ssb_map[ssb][i]) + "</td>"
            row += '</tr>'
            html += row
        html += '</table>'

        dprint("HTML for this table is: '%s'" % html)

        chart = {
                    "title": title,
                    "chart_type": "testset_summary_table",
                    "data": html
                }
        chart_list.append(chart)
    return chart_list

def make_chart_data(test_logdir, TESTDIR, chart_config_filename, data_lines):
    chart_config = get_chart_config(TESTDIR, chart_config_filename)

    chart_data_filename=test_logdir+"/flot_chart_data.json"

    test_name = TESTDIR
    chart_data = { "test_name" : TESTDIR }
    chart_type = chart_config["chart_type"]

    charts = { "chart_config": chart_config,  "charts": [] }

    # convert data to list of entry_class items
    entries = []
    for line in data_lines:
        item = flat_entry(line)
        entries.append(item)

    # make the requested charts
    if chart_type=="measure_plot":
        chart_list = make_measure_plots(test_name, chart_config, entries)
    elif chart_type=="measure_table":
        chart_list = make_measure_tables(test_name, chart_config, entries)
    elif chart_type=="testcase_table":
        chart_list = make_testcase_table(test_name, chart_config, entries)
    elif chart_type=="testset_summary_table":
        chart_list = make_testset_summary_table(test_name, chart_config, entries)
    else:
        eprint("unknown chart type: '%s'" % chart_type)
    charts["charts"].extend(chart_list)

    vprint("Writing chart data to %s" % chart_data_filename)
    with FileLock(chart_data_filename + '.lock'):
        with open(chart_data_filename, 'w') as f:
            f.write(json.dumps(charts, sort_keys=True, indent=4, separators=(',', ': ')))
